# PNA-API

This is the API for Play Network Africa.

## Install the Application

This project uses [Composer](https://getcomposer.org), install it.

Run this command to clone the project.

    `git clone git@bitbucket.org:intellectualapps/airrefuel-api.git`

Run this command to install all dependencies
	
		`composer install`.

* You need to configure the settings to suit your local environment. A sample can be found in `src/settings.php.dist`

* Ensure `logs/` is web writeable.

To run the application in development, you can also run this command. 

	composer start

Run this command to run the test suite

	composer test

That's it! Now go build something cool.

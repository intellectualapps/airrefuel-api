<?php
namespace pna\traits;

use PHPHtmlParser\Dom;

trait ParsesHtml {
	protected function getPhotoUrls($feeds) {
		$photoUrls = '';
		$count = 0;

		foreach ($feeds as $feed) {
			$domParser = new Dom;
			$domParser->load($feed->feed);
			$img = $domParser->find('img');

			if ($count > 0) {
				$photoUrls .= ',';
			}
			$photoUrls .= $this->appendDomain($img->src);
			$count++;
		}

		return $photoUrls;
	}

	protected function appendDomain($url) {
		if (stristr($url, 'http')) {
			return $url;
		} else {
			return $this->getAssetOnDomain($url);
		}
	}
}

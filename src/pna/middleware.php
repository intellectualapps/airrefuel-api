<?php

use Slim\Middleware\JwtAuthentication;
use Slim\Middleware\JwtAuthentication\RequestPathRule;
use Slim\Middleware\JwtAuthentication\RequestMethodRule;

// Middleware

$apiPath = "/api/v1";

$app->add(new JwtAuthentication(
    [
        "secure"      => $container['settings']['jwt']['securedServer'],
        "path"        => $apiPath,
        "realm"       => "Protected",
        "secret"      => $container['settings']['jwt']['authSecret'],
        "passthrough" => ["$apiPath/authenticate", "$apiPath/lookup"],
        "callback"    => function ($request, $response, $arguments) use ($container) {
            $container["jwt"] = $arguments["decoded"];
        },
        "rules" => [
            new RequestPathRule([
                "path" => "$apiPath/member",
                "passthrough" => []
            ]),
            new RequestMethodRule([
                "passthrough" => ["POST"]
            ])
        ],
        "error" => function ($request, $response, $arguments) {
            $data = [];
            $data["code"] = 401;
            $data["link"] = "@todo";
            $data["message"] = $arguments["message"];
            $data["developerMessage"] = "We cannot process your request without authentication";
            return $response
                ->withHeader("Content-Type", "application/json")
                ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
        }
    ]
));

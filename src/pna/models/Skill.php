<?php
namespace pna\models;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model {
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'skills';

	/**
	 * Turn off the created_at & updated_at columns
	 * @var boolean
	 */
	public $timestamps = false;

	/**
	 * Fields that are mass assignable
	 * @var array
	 */
	protected $fillable = [
		'user_id', 'skill',
	];

	public static function getCommaSeparatedValue($skills) {
		$skillsStr = '';
		$count = 0;
		foreach ($skills as $skill) {
			if ($count > 0) {
				$skillsStr .= ',';
			}
			$skillsStr .= $skill->skill;
			$count++;

			if ($count === 3) {
				return $skillsStr;
			}
		}

		return $skillsStr;
	}
}

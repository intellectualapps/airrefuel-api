<?php
namespace pna\models;

use Illuminate\Database\Eloquent\Model;
use pna\models\Connection;
use pna\models\Skill;
use pna\models\Photo;
use pna\models\PremiumMember;

class Member extends Model {
	
	public $timestamps = false;

	public $incrementing = false;

	protected $table = 'pnn_users';

	protected $fillable = [
		'email', 'password', 'fname', 'lname', 'flname', 'phone', "sex", "address",
		"state", "country", "url", "pic", "pic_id", "pic_likes", "job_title", "company",
		"industry", "skills", "website", "facebook", "twitter", "instagram", "views",
		"lastactive", "status", "privacy", "date", "joined",
	];

	protected $hidden = [
		'password',
	];

	public function premium() {
		return $this->hasOne(PremiumMember::class, 'member_id');
	}

	public function connections() {
		return $this->hasMany(Connection::class, 'm_id');
	}

	public function getConnectionState($otherMemberId) {
		$connection = $this->connections()->where('user_id', $otherMemberId)->first();
		if (is_null($connection)) {
			$networkState = 'none';
		} else {
			$networkState = lcfirst($connection->status);
		}
		return $networkState;
	}

	public function isPremiumMember() {
		$premuimCount = $this->premium()->count();

		return $premuimCount > 0;
	}

	public function photos() {
		return $this->hasMany(Photo::class, 'user_id');
	}

	public function feeds() {
		return $this->hasMany(Feed::class, 'user_id');
	}

	public function skills() {
		return $this->hasMany(Skill::class, 'user_id');
	}

	public function experience() {
		return $this->hasMany(Experience::class, 'user_id');

	}

	public function setEmailAttribute($value) {
		if(is_null($value)) {
			$this->attributes['email'] = '';
		} else {
			$this->attributes['email'] = $value;
		}
	}

	public function setFNameAttribute($value) {
		if(is_null($value)) {
			$this->attributes['fname'] = '';
		} else {
			$this->attributes['fname'] = $value;
		}
	}

	public function setLNameAttribute($value) {
		if(is_null($value)) {
			$this->attributes['lname'] = '';
		} else {
			$this->attributes['lname'] = $value;
		}
	}

	public function setPhoneAttribute($value) {
		if(is_null($value)) {
			$this->attributes['phone'] = '';
		} else {
			$this->attributes['phone'] = $value;
		}
	}

	public function setFlnameAttribute($value) {
		if (is_null($value)) {
			$this->attributes['flname'] = $this->fname . " " . $this->lname;
		} else {
			$this->attributes['flname'] = $value;
		}
	}

	public function setSexAttribute($value) {
		if (empty($value)) {
			$this->attributes['sex'] = '';
		} else {
			$this->attributes['sex'] = $value;
		}
	}

	public function setAddressAttribute($value) {
		if (empty($value)) {
			$this->attributes['address'] = '';
		} else {
			$this->attributes['address'] = $value;
		}
	}

	public function setCountryAttribute($value) {
		if (empty($value)) {
			$this->attributes['country'] = '';
		} else {
			$this->attributes['country'] = $value;
		}
	}

	public function setStateAttribute($value) {
		if (empty($value)) {
			$this->attributes['state'] = '';
		} else {
			$this->attributes['state'] = $value;
		}
	}

	public function setUrlAttribute($value) {
		if (empty($value)) {
			$this->attributes['url'] = 'https://playnetwork.africa';
		} else {
			$this->attributes['url'] = $value;
		}
	}

	public function setPicAttribute($value) {
		if (empty($value)) {
			$this->attributes['pic'] = 'images/avatar.jpg';
		} else {
			$this->attributes['pic'] = $value;
		}
	}

	public function getPicAttribute($value) {
		if (empty($value)) {
			return 'images/avatar.jpg';
		} else {
			return $value;
		}
	}

	public function setPicIdAttribute($value) {
		if (empty($value)) {
			$this->attributes['pic_id'] = '';
		} else {
			$this->attributes['pic_id'] = $value;
		}
	}

	public function setPicLikesAttribute($value) {
		if (empty($value)) {
			$this->attributes['pic_likes'] = 0;
		} else {
			$this->attributes['pic_like'] = $value;
		}
	}

	public function setJobTitleAttribute($value) {
		if (empty($value)) {
			$this->attributes['job_title'] = '';
		} else {
			$this->attributes['job_title'] = $value;
		}
	}

	public function setCompanyAttribute($value) {
		if (empty($value)) {
			$this->attributes['company'] = '';
		} else {
			$this->attributes['company'] = $value;
		}
	}

	public function setIndustryAttribute($value) {
		if (empty($value)) {
			$this->attributes['industry'] = '';
		} else {
			$this->attributes['industry'] = $value;
		}
	}

	public function setSkillsAttribute($value) {
		if (empty($value)) {
			$this->attributes['skills'] = '';
		} else {
			$this->attributes['skills'] = $value;
		}
	}

	public function setWebsiteAttribute($value) {
		if (empty($value)) {
			$this->attributes['website'] = '';
		} else {
			$this->attributes['website'] = $value;
		}
	}

	public function setFacebookAttribute($value) {
		if (empty($value)) {
			$this->attributes['facebook'] = '';
		} else {
			$this->attributes['facebook'] = $value;
		}
	}

	public function setTwitterAttribute($value) {
		if (empty($value)) {
			$this->attributes['twitter'] = '';
		} else {
			$this->attributes['twitter'] = $value;
		}
	}

	public function setInstagramAttribute($value) {
		if (empty($value)) {
			$this->attributes['instagram'] = '';
		} else {
			$this->attributes['instagram'] = $value;
		}
	}

	public function setViewsAttribute($value) {
		if (empty($value)) {
			$this->attributes['views'] = 0;
		} else {
			$this->attributes['views'] = $value;
		}
	}

	public function setLastActiveAttribute($value) {
		if (empty($value)) {
			$this->attributes['lastactive'] = date('Y-m-d H:i:s');
		} else {
			$this->attributes['lastactive'] = $value;
		}
	}

	public function setStatusAttribute($value) {
		if (empty($value)) {
			$this->attributes['status'] = 'Unverified';
		} else {
			$this->attributes['status'] = $value;
		}
	}

	public function setPrivacyAttribute($value) {
		if (empty($value)) {
			$this->attributes['privacy'] = 'Yes';
		} else {
			$this->attributes['privacy'] = $value;
		}
	}

	public function setDateAttribute($value) {
		if (empty($value)) {
			$this->attributes['date'] = date('Y-m-d H:i:s');
		} else {
			$this->attributes['date'] = $value;
		}
	}

	public function setJoinedAttribute($value) {
		if (empty($value)) {
			$this->attributes['joined'] = date('l, F d, Y');
		} else {
			$this->attributes['joined'] = $value;
		}
	}

	public function getPayload() {
		$membershipType = ($this->isPremiumMember()) ? "premium" : "regular";

		return [
			'id' => $this->id,
			'email' => $this->email,
			'fname' => $this->fname,
			'lname' => $this->lname,
			'flname' => $this->flname,
			'phone' => $this->phone,
			'sex' => $this->sex,
			'address' => $this->address,
			'state' => $this->state,
			'country' => $this->country,
			'url' => $this->url,
			'pic' => $this->pic,
			'picId' => $this->pic_id,
			'picLikes' => $this->pic_likes,
			'jobTitle' => $this->job_title,
			'company' => $this->company,
			'industry' => $this->industry,
			'skills' => Skill::getCommaSeparatedValue($this->skills()->get()),
			'experience' => $this->getExperience(),
			'website' => $this->website,
			'facebook' => $this->facebook,
			'twitter' => $this->twitter,
			'instagram' => $this->instagram,
			'views' => $this->views,
			'lastActive' => $this->lastactive,
			'status' => $this->status,
			'privacy' => $this->privacy,
			'date' => $this->date,
			'joined' => $this->joined,
			'membershipType' => $membershipType,
		];
	}

	function getExperience() {
		$experiences = $this->experience()->get();
		$experiencePayload = [];

		$count = 0;
		foreach ($experiences as $experience) {
			array_push($experiencePayload, $experience->getPayload());
			$count++;

			if ($count === 3) {
				return $experiencePayload;
			}
		}

		return $experiencePayload;
	}

	public static function  getMembersInIndustrySlimProfile($member, $profileAssetOnDomain)
	{
		return [
			'memberId' => $member->id,
			'fullName' => $member->flname,
			'jobTitle' => $member->job_title,
			'company' => $member->company,
			'industry' => $member->industry,
			'state' => $member->state,
			'profilePhoto' =>$profileAssetOnDomain,
			"email"=>$member->email,
			"phone"=>$member->phone,
		];
	}
}

<?php
namespace pna\models;

use Illuminate\Database\Eloquent\Model;

class Connection extends Model {
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'connections';

	/**
	 * Turn off the created_at & updated_at columns
	 * @var boolean
	 */
	public $timestamps = false;

	/**
	 * Fields that are mass assignable
	 * @var array
	 */
	protected $fillable = [
		'm_id', 'user_id', 'status', 'date',
	];
}

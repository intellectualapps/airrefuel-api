<?php
namespace pna\models;

use Illuminate\Database\Eloquent\Model;
use pna\models\State;

class AppEvent extends Model {

	public $incrementing = false;

	protected $table = 'app_event';

	public $timestamps = false;

	protected $fillable = [
		'id', 'title', 'state', 'location', 'street',
		'street_number', 'date', 'time', 'dress_code',
		'venue', 'organizer', 'pic_url', 'info', 'posted_by',
		'is_paid_event', 'has_table_reservation', 'event_type'
	];

	const TEMPLATES = [
		'index' => 'events/index.phtml',
		'eventsForm' => 'events/event-form.phtml',
	];

	public function scopeImminent($query, $date, $time) {
		return $query->whereDate('date', '=', $date)
			->where('time', '>', $time)
			->orWhere('date', '>', $date);
	}

	public function getAddressAttribute() {
		return $this->street_number . ' ' . $this->street
		. ', ' . $this->location . ', ' . $this->state->name;
	}

	public function state() {
		return $this->belongsTo(State::class);
	}

	public function getPostedByAttribute($value) {
		if (is_null($value)) {
			return '';
		} else {
			return $value;
		}
	}

	public function getInfoAttribute($value) {
		if (is_null($value)) {
			return '';
		} else {
			return $value;
		}
	}

	public function getPicUrlAttribute($value) {
		if (is_null($value)) {
			return '';
		} else {
			return $value;
		}
	}

	public function isPaidEvent() {
		return ($this->is_paid_event === 0) ? false : true;
	}

	public function getEventMemberRSVP($memberId, $eventId) {
		$rsvp = AppEventRSVP::where(['member_id' => $memberId, 'event_id' => $eventId])->count();
		if ($rsvp === 0) {
			return false;
		}

		return true;
	}

	public function hasTableReservation() {
		return ($this->has_table_reservation === 0) ? false : true;
	}

	public function getEventMemberTableReservation($memberId, $eventId) {
		$tableReservation = AppEventTableReservation::where(['member_id' => $memberId, 'event_id' => $eventId])->count();
		if ($tableReservation === 0) {
			return false;
		}

		return true;
	}

	public function getNumberOfRSVPs($eventId = null) {
		if (is_null($eventId)) {
			return AppEventRSVP::where('event_id', $this->id)->count();
		} else {
			return AppEventRSVP::where('event_id', $eventId)->count();
		}
	}

	public function getEventRSVPs() {
		$eventRSVPs = AppEventRSVP::where('event_id', $this->id)->get();
		if (empty($eventRSVPs)) {
			return [];
		} else {
			return $eventRSVPs;
		}
	}

	public function getEventTicket($eventId) {
		$eventFee = AppEventFee::select('regular_ticket as regular', 'vip_ticket as vip')->where("event_id", $eventId)->first();
		if (empty($eventFee)) {
			return false;
		} else {
			return $eventFee;
		}
	}

	public function getEventTable($eventId) {
		$eventTable = AppEventFee::select('regular_table_reservation as regular', 'vip_table_reservation as vip')
			->where("event_id", $eventId)
			->first();
		if (empty($eventTable)) {
			return false;
		} else {
			return $eventTable;
		}
	}

	public function getEventGroup($parseDate) {
		if (strpos($parseDate, 'days') !== false || strpos($parseDate, 'day') !== false || strpos($parseDate, 'hours') !== false || strpos($parseDate, 'hour') !== false || strpos($parseDate, 'minutes') !== false || strpos($parseDate, 'minute') !== false || strpos($parseDate, 'seconds') !== false || strpos($parseDate, 'second') !== false) {
			return 'thisWeek';
		} else if (strpos($parseDate, 'weeks')  !== false || strpos($parseDate, 'week') !== false) {
			return 'thisMonth';
		} else {
			return 'upcoming';
		}
	}
}
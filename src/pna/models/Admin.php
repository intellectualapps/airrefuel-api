<?php
namespace pna\models;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model {

	public $incrementing = false;

	protected $table = 'admin';

	public $timestamps = false;

	protected $fillable = [
		'email', 'password'
	];

	const TEMPLATES = [
		'index' => 'login.phtml'
    ];
}
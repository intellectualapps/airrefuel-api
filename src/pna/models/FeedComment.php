<?php
namespace pna\models;

use Illuminate\Database\Eloquent\Model;

class FeedComment extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'feed_comment';

    /**
     * Turn off the created_at & updated_at columns
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Fields that are mass assignable
     * @var array
     */
    protected $fillable = [
        'feed_id', 'comment', 'user_id', 'comment_date'
    ];

}

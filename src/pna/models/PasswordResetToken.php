<?php
namespace pna\models;

use Illuminate\Database\Eloquent\Model;
// use Valitron\Validator;

class PasswordResetToken extends Model
{
    /**
     * Turn off incrementing id
     * 
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Turn off the created_at & updated_at columns
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Fields that are mass assignable
     * @var array
     */
    protected $fillable = [
        'id', 'token',
    ];

    protected $table='password_reset_token';

    /**
     * Update author with new data
     *
     * @param  arrray $attributes
     * @return null
     */
    public function update(array $attributes = [], array $options = [])
    {
        $validator = $this->getValidator($attributes);
        if (!$validator->validate()) {
            $messages = [];
            foreach ($validator->errors() as $fieldName => $errors) {
                $messages[] = current($errors);
            }
            $message = implode("\n", $messages);
            throw new \Exception($message);
        }

        return parent::update($attributes);
    }

}

<?php
namespace pna\models;

use Illuminate\Database\Eloquent\Model;
use pna\controllers\YellowPagesListingController;

class YellowPagesListing extends Model {
  protected $table = 'yp_listings';

  public $timestamps = false;

  protected $fillable = [
    'user_id', 'biz_name', 'biz_url' , 'biz_id', 'biz_cat', 'biz_phone', 'other_phone', 'biz_email', 'biz_address', 'biz_state', 'biz_details', 'biz_offers', 'biz_website', 'biz_tags', 'biz_logo', 'biz_hours', 'biz_mop', 'views', 'date', 'dateadded'
  ];

  public function setUrlAttribute($value)
  {
		if (empty($value)) {
			$this->attributes['biz_url'] = 'https://playnetwork.africa';
		} else {
			$this->attributes['biz_url'] = $value;
		}
	}

  public function setOtherPhoneAttribute($value)
  {
		if (empty($value)) {
			$this->attributes['other_phone'] = '';
		} else {
			$this->attributes['other_phone'] = $value;
		}
	}

  public function setOffersAttribute($value)
  {
		if (empty($value)) {
			$this->attributes['biz_offers'] = '';
		} else {
			$this->attributes['biz_offers'] = $value;
		}
	}

  public function setWebsiteAttribute($value)
  {
		if (empty($value)) {
			$this->attributes['biz_website'] = '';
		} else {
			$this->attributes['biz_website'] = $value;
		}
	}

  public function setTagsAttribute($value)
  {
		if (empty($value)) {
			$this->attributes['biz_tags'] = '';
		} else {
			$this->attributes['biz_tags'] = $value;
		}
	}

  public function setLogoAttribute($value)
  {
		if (empty($value)) {
			$this->attributes['biz_logo'] = 'images/yp-default.jpg';
		} else {
			$this->attributes['biz_logo'] = $value;
		}
	}

  public function getLogoAttribute($value)
  {
		if (empty($value)) {
			return 'images/yp-default.jpg';
		} else {
			return $value;
		}
	}

  public function setViewsAttribute()
  {
		$this->attributes['views'] = 0;
	}

  public function setDateAttribute()
  {
		$this->attributes['date'] = date('Y-m-d H:i:s');
	}

  public function setJoinedAttribute()
  {
		$this->attributes['dateadded'] = date('l, F d, Y');
  }
  
  public function getPayload()
  {
    return [
      'id' => $this->id,
			'memberId' => (int) $this->user_id,
			'businessName' => $this->biz_name,
			'businessUrl' => $this->biz_url,
			'businessId' => $this->biz_id,
			'businessCategory' => YellowPagesListingController::getBusinessCategoryName($this->biz_cat),
			'businessPhone' => $this->biz_phone,
			'otherPhones' => $this->other_phone,
			'businessEmail' => $this->biz_email,
			'businessAddress' => $this->biz_address,
			'businessState' => $this->biz_state,
			'businessDetails' => strip_tags($this->biz_details),
			'businessOffers' => ($this->biz_offers == "") ? [] : explode("\r\n", strip_tags($this->biz_offers)),
			'businessWebsite' => $this->biz_website,
			'businessTags' => ($this->biz_tags == "") ? [] : explode(', ', $this->biz_tags),
			'businessLogo' => "https://playnetwork.africa/" . $this->biz_logo,
			'businessHours' => ($this->biz_hours == "") ? [] : explode("\r\n", $this->biz_hours),
			'methodsOfPayment' => ($this->biz_mop == "") ? [] : explode(', ', $this->biz_mop),
			'dateAdded' => $this->dateadded,
			'views' => $this->views
    ];
  }

}

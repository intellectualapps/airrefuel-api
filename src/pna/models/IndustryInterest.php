<?php
namespace pna\models;

use pna\models\Industry;
use Illuminate\Database\Eloquent\Model;

class IndustryInterest extends Model{

	protected $table = "user_interest";
	protected $guarded = [""]; 
	public $timestamps = false;

	public function interestAlreadyExist($memberId, $industryId)
	{
		$count = self::where("user_id", $memberId)
					->where("industry_id", $industryId)
					->count();

		return ($count>0)? true : false;
	}

	public function industryExist($industryId)
	{

		$industry = Industry::find($industryId);

		return is_null($industry)? false: true;
	}

	public function createIndustryInterest($industry_id, $member_id)
	{
		self::create([
            "user_id"=>$member_id["member-id"],
            "industry_id"=>$industry_id
        ]);
	}

	public function getInterest($member_id)
	{
	 	$interests = self::select("industry_id")
	 						->where("user_id", $member_id)
	 						->get()
	 						->toArray();

	 	$industries = Industry::find($interests);

	 	return $industries;
	}

}
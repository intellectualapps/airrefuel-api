<?php
namespace pna\models;

use Illuminate\Database\Eloquent\Model;
use pna\helpers\CasingHelper;
use pna\helpers\DateTimeHelper;
use pna\models\RedeemedOffer;

class Offer extends Model {

    public $incrementing = false;
    
    protected $table = 'offer';

    public $timestamps = false;

    protected $fillable = [
        'id', 'title', 'business_name', 'address', 'type', 'photo', 'details', 'status',
        'offer_level', 'created_at', 'starts_at', 'stops_at', 'redemption_option',
    ];

	const TEMPLATES = [
		'index' => 'offers/index.phtml',
		'offersForm' => 'offers/offer-form.phtml',
	];

	public function scopeImminent($query, $start, $stop) {
		return $query->whereDate('starts_at', '>=', $start)
			->where('stops_at', '<=', $stop);
	}

    public function getTypeAttribute($value)
    {
        return CasingHelper::getSentenceCase($value, true);
    }

    public function getOfferLevelAttribute($value)
    {
        return CasingHelper::getSentenceCase($value, true);
    }

    public function getStatusAttribute($value)
    {
        return CasingHelper::getSentenceCase($value, true);
    }

    public function isActive()
    {
        return (strcasecmp($this->status, 'ACTIVE') == 0);
    }

    public function isExpired()
    {
        $now = new DateTimeHelper('now');

        return $now->isAfter($this->stops_at);
    }

    public function isPremiumOffer()
    {
        return (strcasecmp($this->offer_level, 'PREMIUM') == 0);
    }

    public function isMultiple()
    {
        return (strcasecmp($this->type, 'MULTIPLE') == 0);
    }

    public function getNumberOfTimesRedeemed($by)
    {
        $offerRedeemedBy = $by;
        $offerRedeemedId = $this->id;
        $redeemedOffer = RedeemedOffer::where(['member_id' => $offerRedeemedBy, 'offer_id' => $offerRedeemedId])->first();

        return empty($redeemedOffer) ? 0 : $redeemedOffer['count'];
    }

    public function redemptionOption() {
        return $this->redemption_option;
    }

}

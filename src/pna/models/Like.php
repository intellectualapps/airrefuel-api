<?php
namespace pna\models;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{

    /**
     * Turn off the created_at & updated_at columns
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Fields that are mass assignable
     * @var array
     */
    protected $fillable = [
        'user_id', 'p_id', 'type', 'date'
    ];

}

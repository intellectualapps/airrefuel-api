<?php
namespace pna\models;

use Illuminate\Database\Eloquent\Model;

class PremiumMember extends Model
{
    protected $primaryKey = 'member_id';
    
    protected $table = 'premium_member';

    
    public $timestamps = false;

    
    protected $fillable = [
        'member_id', 'access_token', 'authorization_token', 'created_at', 'updated_at', 'expiry_date'
    ];

    
}

<?php
namespace pna\models;

use Illuminate\Database\Eloquent\Model;

class AppEventRSVP extends Model {
	
	public $incrementing = false;
	
	protected $table = 'app_event_rsvp';
	
	public $timestamps = false;

	public $primaryKey = [
		'member_id', 'event_id'
	];
	
	protected $fillable = [
		'member_id', 'event_id'
	];

}
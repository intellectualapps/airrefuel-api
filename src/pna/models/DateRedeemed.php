<?php
namespace pna\models;

use Illuminate\Database\Eloquent\Model;
use pna\helpers\DateTimeHelper;

class DateRedeemed extends Model
{
    protected $primaryKey = ['member_id', 'offer_id', 'created_at'];
    
    /**
     * Turn off incrementing id
     * 
     * @var boolean
     */
    public $incrementing = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'date_redeemed';

    /**
     * Turn off the created_at & updated_at columns
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Fields that are mass assignable
     * @var array
     */
    protected $fillable = [
        'member_id', 'offer_id', 'created_at'
    ];

}

<?php
namespace pna\models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'country';

    /**
     * Turn off the created_at & updated_at columns
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Fields that are mass assignable
     * @var array
     */
    protected $fillable = [
        'id', 'sortname', 'name', 'phonecode'
    ];

    static public function getPayload()
    {
        $countries = static::all();
        $data = [];

        foreach ($countries as $country) {
            array_push($data, ['id' => $country->id, 'sortName' => $country->sortname, 'name' => $country->name, 'phoneCode' => $country->phonecode]);
        }
        return $data;
    }

}

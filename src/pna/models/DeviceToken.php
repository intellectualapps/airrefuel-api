<?php
namespace pna\models;

use Illuminate\Database\Eloquent\Model;

class DeviceToken extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'device_token';

    /**
     * Turn off the created_at & updated_at columns
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Fields that are mass assignable
     * @var array
     */
    protected $fillable = [
        'email', 'token', 'platform', 'uuid'
    ];

}

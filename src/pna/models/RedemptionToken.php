<?php
namespace pna\models;

use Illuminate\Database\Eloquent\Model;
use pna\helpers\DateTimeHelper;

class RedemptionToken extends Model
{
    
    public $primaryKey = 'purpose';
    /**
     * Turn off incrementing id
     * 
     * @var boolean
     */
    public $incrementing = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'redemption_token';

    /**
     * Turn off the created_at & updated_at columns
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Fields that are mass assignable
     * @var array
     */
    protected $fillable = [
        'purpose', 'token'
    ];
}

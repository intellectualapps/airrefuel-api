<?php
namespace pna\models;

use Illuminate\Database\Eloquent\Model;

class AppPNADaily extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'title', 'image_url', 'content_url'
    ];

    protected $table = 'app_pnadaily_home_screen';
}

<?php
namespace pna\models;

use Illuminate\Database\Eloquent\Model;

class YellowPagesCategory extends Model {
  protected $table = 'yp_categories';

  protected $primaryKey = 'cat_id';

  public $timestamps = false;

  protected $fillable = [
    'cat_id', 'cat_name'
  ];

}

<?php
namespace pna\models;

use Illuminate\Database\Eloquent\Model;

class Feed extends Model {
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'feed';

	/**
	 * Turn off the created_at & updated_at columns
	 * @var boolean
	 */
	public $timestamps = false;

	/**
	 * Fields that are mass assignable
	 * @var array
	 */
	protected $fillable = [
		'user_id', 'p_id', 'type', 'feed', 'likes', 'date',
	];

	const TYPE_INFO = 'info';
	const TYPE_SHARE = 'share';
	const TYPE_PIC = 'pic';
	const TYPE_JOB = 'job';

}

<?php

namespace pna\models;

use Awurth\SlimValidation\Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use pna\models\ResponsePayload;

class ErrorResponsePayload {
	public static function getValidationErrorPayload($link, Validator $validator) {
		$code = 422;
		$developerMessage = $validator->getErrors();

		switch ($link) {
		case '/offer':
			$link = $link;
			$message = 'Invalid offer parameters.';
			break;

		default:
			$message = "Invalid parameters";
			break;
		}

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
	}

	public static function getParametersErrorPayload($link) {
		$code = 422;
		$developerMessage = 'Some required parameters are missing';

		switch ($link) {
		case '/offer':
			$link = $link;
			$message = 'Invalid offer';
			break;

		default:
			$link = $link;
			$message = 'Missing parameters';
			break;
		}

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
	}

	/**
	 * Database error
	 *
	 * @return Array
	 */
	public static function getDatabaseErrorPayload($link, $dbException) {
		$code = 500;
		$link = $link;
		$message = "An error occured";
		$developerMessage = $dbException->getMessage();

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
	}

	public static function getCustomErrorPayload($code = 422, $message = '', $link = '', $developerMessage = '', $exception = null) {
		if (is_null($exception)) {
			$developerMessage = $developerMessage;
		} else {
			$developerMessage = $exception->getMessage();
		}

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
	}

	public static function getModelNotFoundErrorPayload($link, ModelNotFoundException $modelException) {
		$model = $modelException->getModel();

		switch ($model) {
		case 'pna\models\Member':
			$code = 401;
			$message = "Member does not exist.";
			$developerMessage = $modelException->getMessage();
			break;
		case 'pna\models\AppEvent':
			$code = 500;
			$message = "AppEvent does not exist.";
			$developerMessage = $modelException->getMessage();
			break;

		case 'pna\models\Industry':
			$code = 500;
			$message = "Industry does not exist.";
			$developerMessage = $modelException->getMessage();
			break;

		default:
			$code = 500;
			$message = "[{$model}] model does not exist.";
			$developerMessage = $modelException->getMessage();
			break;
		}

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
	}

	public static function getDateErrorPayload($link) {
		$code = 422;
		$developerMessage = 'The date provided has passed.';

		$message = "Invalid date.";

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
	}
}

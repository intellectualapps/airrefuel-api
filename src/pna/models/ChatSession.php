<?php
namespace pna\models;

use Illuminate\Database\Eloquent\Model;

class ChatSession extends Model {
	protected $primaryKey = 'id';

	protected $table = 'chat_session';

	public $timestamps = false;

	protected $fillable = [
		'id', 'member_id', 'other_member_id', 'created_at',
	];

	public function getPayload()
	{
		$chatSession = ChatSession::select('id as chatId', 'member_id', 'other_member_id', 'created_at')
				->where(['member_id' => $this->member_id, 'other_member_id' => $this->other_member_id])
				->orWhere(['member_id' => $this->other_member_id, 'other_member_id' => $this->member_id])
				->first();

		return [
			'chatId' => $chatSession->chatId,
			'memberId' => $chatSession->member_id,
			'otherMemberId' => $chatSession->other_member_id,
			'dateStarted' => $chatSession->created_at
		];
	}
}

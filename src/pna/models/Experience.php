<?php
namespace pna\models;

use Illuminate\Database\Eloquent\Model;

class Experience extends Model {
	protected $table = 'experience';

	public $timestamps = false;

	protected $fillable = [
		'user_id', 'employer', 'position', 'period', 'location', 'dateadded', 'date',
	];

	public function getPayload() {
		return [
			'id' => $this->id,
			'userId' => $this->user_id,
			'employer' => $this->employer,
			'position' => $this->position,
			'period' => $this->period,
			'location' => $this->location,
			'dateAdded' => $this->dateadded,
			'date' => $this->date,
		];
	}

}

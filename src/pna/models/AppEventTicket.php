<?php
namespace pna\models;

use Illuminate\Database\Eloquent\Model;

class AppEventTicket extends Model {
	
	public $incrementing = false;
	
	protected $table = 'app_event_ticket';
	
	public $timestamps = false;

	public $primaryKey = [
		'member_id', 'event_id'
	];
	
	protected $fillable = [
		'member_id', 'event_id', 'ticket_type', 'reference_code', 'created_at', 'quantity'
	];

}
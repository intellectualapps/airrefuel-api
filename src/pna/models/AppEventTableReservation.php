<?php
namespace pna\models;

use Illuminate\Database\Eloquent\Model;

class AppEventTableReservation extends Model {
	
	public $incrementing = false;
	
	protected $table = 'app_event_table_reservation';
	
	public $timestamps = false;

	public $primaryKey = [
		'member_id', 'event_id'
	];
	
	protected $fillable = [
		'member_id', 'event_id', 'table_reservation_type', 'reference_code', 'created_at', 'quantity'
	];

}
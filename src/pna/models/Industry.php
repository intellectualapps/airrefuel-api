<?php
namespace pna\models;

use Illuminate\Database\Eloquent\Model;

class Industry extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'industry';

    /**
     * Turn off the created_at & updated_at columns
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Fields that are mass assignable
     * @var array
     */
    protected $fillable = [
        'id', 'industry'
    ];

    public function getPayload($payload) {
        return [
            "industryId" => $payload->id,
            "industryName" => $payload->industry,
            "imageUrl" => $payload->image_url
        ];
    }

}

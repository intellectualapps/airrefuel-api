<?php
namespace pna\models;

use Illuminate\Database\Eloquent\Model;

class PNADaily extends Model
{
    /**
     * Turn off the created_at & updated_at columns
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Fields that are mass assignable
     * @var array
     */
    protected $fillable = [
        'banner', 'post', 'posted', 'date'
    ];

    /**
     * Fields that are hidden
     * @var array
     */
    protected $hidden = [
        'banner',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pnndaily';
}

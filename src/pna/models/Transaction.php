<?php
namespace pna\models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model {
  protected $table = 'transaction';

  /**
   * Turn on the created_at & updated_at columns
   * @var boolean
   */
  public $timestamps = false;

  /**
   * Fields that are mass assignable
   * @var array
   */
  protected $fillable = [
    'reference_code', 'email', 'access_code', 'amount', 'response_code', 'response_message'
  ];

}

<?php
namespace pna\models;

use Illuminate\Database\Eloquent\Model;

class State extends Model {
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'state';

	/**
	 * Turn off the created_at & updated_at columns
	 * @var boolean
	 */
	public $timestamps = false;

	/**
	 * Fields that are mass assignable
	 * @var array
	 */
	protected $fillable = [
		'id', 'name', 'country_id',
	];

	public function appEvents() {
		return $this->hasMany(AppEvent::class);
	}

}

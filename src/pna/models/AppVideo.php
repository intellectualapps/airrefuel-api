<?php
namespace pna\models;

use Illuminate\Database\Eloquent\Model;

class AppVideo extends Model {

	public $incrementing = false;

	protected $table = 'app_video_home_screen';

	protected $primaryKey = 'title';

	public $timestamps = false;

	protected $fillable = [
		'title', 'video_url'
    ];
}
<?php
namespace pna\models;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model {

	/**
	 * Turn off the created_at & updated_at columns
	 * @var boolean
	 */
	public $timestamps = false;

	/**
	 * Fields that are mass assignable
	 * @var array
	 */
	protected $fillable = [
		'album_id', 'p_id', 'user_id', 'photo', 'likes', 'caption', 'date',
	];

}

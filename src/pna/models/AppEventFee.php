<?php
namespace pna\models;

use Illuminate\Database\Eloquent\Model;

class AppEventFee extends Model {
	
	public $incrementing = false;
	
	protected $table = 'app_event_fee';
	
	public $timestamps = false;

	public $primaryKey = [
		'event_id'
	];
	
	protected $fillable = [
		'event_id', 'regular_ticket', 'vip_ticket', 'regular_table_reservation', 'vip_table_reservation'
	];

}
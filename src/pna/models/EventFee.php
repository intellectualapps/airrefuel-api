<?php
namespace pna\models;

use Illuminate\Database\Eloquent\Model;

class EventFee extends Model {

	/**
	 * Turn off incrementing id
	 *
	 * @var boolean
	 */
	public $incrementing = false;
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'app_event_fee';

	/**
	 * Turn off the created_at & updated_at columns
	 * @var boolean
	 */
	public $timestamps = false;

	/**
	 * Fields that are mass assignable
	 * @var array
	 */
	protected $fillable = [
		'event_id', 'regular_ticket', 'vip_ticket',
		'regular_table_reservation', 'vip_table_reservation',
	];

	protected $primaryKey = 'event_id';

}

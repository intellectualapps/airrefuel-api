<?php
namespace pna\controllers;

use Illuminate\Database\Capsule\Manager as DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Respect\Validation\Validator as Rule;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;
use pna\controllers\BaseController;
use pna\exceptions\BaseException;
use pna\helpers\DateTimeHelper;
use pna\helpers\StringTrimmer;
use pna\helpers\UniqueIdHelper;
use pna\models\DateRedeemed;
use pna\models\ErrorResponsePayload;
use pna\models\Member;
use pna\models\Offer;
use pna\models\RedeemedOffer;
use pna\models\RedemptionToken;
use pna\models\ResponsePayload;

class OfferController extends BaseController {
    protected $requiredParams = ['title', 'business-name', 'address', 'details', 'start-date', 'stop-date', 'redemption-option'];

    public function __construct(Container $container) {
        parent::__construct($container);
    }

    public function listOffersPage(Request $request, Response $response) {
		$requestParams = $request->getParams();

		$messages['error'] = $this->container->flash->getFirstMessage('error');
		$messages['success'] = $this->container->flash->getFirstMessage('success');

		$start = isset($requestParams['start']) ? $requestParams['start'] : 0;
		$pageSize = 10;

        $today = new DateTimeHelper();
        $date = $today->format('Y-m-d');

		$offers = Offer::whereDate('starts_at', '>=', $date)
			->where('stops_at', '>=', $date)
            ->skip($start)
            ->take($pageSize)
            ->orderBy('created_at', 'desc')
            ->get();
            
		$offersCount = Offer::whereDate('starts_at', '>=', $date)
            ->where('stops_at', '<=', $date)
            ->count();

		$offersIndexUrl = $this->getEndpointUrl('offers.index');
		$offersSearchUrl = $this->getEndpointUrl('offers.search');
		$offersCreateUrl = $this->getEndpointUrl('offers.create');
        $offersEditUrl = $this->getEndpointUrl('offers.edit', ['offer-id' => '']);
        
        $eventsIndexUrl = $this->getEndpointUrl('events.index');

		return $this->container->view->render($response, Offer::TEMPLATES['index'], [
			'offers' => $offers, 'start' => $start, 'count' => $offersCount, 'offersIndex' => $offersIndexUrl,
			'offersSearch' => $offersSearchUrl, 'offersCreate' => $offersCreateUrl,
			'offersEdit' => $offersEditUrl, 'eventsIndex' => $eventsIndexUrl, 'messages' => $messages,
		]);
	}

	public function createOfferPage(Request $request, Response $response) {
		$today = new DateTimeHelper();

		$messages['error'] = $this->container->flash->getFirstMessage('error');
		$messages['success'] = $this->container->flash->getFirstMessage('success');

		$offersIndexUrl = $this->getEndpointUrl('offers.index');
		$offersCreateUrl = $this->getEndpointUrl('api.offer.create');
        
        $eventsIndexUrl = $this->getEndpointUrl('events.index');

		return $this->container->view->render($response, Offer::TEMPLATES['offersForm'], [
			'today' => $today->format('Y-m-d'), 'action' => $offersCreateUrl,
			'offersIndex' => $offersIndexUrl, 'eventsIndex' => $eventsIndexUrl, 'messages' => $messages,
		]);
	}

	public function getEditOfferPage(Request $request, Response $response, $args) {
		$offerId = $args['offer-id'];
		$today = new DateTimeHelper();
		$link = $this->getPath($request);

		$messages['error'] = $this->container->flash->getFirstMessage('error');
		$messages['success'] = $this->container->flash->getFirstMessage('success');

		try {
            $eventsIndexUrl = $this->getEndpointUrl('events.index');
			$offersIndexUrl = $this->getEndpointUrl('offers.index');
			$offer = Offer::findOrFail($offerId);
			$offersUpdateUrl = $this->getEndpointUrl('api.offer.edit', ['offer-id' => $offer->id]);

			return $this->container->view->render($response, Offer::TEMPLATES['offersForm'], [
				'today' => $today->format('Y-m-d'), 'offer' => $offer,
				'form' => 'edit', 'action' => $offersUpdateUrl, 'offersIndex' => $offersIndexUrl, 'eventsIndex' => $eventsIndexUrl,
				'messages' => $messages,
			]);

		} catch (QueryException $dbException) {
			$databaseErrorPayload = ErrorResponsePayload::getDatabaseErrorPayload($link, $dbException);
			$this->container->flash->addMessage('error', $databaseErrorPayload['message']);
			return $response->withRedirect($offersIndexUrl);
		} catch (ModelNotFoundException $modelException) {
			$modelNotFoundErrorPayload = ErrorResponsePayload::getModelNotFoundErrorPayload($link, $modelException);
			$this->container->flash->addMessage('error', $modelNotFoundErrorPayload['message']);
			return $response->withRedirect($offersIndexUrl);
		}
	}

    public function createOffer(Request $request, Response $response) {
        $offerCreationRules = $this->getRulesForOfferCreation();
        $validator = $this->getValidator($request, $offerCreationRules);
        $endpoint = $this->getPath($request);
		$offersCreateUrl = $this->getEndpointUrl('offers.create');

        if (!$validator->isValid()) {
            $validationErrorPayload = ErrorResponsePayload::getValidationErrorPayload($endpoint, $validator);
            return $response->withJson($validationErrorPayload, $validationErrorPayload['code']);
        }

		$requestParams = StringTrimmer::trimSpaces($request->getParams());

        if ($this->hasMissingRequiredParams($requestParams)) {
            $parametersErrorPayload = ErrorResponsePayload::getParametersErrorPayload($endpoint);
            return $response->withJson($parametersErrorPayload, $parametersErrorPayload['code']);
        }

        $today = new DateTimeHelper('now');

        if ($today->isAfter($requestParams['stop-date'])) {
            $customErrorPayload = ErrorResponsePayload::getCustomErrorPayload(422, 'Invalid offer',
                '/offer', "Invalid offer; stop-date can not be earlier than today's date.");
            return $response->withJson($customErrorPayload, $customErrorPayload['code']);
        }

        $requestParams['created-at'] = $today->format('Y-m-d');

        if ($this->hasUploaded("photo")) {
			$config = $this->getSettingsAttribute('cloudinary');
			\Cloudinary::config($config);
			$uploadResponse = \Cloudinary\Uploader::upload($_FILES["photo"]["tmp_name"]);
			$requestParams['photo'] = $uploadResponse['url'];
		}

        $offerParams = $this->getOfferParams($requestParams);

        try {
            $offer = new Offer($offerParams);
            $offer->save();

            $successMessage = 'Discount creation was successful.';

            $this->container->flash->addMessage('success', $successMessage);
            return $response->withRedirect($offersCreateUrl);
        } catch (QueryException $dbException) {
            $this->container->flash->addMessage('error', $databaseErrorPayload['message']);
			return $response->withRedirect($offersCreateUrl);
		} catch (ModelNotFoundException $modelException) {
			$modelNotFoundErrorPayload = ErrorResponsePayload::getModelNotFoundErrorPayload($endpoint, $modelException);
			$this->container->flash->addMessage('error', $modelNotFoundErrorPayload['message']);
			return $response->withRedirect($offersEditUrl);
		}
    }

    public function getOffers(Request $request, Response $response) {
        $this->requiredParams = ['member-id'];
        $requestParams = $request->getQueryParams();
        $link = $this->getPath($request);

        if ($this->hasMissingRequiredParams($requestParams)) {
            $customErrorPayload = ErrorResponsePayload::getCustomErrorPayload(422, 'Missing parameters.',
                    $link, "Some required parameters are missing.");
            return $response->withJson($customErrorPayload, $customErrorPayload['code']);
        }

        if (isset($requestParams['start']) && isset($requestParams['page-size'])) {
            $rulesForFetchingOffers = $this->getRulesForFetchingOffers();
            $validator = $this->getValidator($request, $rulesForFetchingOffers);

            if (!$validator->isValid()) {
                $validationErrorPayload = ErrorResponsePayload::getValidationErrorPayload('/offer', $validator);
                return $response->withJson($validationErrorPayload, $validationErrorPayload['code']);
            }

            $start = $requestParams['start'];
            $pageSize = $requestParams['page-size'];
        } else {
            $start = 0;
            $pageSize = 10;
        }

        $offersPayload = [];
        $status = (array_key_exists('status', $requestParams)) ?  strtoupper($requestParams['status']) : '';

        if ($start < 0) {
            $customErrorPayload = ErrorResponsePayload::getCustomErrorPayload(422, 'Invalid parameter', $link, 
                'The value of start parameter cannot be negative.');
            return $response->withJson($customErrorPayload, $customErrorPayload['code']);
        }

        if ($pageSize < 0) {
            $customErrorPayload = ErrorResponsePayload::getCustomErrorPayload(422, 'Invalid parameter', $link, 
                'The value of page-size parameter cannot be negative.');
            return $response->withJson($customErrorPayload, $customErrorPayload['code']);
        }

        try {
            if (empty($status)) {
                $offers = Offer::skip($start)->take($pageSize)->get();
            } else {
                $offers = Offer::where('status', $status)->skip($start)->take($pageSize)->get();
            }
        } catch (QueryException $dbException) {
            $databaseErrorPayload = ErrorResponsePayload::getDatabaseErrorPayload('/offer', $dbException);
            return $response->withJson($databaseErrorPayload, $databaseErrorPayload['code']);
        }

        if ($offers->count() < 1) {
            $offersPayload = [];
            return $response->withJson(['offers' => $offersPayload]);
        }

        foreach ($offers as $offer) {
            $offerArray = $offer->toArray();
            $offerArray['status'] = $offer->status;
            $offerArray['numberOfTimesRedeemed'] = $offer->getNumberOfTimesRedeemed($requestParams['member-id']);
            array_push($offersPayload, ResponsePayload::getDataPayload($offerArray));
        }
        return $response->withJson(['offers' => $offersPayload]);
    }

    public function redeemOffer(Request $request, Response $response, $args) {
        $offerRedemptionRules = $this->getRulesForOfferRemdemption();
        $validator = $this->getValidator($request, $offerRedemptionRules);

        $link = $this->getPath($request);

        if (!$validator->isValid()) {
            $validationErrorPayload = ErrorResponsePayload::getValidationErrorPayload($link, $validator);
            return $response->withJson($validationErrorPayload, $validationErrorPayload['code']);
        }

        $this->requiredParams = ['member-id', 'offer-id', 'token', 'redemption-option'];

        $requestParams = [
            'member-id' => $request->getParam('member-id'),
            'offer-id' => $args['offer-id'],
            'token' => $request->getParam('token'),
            'redemption-option' => $request->getParam('redemption-option')
        ];

        if ($this->hasMissingRequiredParams($requestParams)) {
            $parametersErrorPayload = ErrorResponsePayload::getParametersErrorPayload($link);
            return $response->withJson($parametersErrorPayload, $parametersErrorPayload['code']);
        }

        try {
            $token = RedemptionToken::where('token', $requestParams['token'])->first();
            $offer = Offer::findOrFail($requestParams['offer-id']);
            $member = Member::findOrFail($requestParams['member-id']);

            if (is_null($token)) {
                $customErrorPayload = ErrorResponsePayload::getCustomErrorPayload(422, 'Invalid token.',
                    $link, "Invalid token; offer can not be redeemed since token is invalid.");
                return $response->withJson($customErrorPayload, $customErrorPayload['code']);
            }

            if (is_null($offer)) {
                $customErrorPayload = ErrorResponsePayload::getCustomErrorPayload(422, 'Invalid offer.',
                    $link, "Invalid offer; the requested offer was not found.");
                return $response->withJson($customErrorPayload, $customErrorPayload['code']);
            }

            if ($offer->isExpired()) {
                $customErrorPayload = ErrorResponsePayload::getCustomErrorPayload(422, 'Offer has expired.',
                    $link, "Offer with the provided offer-id has expired.");
                return $response->withJson($customErrorPayload, $customErrorPayload['code']);
            }

            if (!$offer->isActive()) {
                $customErrorPayload = ErrorResponsePayload::getCustomErrorPayload(422, $offer->isActive(),
                    $offer->status, "Offer with the provided offer-id is inactive.");
                return $response->withJson($customErrorPayload, $customErrorPayload['code']);
            }

            if ($requestParams['redemption-option'] !== $offer->redemptionOption()) {
                $customErrorPayload = ErrorResponsePayload::getCustomErrorPayload(422, "Invalid redemption option",
                    $link, "Offer with the provided redemption option cannot be redeemed.");
                return $response->withJson($customErrorPayload, $customErrorPayload['code']);
            }

            if ($offer->isPremiumOffer() && !$member->isPremiumMember()) {
                $customErrorPayload = ErrorResponsePayload::getCustomErrorPayload(422, 'Premium members only.',
                    $link, "Offer with the provided offer-id is a premium offer but member with the provided member-id is a regular member.");
                return $response->withJson($customErrorPayload, $customErrorPayload['code']);
            }

            $redeemedOffer = $this->getRedeemedOffer($member->id, $offer->id);
            $redeemedOfferArray = $redeemedOffer->toArray();
            $redeemedOfferArray['redemptionStatus'] = "Success";
            $redeemedOfferArray['offer'] = $offer;

            $redeemedOfferPayload = ResponsePayload::getDataPayload($redeemedOfferArray);

            return $response->withJson(['redeemedOffer' => $redeemedOfferPayload]);

        } catch (QueryException $dbException) {
            $databaseErrorPayload = ErrorResponsePayload::getDatabaseErrorPayload($link, $dbException);
            return $response->withJson($databaseErrorPayload, $databaseErrorPayload['code']);
        } catch (BaseException $baseException) {
            $customErrorPayload = ErrorResponsePayload::getCustomErrorPayload(422, 'Offer already redeemed',
                $link, $baseException->getMessage());
            return $response->withJson($customErrorPayload, $customErrorPayload['code']);
        } catch (ModelNotFoundException $modelException) {
            $model = $modelException->getModel();
            switch ($model) {
                case 'pna\models\Offer':
                    $code = 422;
                    $message = 'Invalid offer.';
                    $developerMessage = 'Offer with the provided offer-id could not be found.';
                    break;

                default:
                    $message = 'Only members can redeem offers.';
                    $developerMessage = "Member with the provided member-id could not be found.";
                    break;
            }
            $customErrorPayload = ErrorResponsePayload::getCustomErrorPayload(401, $message,
                $link, $developerMessage);
            return $response->withJson($customErrorPayload, $customErrorPayload['code']);
        }
    }

    public function updateOffer(Request $request, Response $response, $args) {
		$offerId = $args['offer-id'];
		$today = new DateTimeHelper();
		$link = $this->getPath($request);
        $requestParams = $request->getParams();

        try {
            $attributesToUpdate = [];
            $offer = Offer::findOrFail($offerId);
            $offersEditUrl = $this->getEndpointUrl('offers.edit', ['offer-id' => $offer->id]);
            
            $offer->offer_level = ($offer->offer_level != $requestParams['offer-level']) ?  $requestParams['offer-level'] : $offer->offer_level;
            $offer->title = ($offer->title != $requestParams['title']) ?  $requestParams['title'] : $offer->title;
            $offer->business_name = ($offer->business_name != $requestParams['business-name']) ?  $requestParams['business-name'] : $offer->business_name;
            $offer->address = ($offer->address != $requestParams['address']) ?  $requestParams['address'] : $offer->address;
            $offer->details = ($offer->details != $requestParams['details']) ?  $requestParams['details'] : $offer->details;
            $offer->starts_at = ($offer->starts_at != $requestParams['start-date']) ?  $requestParams['start-date'] : $offer->starts_at;
            $offer->stops_at = ($offer->stops_at != $requestParams['stop-date']) ?  $requestParams['stop-date'] : $offer->stops_at;
            $offer->type = ($offer->type != $requestParams['type']) ?  strtoupper($requestParams['type']) : $offfer->type;
            $offer->status = ($offer->status != $requestParams['status']) ?  strtoupper($requestParams['status']) : $offer->status;
            $offer->redemption_option = ($offer->redemption_option != $requestParams['redemption-option']) ?  strtoupper($requestParams['redemption-option']) : $offer->redemption_option;

			if ($this->hasUploaded("photo")) {
				$config = $this->getSettingsAttribute('cloudinary');
				\Cloudinary::config($config);
				$uploadResponse = \Cloudinary\Uploader::upload($_FILES["photo"]["tmp_name"]);
				$offer->photo = $uploadResponse['url'];
            }
            
            $offer->update();
            $this->container->flash->addMessage('success', 'Update successful.');
			return $response->withRedirect($offersEditUrl);
        } catch (QueryException $dbException) {
			$databaseErrorPayload = ErrorResponsePayload::getDatabaseErrorPayload($link, $dbException);
			$this->container->flash->addMessage('error', $databaseErrorPayload['message']);
			return $response->withRedirect($offersEditUrl);
		} catch (ModelNotFoundException $modelException) {
			$modelNotFoundErrorPayload = ErrorResponsePayload::getModelNotFoundErrorPayload($link, $modelException);
			$this->container->flash->addMessage('error', $modelNotFoundErrorPayload['message']);
			return $response->withRedirect($offersEditUrl);
		}
    }

    private function getRedeemedOffer($memberId, $offerId) {
        $redeemedOffer = RedeemedOffer::where(['member_id' => $memberId, 'offer_id' => $offerId])->first();

        $today = new DateTimeHelper();
        $redemptionDate = [
            'member_id' => $memberId, 'offer_id' => $offerId, 'created_at' => $today->format('Y-m-d'),
        ];

        if (empty($redeemedOffer)) {
            $redeemOffer = ['member_id' => $memberId, 'offer_id' => $offerId, 'count' => 1];

            $redeemedOffer = new RedeemedOffer($redeemOffer);

            DB::transaction(function () use ($redeemedOffer, $redemptionDate) {
                $redeemedOffer->save();

                $redemptionDate = new DateRedeemed($redemptionDate);
                $redemptionDate->save();
            });
        } else {

            $offer = Offer::findOrFail($offerId);

            if (!$offer->isMultiple()) {
                throw new BaseException('Offer with the provided offer-id has already been redeemed by member with the provided member-id.');
            }

            DB::transaction(function () use ($redeemedOffer, $redemptionDate) {
                $redeemedBefore = DateRedeemed::where($redemptionDate)->first();

                if (is_null($redeemedBefore)) {
                    $redeemedOffer->where(['member_id' => $redeemedOffer->member_id, 'offer_id' => $redeemedOffer->offer_id])
                        ->increment('count', 1);

                    $redemptionDate = new DateRedeemed($redemptionDate);
                    $redemptionDate->save();
                } else {
                    throw new BaseException('You have already redeemed this offer today');
                }
                
            });
        }

        $redeemedOffer = RedeemedOffer::where(['member_id' => $memberId, 'offer_id' => $offerId])->first();

        return $redeemedOffer;
    }

    private function getOfferParams(array $params) {
        $type = (is_null($params['type'])) ? 'ONE_OFF' : strtoupper($params['type']);

        $status = (is_null($params['status'])) ? 'INACTIVE' : strtoupper($params['status']);
        $offer_level = (is_null($params['offer_level'])) ? 'REGULAR' : strtoupper($params['offer_level']);

        return [
            'id' => UniqueIdHelper::getUniqueId(),
            'title' => $params['title'],
            'business_name' => $params['business-name'],
            'address' => $params['address'],
            'type' => $type,
            'photo' => $params['photo'],
            'details' => $params['details'],
            'status' => $status,
            'offer_level' => $offer_level,
            'created_at' => $params['created-at'],
            'starts_at' => $params['start-date'],
            'stops_at' => $params['stop-date'],
            'redemption_option' => $params['redemption-option']
        ];
    }

    private function getRulesForOfferCreation() {
        return [
            'title' => Rule::stringType()->length(1, null),
            'business-name' => Rule::stringType()->length(1, null),
            'address' => Rule::stringType()->length(1, null),
            'type' => Rule::when(Rule::stringType(), Rule::length(7, 8), Rule::nullType()),
            'details' => Rule::stringType()->length(1, null),
            'status' => Rule::when(Rule::stringType(), Rule::length(6, 8), Rule::nullType()),
            'offer_level' => Rule::when(Rule::stringType(), Rule::length(7), Rule::nullType()),
            'start-date' => Rule::date('Y-m-d')->length(1, null),
            'stop-date' => Rule::date('Y-m-d')->length(1, null),
        ];
    }

    private function getRulesForOfferRemdemption() {
        return [
            'token' => Rule::stringType()->noWhitespace(),
        ];
    }

    private function getRulesForFetchingOffers() {
        return [
            'status' => Rule::when(Rule::stringType(), Rule::length(6, 8), Rule::nullType()),
            'start' => Rule::intVal(),
            'page-size' => Rule::intVal(),
        ];
    }

    public function getOfferById(Request $request, Response $response, $args) {
        $link = $this->getPath($request);
        $this->requiredParams = ['member-id', 'offer-id'];

        $params = [
			"member-id" => $request->getQueryParam('member-id'),
			"offer-id" => $args['offer-id'],
		];

        if ($this->hasMissingRequiredParams($params)) {
            $parametersErrorPayload = ErrorResponsePayload::getParametersErrorPayload($link);
			return $response->withJson($parametersErrorPayload, $parametersErrorPayload['code']);
        }

        try {
            $offer = Offer::where('id', $params['offer-id'])->first();
        } catch (QueryException $dbException) {
            $databaseErrorPayload = ErrorResponsePayload::getDatabaseErrorPayload($link, $dbException);
            return $response->withJson($databaseErrorPayload, $databaseErrorPayload['code']);
        }

        if (empty($offer)) {
            $offerPayload = [];
            return $response->withJson(['offer' => $offerPayload]);
        }

        $offerArray = $offer->toArray();
        $offerArray['numberOfTimesRedeemed'] = $offer->getNumberOfTimesRedeemed($params['member-id']);
        $offersPayload = ResponsePayload::getDataPayload($offerArray);
        
        return $response->withJson(['offer' => $offersPayload]);
    }
}

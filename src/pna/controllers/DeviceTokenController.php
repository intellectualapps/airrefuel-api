<?php
namespace pna\controllers;

use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;
use Illuminate\Database\QueryException;
use Illuminate\Pagination\Paginator;
use pna\controllers\BaseController;
use pna\models\Member;
use pna\models\DeviceToken;
use pna\models\ResponsePayload;

class DeviceTokenController extends BaseController
{
    protected $requiredParams = ['email', 'token', 'platform', 'uuid'];

    public function __construct(Container $container)
    {
        parent::__construct($container);
    }

    /**
     * Add device token
     */
    public function addDeviceToken(Request $request, Response $response)
    {
        $params = $request->getParsedBody();

        if (is_null($params['email'])) {
            $parametersErrorPayload = $this->getParametersErrorPayload('/member/device-token');
            return $response->withJson($parametersErrorPayload, 401);
        }

        if ($this->hasMissingRequiredParams($params)) {
            $parametersErrorPayload = $this->getParametersErrorPayload('/member/device-token');
            return $response->withJson($parametersErrorPayload, 401);
        }

        try {
            $member = Member::where('email', $params['email'])->first();
            if (empty($member)) {
                $memberDoesNotExistPayload = $this->getMemberNotFoundPayload();
                return $response->withJson($memberDoesNotExistPayload, 422);
            }

            $device_token = DeviceToken::where('email', $params['email'])->first();

            if (empty($device_token)) {
                $device_token = new DeviceToken($params);
                $device_token->save();
            } else {
                $device_token = DeviceToken::where('email', $params['email'])->update($params);
            }

            $device_token = DeviceToken::where('email', $params['email'])->first();

            return $response->withJson(['deviceToken' => $device_token], 200);

        } catch(QueryException $dbException) {
            $databaseErrorPayload = $this->getDatabaseErrorPayload('/member/device-token', $dbException);
            return $response->withJson($databaseErrorPayload, 500);
        }
    }

    private function getMemberNotFoundPayload()
    {
        $code = 422;
        $link = '/member/device-token';
        $message = 'Invalid credentials';
        $developerMessage = 'We could not find any member with the email provided';

        return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
    }

}
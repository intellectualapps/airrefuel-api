<?php
namespace pna\controllers;

use PHPHtmlParser\Dom;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;
use pna\models\PNADaily;

class PNATravelController extends BaseController {
	protected $requiredParams = ['format'];

    public function getPNATravel(Request $request, Response $response) {
        $link = $this->getPath($request);
        
        try {
            $pnaTravel = file_get_contents('https://playnetwork.africa/magazine.php?cid=3');
        } catch (QueryException $dbException) {
        	$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
        	return $response->withJson($databaseErrorPayload, 500);
        }

        $post = htmlentities($pnaTravel);
      
        if (empty($post)) {
            $noPostErrorPayload = $this->getCustomErrorPayload($link, 'There is not content at the moment.', 422,
				'PNA Travel Magazine is empty.');
            $response->withJson($noPostErrorPayload, $noPostErrorPayload['code']);
        }
        return $response->withJson(['content' => $post]);
    }
}

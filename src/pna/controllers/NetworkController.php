<?php
namespace pna\controllers;

use Illuminate\Database\QueryException;
use pna\controllers\FeedController;
use pna\controllers\message\NotificationController;
use pna\models\Connection;
use pna\models\ErrorResponsePayload;
use pna\models\Feed;
use pna\models\Member;
use pna\models\ResponsePayload;
use pna\models\Skill;
use pna\traits\ParsesHtml;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;
use pna\models\Industry;

class NetworkController extends BaseController {
	use ParsesHtml;

	protected $requiredParams = ['member-id'];

	public function __construct(Container $container) {
		parent::__construct($container);
	}

	public function getNetworkCategoriesUsers(Request $request, Response $response, $args) {
		$requestingMemberId = $args['member-id'];
		$memberIndustryId = $args['industry-id'];
		$link = $this->getPath($request);
		$skip = $request->getParam("start");
		$page_size = $request->getParam("page-size");

		try {
			$member = Member::find($requestingMemberId);
		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, 500);
		}

		if (empty($member)) {
			$memberDoesNotExistPayload = $this->getMemberDoesNotExistErrorPayload($link);
			return $response->withJson($memberDoesNotExistPayload, 422);
		}
		
		try {
			$allConnections = Connection::where("m_id", $requestingMemberId)
												->where("status", "Accepted")
												->pluck('user_id')
												->toArray();
			
			$industryConnectedMembers = Member::select('*')											
													->where("industry", $memberIndustryId)
													->whereIn("id", $allConnections)
													->get()
													->toArray();

			if (empty($allConnections)) {
				$memberConnectionDoesNotExistPayload = $this->getMemberConnectionDoesNotExistErrorPayload($link);
				return $response->withJson($memberConnectionDoesNotExistPayload, 422);
			}

			if (empty($industryConnectedMembers)) {
				$industryMemberDoesNotExistPayload = $this->getIndustryMemberDoesNotExistErrorPayload($link);
				return $response->withJson($industryMemberDoesNotExistPayload, 422);
			}

			if (!is_null($skip) and !is_null($page_size)) {
				$dataOfConnectedMembers = Member::select('*')											
												->where("industry", $memberIndustryId)
												->whereIn("id", $allConnections)
												->orderBy("industry", "ASC")
												->skip($skip)
												->take($page_size)
												->get();
			} else {
				$dataOfConnectedMembers = Member::select('*')											
												->where("industry", $memberIndustryId)
												->whereIn("id", $allConnections)
												->orderBy("industry", "ASC")
												->get();
			}

			$membersInIndustrySlimProfile = $dataOfConnectedMembers->map(
				function($member) {
					return Member::getMembersInIndustrySlimProfile($member,$this->getAssetOnDomain($member->pic));
				}
			);

			foreach ($membersInIndustrySlimProfile as $memberInIndustrySlimProfile) {
				$memberInIndustrySlimProfile['inNetwork'] = true;
			}
			
			return $response->withJson(["industryusers"=> $membersInIndustrySlimProfile], 200);
		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, 500);
		}
	}

	public function isMemberInUserNetwork($memberid, $userid) {
		try {
			$allConnections = Connection::where("user_id", $userid)
												->where("m_id", $memberid)
												->pluck('id');

			return $response->withJson(["inNetwork"=>"true", "industryusers"=>$dataOfConnectedMembers]);
		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, 500);
		}
	}

	public function getNetworkCategories(Request $request, Response $response, $args) {
		$requestingMemberId = $args['member-id'];
		$link = $this->getPath($request);
		$skip = $request->getParam("start");
		$page_size = $request->getParam("page-size");
		
		try {
			$allAcceptedConnections = Connection::select("user_id")
												->where("m_id", $requestingMemberId)
												->where("status", "Accepted")
												->get()
												->toArray();

			$industriesOfConnectedMembers = Member::select("industry")
												->distinct()
												->find($allAcceptedConnections)
												->toArray();

			if (!is_null($skip) and !is_null($page_size)) {
				$industriesDataOfConnectedMembers = Industry::orderBy("industry", "ASC")
															->skip($skip)
															->take($page_size)
															->find($industriesOfConnectedMembers)
															->toArray();
			} else {
				$industriesDataOfConnectedMembers = Industry::orderBy("industry", "ASC")
															->find($industriesOfConnectedMembers)
															->toArray();
			}

			return $response->withJson(["industries"=>$industriesDataOfConnectedMembers]);
		} catch(QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, 500);
		}
	}

	public function getNetworkMemberFullProfileWithNetworkId(Request $request, Response $response, $args) {
		$requestingMemberId = $args['member-id'];
		$networkId = $args['network-id'];
		$link = $this->getPath($request);

		try {
			$member = Member::find($requestingMemberId);
			$network = Connection::find($networkId);
		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, 500);
		}

		if (empty($member)) {
			$memberDoesNotExistPayload = $this->getMemberDoesNotExistErrorPayload($link);
			return $response->withJson($memberDoesNotExistPayload, 422);
		}

		if (empty($network)) {
			$networkDoesNotExistErrorPayload = $this->getNetworkDoesNotExistErrorPayload($link);
			return $response->withJson($networkDoesNotExistErrorPayload);
		}

		if (!$this->memberIsInNetwork($network, $requestingMemberId)) {
			$notNetworkMemberErrorPayload = $this->getNotNetworkMemberErrorPayload($link, 'requesting');
			return $response->withJson($notNetworkMemberErrorPayload);
		}

		$otherMemberId = ($network->m_id == $requestingMemberId) ? $network->user_id : $network->m_id;

		try {
			$otherMember = Member::find($otherMemberId);
		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, 500);
		}

		$experiences = $member->experience()->get();

		$experiencePayload = [];

		foreach ($experiences as $experience) {
			array_push($experiencePayload, $experience->getPayload());
		}

		$profilePicFeeds = $otherMember->feeds()->where('type', Feed::TYPE_PIC)->get();
		$otherMemberProfile = $otherMember->getPayload();
		$otherMemberProfile['pic'] = $this->appendDomain($otherMember->pic);
		$otherMemberProfile['profilePics'] = $this->getPhotoUrls($profilePicFeeds);
		$otherMemberProfile['skills'] = Skill::getCommaSeparatedValue($otherMember->skills()->get());
		$otherMemberProfile['experience'] = $experiencePayload;
		$otherMemberProfile['networkStatus'] = $network->status;

		return $response->withJson(['otherMemberProfile' => $otherMemberProfile]);
	}

	public function getNetworkMemberFullProfile(Request $request, Response $response, $args) {
		$requestingMemberId = $args['requesting-member-id'];
		$otherMemberId = $args['other-member-id'];
		$link = $this->getPath($request);

		try {
			$member = Member::find($requestingMemberId);
		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, 500);
		}

		if (empty($member)) {
			$memberDoesNotExistPayload = $this->getMemberDoesNotExistErrorPayload($link);
			return $response->withJson($memberDoesNotExistPayload, 422);
		}

		$network = $this->getNetworkWithMode($requestingMemberId);

		if (count($network) == 0) {
			$emptyNetworkErrorPayload = $this->getEmptyNetworkErrorPayload($link);
			return $response->withJson($emptyNetworkErrorPayload, 422);
		}

		$networkMember = $this->getMemberInNetwork($network, $otherMemberId);

		if (is_null($networkMember)) {
			$notNetworkMemberErrorPayload = $this->getNotNetworkMemberErrorPayload($link);
			return $response->withJson($notNetworkMemberErrorPayload, 422);
		}

		try {
			$otherMember = Member::find($otherMemberId);
		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, 500);
		}

		$experiences = $member->experience()->get();

		$experiencePayload = [];

		foreach ($experiences as $experience) {
			array_push($experiencePayload, $experience->getPayload());
		}

		$profilePicFeeds = $otherMember->feeds()->where('type', Feed::TYPE_PIC)->get();
		$otherMemberProfile = $otherMember->getPayload();
		$otherMemberProfile['pic'] = $this->appendDomain($otherMember->pic);
		$otherMemberProfile['profilePics'] = $this->getPhotoUrls($profilePicFeeds);
		$otherMemberProfile['skills'] = Skill::getCommaSeparatedValue($otherMember->skills()->get());
		$otherMemberProfile['experience'] = $experiencePayload;

		$otherMemberProfile['networkStatus'] = $networkMember->status;

		return $response->withJson(['otherMemberProfile' => $otherMemberProfile]);
	}

	protected function getMemberInNetwork($network, $memberId) {
		$member = null;

		foreach ($network as $net) {
			if ($net->m_id == $memberId || $net->user_id == $memberId) {
				$member = $net;
				break;
			}
		}

		return $member;
	}

	protected function memberIsInNetwork($network, $memberId) {
		return ($network->m_id == $memberId || $network->user_id == $memberId) ? true : false;
	}

	public function getNetwork(Request $request, Response $response, $args) {
		$mode = $request->getQueryParam('mode');
		$memberId = $args['member-id'];
		$link = $this->getPath($request);

		try {
			$member = Member::find($memberId);
		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, 500);
		}

		if (empty($member)) {
			$memberDoesNotExistPayload = $this->getMemberDoesNotExistErrorPayload($link);
			return $response->withJson($memberDoesNotExistPayload, 422);
		}

		$skip = 0;
		$page_size = 10;

		if (!is_null($request->getQueryParam('start')) && !is_null($request->getQueryParam('page-size'))) {
			$skip = $request->getQueryParam('start');
			$page_size = $request->getQueryParam('page-size');
		}

		if (empty($mode)) {
			$network = $this->getNetworkSubset($memberId, '', $skip, $page_size);
		} else {
			if (!in_array($mode, ['accepted', 'pending'])) {
				$invalidModeErrorPayload = $this->getInvalidModeErrorPayload($link);
				return $response->withJson($invalidModeErrorPayload, 422);
			}

			$network = $this->getNetworkSubset($memberId, $mode, $skip, $page_size);
		}

		$networkDetails = $this->getNetworkDetails($network, $memberId);
		return $response->withJson(['network' => $networkDetails]);
	}

	public function getNetworkRequest(Request $request, Response $response, $args) {
		$memberId = $args['member-id'];
		$link = $this->getPath($request);

		try {
			$member = Member::find($memberId);
		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, 500);
		}

		if (empty($member)) {
			$memberDoesNotExistPayload = $this->getMemberDoesNotExistErrorPayload($link);
			return $response->withJson($memberDoesNotExistPayload, 422);
		}

		$skip = 0;
		$page_size = 10;

		if (!is_null($request->getQueryParam('start')) && !is_null($request->getQueryParam('page-size'))) {
			$skip = $request->getQueryParam('start');
			$page_size = $request->getQueryParam('page-size');
		}

		$network = $this->getNetworkRequestSubset($memberId, $skip, $page_size);

		$networkDetails = $this->getNetworkDetails($network, $memberId);
		return $response->withJson(['network' => $networkDetails]);
	}

	public function updateNetworkRequest(Request $request, Response $response, $args) {
		$memberId = $request->getQueryParam('member-id');
		$networkId = $args['network-id'];
		$link = $this->getPath($request);

		if ($args['option'] === "true") {
			$option = true;
		} else {
			$option = false;
		}

		try {
			$member = Member::find($memberId);
		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, 500);
		}

		if (empty($member)) {
			$memberDoesNotExistPayload = $this->getMemberDoesNotExistErrorPayload($link);
			return $response->withJson($memberDoesNotExistPayload, 422);
		}

		try {
			$networkRequest = Connection::find($networkId);
		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, 500);
		}

		if (empty($networkRequest)) {
			$networkDoesNotExistPayload = $this->getNetworkDoesNotExistErrorPayload($link);
			return $response->withJson($networkDoesNotExistPayload, 422);
		}

		$networkDetails = [
			'id' => $networkRequest->id,
			'm_id' => $networkRequest->m_id,
			'user_id' => $member->id,
			'status' => $option,
		];

		try {
			$network = $this->updateNetwork($networkDetails, $request, $response);

			$member = Member::find($networkDetails['m_id']);
			$profilePhotoUrl = $this->getAssetOnDomain($member->pic);

			$networkDetailsResponse = ['networkId' => $networkDetails['id'], 'memberId' => $member->id,
				'firstName' => $member->fname, 'lastName' => $member->lname, 'jobTitle' => $member->job_title,
				'company' => $member->company, 'industry' => $member->industry, 'state' => $member->state,
				'profilePhotoUrl' => $profilePhotoUrl, 'hasAccepted' => $option];

			return $response->withJson(['network' => $networkDetailsResponse]);

		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, 500);
		} catch (\Exception $dbException) {
			$customErrorPayload = $this->getCustomErrorPayload($link, 'Invalid request.',
				422, 'This request has already been accepted or declined.');
			return $response->withJson($customErrorPayload, $customErrorPayload['code']);
		}
	}

	private function updateNetwork($networkDetails, $request, $response) {
		$network = Connection::where(["id" => $networkDetails['id'], "m_id" => $networkDetails["m_id"], "user_id" => $networkDetails['user_id'], "status" => "Pending"])->first();

		if (!empty($network)) {
			$member = Member::where('id', $networkDetails['user_id'])->first();
			$member_to_notify = Member::select('pnn_users.*', 'device_token.token as device_token', 'device_token.platform as device_platform', 'device_token.uuid as device_uuid')
				->where('id', $networkDetails['m_id'])
				->leftjoin('device_token', 'device_token.email', '=', 'pnn_users.email')
				->first();

			if ($networkDetails['status']) {
				$network->id = $networkDetails['id'];
				$network->m_id = $networkDetails['m_id'];
				$network->user_id = $networkDetails['user_id'];
				$network->status = "Accepted";

				$network->update();

				$newConnectionParams = [
					'm_id' => $networkDetails['user_id'],
					'user_id' => $networkDetails['m_id'],
					'status' => 'Accepted',
					'date' => date('Y-m-d h:m:s'),
				];

				$newNetwork = new Connection($newConnectionParams);
				$newNetwork->save();

				$details = [
					"type" => "accepted-network-update",
					"content" => "Your network request to $member->fname $member->lname has been accepted.",
					"title" => "Request Accepted",
					"networkId" => $network->id,
				];

				$this->notifyMember($details, $member, $network, $member_to_notify, $request, $response);

			} else {
				$network = Connection::where(['m_id' => $networkDetails['m_id'], "user_id" => $networkDetails['user_id']])->delete();

				$details = [
					"type" => "declined-network-update",
					"content" => "Your network invitation to $member->fname $member->lname has been declined.",
					"title" => "Request Declined",
					"networkId" => $networkDetails['id'],
				];

				$notification = $this->notifyMember($details, $member, $network, $member_to_notify, $request, $response);
			}

			return $network;

		} else {
			throw new \Exception('Request not found');
		}
	}

	private function notifyMember($details, $member, $network, $member_to_notify, $request, $response) {
		$notification_controller = new NotificationController($member_to_notify->device_platform);

		$networkId = $details['networkId'];
		if (!is_null($networkId)) {
			$notificationPayload = [
				"notificationType" => $details['type'],
				"networkId" => $details['networkId'],
				"memberId" => "$member->id",
				"memberName" => "$member->fname $member->lname",
				"notificationContent" => $details['content'],
				"notificationTitle" => $details['title'],
			];
		} else {
			$notificationPayload = [
				"notificationType" => $details['type'],
				"memberId" => "$member->id",
				"memberName" => "$member->fname $member->lname",
				"notificationContent" => $details['content'],
				"notificationTitle" => $details['title'],
			];
		}

		$notification = $notification_controller->sendNotification($member_to_notify['device_token'], $member_to_notify['device_platform'], $notificationPayload, $request, $response);
	}

	protected function getNetworkDetails($network, $id) {
		$networkDetails = [];
		$networkIds = [];

		foreach ($network as $net) {
			$appropriateId = ($net->m_id == $id) ? $net->user_id : $net->m_id;

			if (in_array($appropriateId, $networkIds)) {
				continue;
			}

			array_push($networkIds, $appropriateId);

			$member = Member::find($appropriateId);
			$profilePhotoUrl = $this->getAssetOnDomain($member->pic);
			$lstatus = lcfirst($net->status);

			if ($lstatus === 'pending') {
				$lstatus = false;
			} else {
				$lstatus = true;
			}

			array_push($networkDetails, ['networkId' => $net->id, 'memberId' => $appropriateId,
				'firstName' => $member->fname, 'lastName' => $member->lname, 'jobTitle' => $member->job_title,
				'company' => $member->company, 'industry' => $member->industry, 'state' => $member->state,
				'profilePhotoUrl' => $profilePhotoUrl, 'hasAccepted' => $lstatus]);
		}

		return $networkDetails;
	}

	protected function getAppropriateId($network, $id) {
		$id = ($network->m_id == $id) ? $network->user_id : $network->m_id;
		return $id;
	}

	protected function getNetworkSubset($memberId, $mode = '', $start = 0, $page_size = 10) {
		switch (strtolower($mode)) {
		case 'pending':
			$network = Connection::where(['m_id' => $memberId, 'status' => $mode])
				->orWhere(['user_id'=> $memberId, 'status' => $mode])
				->skip($start)
				->take($page_size)
				->get();
			break;

		case 'accepted':
			$network = Connection::where(['m_id' => $memberId, 'status' => $mode])
				->orWhere(['user_id'=> $memberId, 'status' => $mode])
				->skip($start)
				->take($page_size)
				->get();
			break;

		default:
			$network = Connection::where(['m_id'=> $memberId, 'status' => 'pending'])
				->orWhere(['m_id'=> $memberId, 'status' => 'accepted'])
				->orWhere(['user_id'=> $memberId, 'status'=> 'pending'])
				->orWhere(['user_id'=> $memberId, 'status' => 'accepted'])
				->skip($start)
				->take($page_size)
				->get();
			break;
		}

		return $network;
	}

	protected function getNetworkRequestSubset($memberId, $start = 0, $page_size = 10) {
		$network = Connection::where(['user_id' => $memberId, 'status' => 'Pending'])
			->skip($start)
			->take($page_size)
			->get();

		return $network;
	}

	protected function getNetworkWithMode($memberId, $mode = '') {
		switch ($mode) {
		case 'pending':
			$network = Connection::where(['m_id' => $memberId, 'status' => $mode])
				->get();
			break;

		case 'accepted':
			$network = Connection::where(['m_id' => $memberId, 'status' => $mode])
				->get();
			break;

		default:
			$network = Connection::where('m_id', $memberId)
				->get();
			break;
		}

		return $network;
	}

	public function getNetworkSuggestion(Request $request, Response $response) {
		$email = $request->getQueryParam('email');
		$link = $this->getPath($request);

		if (is_null($email)) {
			$parametersErrorPayload = ErrorResponsePayload::getParametersErrorPayload($link);
			return $response->withJson($parametersErrorPayload, $parametersErrorPayload['code']);
		}

		try {
			$member = Member::where('email', $email)->first();
			if (empty($member)) {
				$memberDoesNotExistPayload = $this->getMemberNotFoundPayload('/member/network/suggestion');
				return $response->withJson($memberDoesNotExistPayload, 422);
			}

			$skip = $request->getQueryParam('start');
			$page_size = $request->getQueryParam('page-size');
			if (!is_null($skip) && !is_null($page_size)) {
				try {
					$network_suggestion = $this->getCustomMemberNetworkSuggestion($member, $skip, $page_size);

					if (count($network_suggestion) < 1) {
						$suggestionNotFoundPayload = $this->getSuggestionNotFoundErrorPayload();
						return $response->withJson($suggestionNotFoundPayload, 422);
					} else {
						return $response->withJson(["network_suggestions" => $network_suggestion], 200);
					}
				} catch (QueryException $dbException) {
					$databaseErrorPayload = $this->getDatabaseErrorPayload('/member/network/suggestion', $dbException);
					return $response->withJson($databaseErrorPayload, 500);
				}
			} else {
				try {
					$network_suggestion = $this->getMemberNetworkSuggestion($member);

					if (count($network_suggestion) < 1) {
						$suggestionNotFoundPayload = $this->getSuggestionNotFoundErrorPayload();
						return $response->withJson($suggestionNotFoundPayload, 422);
					} else {
						return $response->withJson(["network_suggestions" => $network_suggestion], 200);
					}
				} catch (QueryException $dbException) {
					$databaseErrorPayload = $this->getDatabaseErrorPayload('/member/network/suggestion', $dbException);
					return $response->withJson($databaseErrorPayload, 500);
				}
			}

		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload('/member/network/suggestion', $dbException);
			return $response->withJson($databaseErrorPayload, 500);
		}
	}

	private function getMemberNetworkSuggestion($member) {
		$network_suggestion = Member::select(['id', 'email', 'fname', 'lname', 'job_title', 'company', 'url', 'pic'])
			->where('email', '!=', $member->email)
			->get()
			->random(8);

		return $this->getPayload($member->id, $network_suggestion);
	}

	private function getCustomMemberNetworkSuggestion($member, $skip, $page_size) {
		$network_suggestion = Member::select(['id', 'email', 'fname', 'lname', 'job_title', 'company', 'url', 'pic'])
			->where('email', '!=', $member->email)
			->skip($skip)
			->take($page_size)
			->get()
			->random($page_size);

		return $this->getPayload($member->id, $network_suggestion);
	}

	private function getPayload($memberId, $suggestions) {
		$data = [];

		foreach ($suggestions as $suggestion) {
			array_push($data, ['id' => $suggestion->id, 'email' => $suggestion->email, 'fname' => $suggestion->fname, 'lname' => $suggestion->lname, 'jobTitle' => $suggestion->job_title, 'company' => $suggestion->company, 'url' => $suggestion->url, 'pic' => $suggestion->pic, 'networkState' => FeedController::isMemberInNetwork($memberId, $suggestion->id)]);
		}
		return $data;
	}

	private function getSuggestionNotFoundErrorPayload() {
		$code = 422;
		$link = '/member/network/suggestion';
		$message = 'No suggestions';
		$developerMessage = 'We cound not suggest any connection based on user\'s skills & experience';

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
	}

	private function getMemberNotFoundPayload($link) {
		$code = 422;
		$link = $link;
		$message = 'Invalid credentials';
		$developerMessage = 'We could not find any member with the details provided';

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
	}

	public function sendNetworkInvitation(Request $request, Response $response) {
		$link = $this->getPath($request);

		$this->requiredParams = ['email', 'member-ids'];

		$params = $request->getParsedBody();

		if ($this->hasMissingRequiredParams($params)) {
			$parametersErrorPayload = $this->getParametersErrorPayload($link);
			return $response->withJson($parametersErrorPayload, 401);
		}

		try {
			$member = Member::where('email', $params['email'])->first();
			if (empty($member)) {
				$memberDoesNotExistPayload = $this->getMemberDoesNotExistErrorPayload($link);
				return $response->withJson($memberDoesNotExistPayload, 422);
			}

			$member_ids = explode(',', $params['member-ids']);

			foreach ($member_ids as $member_id) {
				try {
					$member_to_invite = Member::select('pnn_users.*', 'device_token.token as device_token', 'device_token.platform as device_platform', 'device_token.uuid as device_uuid')
						->where('id', $member_id)
						->leftjoin('device_token', 'device_token.email', '=', 'pnn_users.email')
						->first();

					if (empty($member_to_invite)) {
						$memberDoesNotExistPayload = $this->getMemberNotFoundPayload($link);
						$message = $memberDoesNotExistPayload['message'];

						continue;
					}

					$notification_controller = new NotificationController($member_to_invite['device_platform']);
					$notificationPayload = [
						"notificationType" => "network-request",
						"memberId" => "$member->id",
						"memberName" => "$member->fname $member->lname",
						"notificationContent" => "You received a network request from $member->fname $member->lname.",
						"notificationTitle" => "Network Request",
					];

					$member_connection = Connection::where(["m_id" => $member->id, "user_id" => $member_id])->first();

					if (empty($member_connection)) {
						$connection_params = [
							'm_id' => $member->id,
							'user_id' => $member_to_invite->id,
							'status' => 'Pending',
							'date' => date('Y-m-d h:m:s'),
						];

						$member_connection = new Connection($connection_params);
						$member_connection->save();

						$notificationPayload['networkId'] = $member_connection->id;

						$notification = $notification_controller->sendNotification($member_to_invite['device_token'], $member_to_invite['device_platform'], $notificationPayload, $request, $response);

						$message = 'Network invitation sent';

					} elseif ($member_connection->status === 'Pending') {
						$notificationPayload['networkId'] = $member_connection->id;

						$notification = $notification_controller->sendNotification($member_to_invite['device_token'], $member_to_invite['device_platform'], $notificationPayload, $request, $response);

						$message = 'Network invitation pending';
					} else {
						$message = 'Network invitation accepted';
						$notification = 201;
					}

					$memberInvited = [
						"networkId" => $member_connection->id,
						"memberId" => $member_to_invite->id,
						"invitationStatus" => $message,
						"pn_status" => $notification,
					];

					$membersInvited[] = $memberInvited;

				} catch (QueryException $dbException) {
					$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
					return $response->withJson($databaseErrorPayload, 500);
				}
			}

			return $response->withJson([
				'networkInvitation' => $membersInvited,
			], 200);

		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, 500);
		}
	}

	public function addMemberToNetwork(Request $request, Response $response, $arg) {
	    $link = $this->getPath($request);
	    $this->requiredParams = ['email', 'member-id'];
		$params = $request->getParsedBody();
		$email = $params["email"];
		$member_id = $params["member-id"];

		if ($this->hasMissingRequiredParams($params)) {
			$parametersErrorPayload = $this->getParametersErrorPayload($link);
			return $response->withJson($parametersErrorPayload, 401);
		}

		try {
			$memberToAdd = Member::find($member_id);
			$member =  Member::where("email", $email)->first();

			if (empty($member)) {
				$memberDoesNotExistPayload = $this->getMemberDoesNotExistErrorPayload($link);
				return $response->withJson($memberDoesNotExistPayload, 422);
			}

			if (empty($memberToAdd)) {
				$memberDoesNotExistPayload = $this->getMemberDoesNotExistErrorPayload($link);
				return $response->withJson($memberDoesNotExistPayload, 422);
			} else {
				$newConnection = [
					'm_id' => $member->id,
					'user_id' => $memberToAdd->id,
					'status' => 'Accepted',
					'date' => date('Y-m-d h:m:s')
				];

				$connectionExists = Connection::where("m_id", $member->id)
												->where("user_id", $memberToAdd->id)
												->get();

				if (count($connectionExists)) {
					return $response->withJson($this->getMemberInNetworkAlreadyErrorPayload($link), 422);
				} else {
					$created  = Connection::create($newConnection);

					return $response->withJson($this->getSucessNetworkPayload(), 201);
				}
			}
		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, 500);
		}   
	}

	public function removeMemberFromNetwork(Request $request, Response $response) {
		$link = $this->getPath($request);
    	$this->requiredParams = ['email', 'member-id'];
		$params = $request->getQueryParams();
		$email = $params["email"];
		$member_id = $params["member-id"];

		if ($this->hasMissingRequiredParams($params)) {
			$parametersErrorPayload = $this->getParametersErrorPayload($link);
			return $response->withJson($parametersErrorPayload, 401);
		}

		try {
			$memberToRemove = Member::find($member_id);
			$member =  Member::where("email", $email)->first();

			if (empty($member)) {
				$memberDoesNotExistPayload = $this->getMemberDoesNotExistErrorPayload($link);
				return $response->withJson($memberDoesNotExistPayload, 422);
			}

			if (empty($memberToRemove)) {
				$memberDoesNotExistPayload = $this->getMemberDoesNotExistErrorPayload($link);
				return $response->withJson($memberDoesNotExistPayload, 422);
			} else {
				$connection = Connection::where("m_id", $member->id)
												->where("user_id", $memberToRemove->id)
												->first();

				if (count($connection)) {
					Connection::destroy($connection->id);
					return $response->withJson($this->getSucessRemoveFromNetworkPayload(), 201);
				} else {
					return $response->withJson($this->getErrorNotInNetworkPayload($link), 404);
				}
			}
		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, 500);
		}
	}
	
	private function getErrorNotInNetworkPayload($link) {
		$code = 404;
		$link = $link;
		$message = 'Network connection not found';
		$developerMessage = "The selected member is not in the same network";

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
	}

	private function getSucessRemoveFromNetworkPayload() {
		$code = 201;
		$link = $link;
		$message = 'Card has been removed';
		$developerMessage = "Member removed from network";

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
	}

	private function getSucessNetworkPayload() {
		$code = 201;
		$link = $link;
		$message = 'Card has been added';
		$developerMessage = "Member added to network";

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
	}

	private function getMemberDoesNotExistErrorPayload($link) {
		$code = 422;
		$link = $link;
		$message = 'Card does not exist';
		$developerMessage = 'The member requested does not exist';

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
	}

	private function getIndustryMemberDoesNotExistErrorPayload($link) {
		$code = 422;
		$link = $link;
		$message = 'No cards exist in user selected indutry';
		$developerMessage = 'The user selected industry has no Member yet';

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
	}

	private function getMemberConnectionDoesNotExistErrorPayload($link) {
		$code = 422;
		$link = $link;
		$message = 'No cards exist in user connection';
		$developerMessage = 'The requesting user has no connection yet';

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
	}

	private function getMemberInNetworkAlreadyErrorPayload($link) {
		$code = 422;
		$link = $link;
		$message = 'Card already in Network';
		$developerMessage = 'The member requested Is already the same network';

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
	}

	private function getInvalidModeErrorPayload($link) {
		$code = 422;
		$link = $link;
		$message = 'Invalid mode';
		$developerMessage = 'The mode provided was not valid';

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
	}

	private function getEmptyNetworkErrorPayload($link) {
		$code = 422;
		$link = $link;
		$message = 'Empty network';
		$developerMessage = 'The requesting member has an empty network';

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
	}

	private function getNetworkDoesNotExistErrorPayload($link) {
		$code = 422;
		$link = $link;
		$message = 'Network does not exist';
		$developerMessage = 'The requested network does not exist';

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
	}

	private function getNotNetworkMemberErrorPayload($link, $whichMember = 'other') {
		$code = 422;
		$link = $link;
		$message = 'No such card in network';

		switch ($whichMember) {
		case 'requesting':
			$developerMessage = "The member provided is not on this network";
			break;

		default:
			$developerMessage = "The other member is not on the requesting member's network";
			break;
		}

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
	}

}

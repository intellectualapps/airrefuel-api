<?php
namespace pna\controllers;

use Illuminate\Database\QueryException;
use pna\controllers\BaseController;
use pna\controllers\StrippedFeedController;
use pna\models\Feed;
use pna\models\Like;
use pna\models\Member;
use pna\models\ResponsePayload;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

class LikeController extends BaseController {
	protected $requiredParams = ['email', 'feed-id'];

	public function __construct(Container $container) {
		parent::__construct($container);
	}

	public function updateFeedLike(Request $request, Response $response, $args) {
		$link = str_replace('/api/v1', '', $request->getUri()->getPath());

		$type = 'Feed';
		$date = date('Y-m-d h:m:s');

		$params = [
			"email" => $request->getParam('email'),
			"feed-id" => $args['feed-id'],
		];

		if ($this->hasMissingRequiredParams($params)) {
			$parametersErrorPayload = $this->getParametersErrorPayload($link);
			return $response->withJson($parametersErrorPayload, 401);
		}

		try {
			$feed = Feed::where('id', $params['feed-id'])->first();
			if (empty($feed)) {
				$feedDoesNotExistPayload = $this->getFeedDoesNotExistErrorPayload($link);
				return $response->withJson($feedDoesNotExistPayload, 422);
			}

			try {
				$member = Member::where('email', $params['email'])->first();
				if (empty($member)) {
					$memberDoesNotExistPayload = $this->getMemberDoesNotExistErrorPayload($link);
					return $response->withJson($memberDoesNotExistPayload, 422);
				}

				$like_details = [
					'user_id' => $member->id,
					'p_id' => $params['feed-id'],
					'type' => $type,
					'date' => $date,
				];

				try {
					$feed_like = $this->updateLike($like_details);

					return $response->withJson($feed_like, 200);

				} catch (QueryException $dbException) {
					$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
					return $response->withJson($databaseErrorPayload, 500);
				}

			} catch (QueryException $dbException) {
				$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
				return $response->withJson($databaseErrorPayload, 500);
			}

		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, 500);
		}
	}

	protected function getFeedDoesNotExistErrorPayload($link) {
		$code = 422;
		$link = $link;
		$message = 'Feed does not exist';
		$developerMessage = 'Feed with id address does not exist';

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
	}

	protected function updateLike($like_details) {
		$feed_like = Like::where(['user_id' => $like_details['user_id'], "p_id" => $like_details['p_id']])->count();

		if ($feed_like > 0) {
			$affectedRows = Like::where(['user_id' => $like_details['user_id'], "p_id" => $like_details['p_id']])->delete();

			$feed = Feed::where(['feed.id' => $like_details['p_id']])->decrement('likes', 1);
		} else {
			$feed_like = new Like($like_details);
			$feed_like->save();

			$feed = Feed::where(['feed.id' => $like_details['p_id']])->increment('likes', 1);
		}

		$feedObj = Feed::select('feed.id', 'feed.user_id', 'feed.p_id', 'feed.feed', 'feed.date', 'feed.type', 'feed.likes')
			->join('pnn_users', 'feed.user_id', '=', 'pnn_users.id')
			->where(['feed.id' => $like_details['p_id']])
			->first();

		$feedController = new StrippedFeedController($this->container);

		$feedLike["feedId"] = $feedObj->id;
		$feedLike["memberId"] = $like_details["user_id"];
		$feedLike["author"] = $feedController->getFeedAuthor($feedObj->user_id);
		$feedLike["description"] = $feedController->getFeedDescription($feedObj->type);
		$feedLike["feedAuthorPicture"] = $feedController->getFeedAuthorPicture($like_details['user_id']);
		$feedLike["content"] = $feedController->getStrippedContent($feedObj);
		$feedLike["altContent"] = $feedController->getAltContent($feedObj);
		$feedLike["type"] = $feedObj->type;
		$feedLike["likes"] = $feedObj->likes;
		$feedLike["hasUserLiked"] = $feedController->checkIfUserLiked($like_details['user_id'], $feedObj->id);
		$feedLike["feedDate"] = $feedObj->date;

		return $feedLike;
	}

	protected function getMemberDoesNotExistErrorPayload($link) {
		$code = 422;
		$link = $link;
		$message = 'Member does not exist';
		$developerMessage = 'Member with email address does not exist';

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
	}

}

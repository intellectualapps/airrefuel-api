<?php
namespace pna\controllers\managers;

use Illuminate\Database\QueryException;
use pna\controllers\message\MessageController;
use pna\controllers\AppEventController;
use pna\controllers\TransactionController;
use pna\helpers\DateTimeHelper;
use pna\models\Member;
use pna\models\AppEvent;
use pna\models\AppEventFee;
use pna\models\AppEventRSVP;
use pna\models\AppEventTicket;
use pna\models\ErrorResponsePayload;
use pna\models\ResponsePayload;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

class EventTicketManager extends TransactionController {

	public function __construct(Container $container) {
		parent::__construct($container);
    }

    public function purchaseTicket(Request $request, Response $response) {
        $link = $this->getPath($request);
        $this->requiredParams = ['email', 'event-id', 'ticket-type', 'quantity'];

        $params = $request->getParsedBody();

        if ($this->hasMissingRequiredParams($params)) {
            $parametersErrorPayload = $this->getParametersErrorPayload($link);
            return $response->withJson($parametersErrorPayload, 401);
        }

        try {
            $member = Member::where('email', $params['email'])->first();
            $event = AppEvent::findOrFail($params['event-id']);
            $paidEvent = $event->isPaidEvent();
            $eventFee = AppEventFee::select(strtolower($params['ticket-type'])."_ticket")
                ->where("event_id", $params['event-id'])
                ->first();
        } catch (QueryException $dbException) {
            $databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
            return $response->withJson($databaseErrorPayload, 500);
        } catch (ModelNotFoundException $modelException) {
			$modelNotFoundErrorPayload = ErrorResponsePayload::getModelNotFoundErrorPayload($link, $modelException);
			return $response->withJson($modelNotFoundErrorPayload, $modelNotFoundErrorPayload['code']);
		}

        if (empty($member)) {
            $memberDoesNotExistPayload = $this->getMemberDoesNotExistErrorPayload($link);
            return $response->withJson($memberDoesNotExistPayload, 422);
        }

        if (empty($event)) {
            $customErrorPayload = ErrorResponsePayload::getCustomErrorPayload(422, 'Invalid Event.', $link, 'The event does not exist.');
            return $response->withJson($customErrorPayload, $customErrorPayload['code']);
        }

        $eventDate = new DateTimeHelper($event->date . ' ' . $event->time);

        if ($eventDate->isBefore('now')) {
            $customErrorPayload = ErrorResponsePayload::getCustomErrorPayload(422, 'Invalid date.', $link, 'The event has passed.');
            return $response->withJson($customErrorPayload, $customErrorPayload['code']);
        }

        if (!$paidEvent) {
            $paidEventErrorPayload = $this->getNotPaidEventPayload($link);
            return $response->withJson($paidEventErrorPayload, 422);
        }

        if (empty($eventFee)) {
            $eventDoesNotExist = $this->getEventDoesNotExistErrorPayload($link);
            return $response->withJson($eventDoesNotExist, 422);
        }

        $email = $params["email"];
        $quantity = $params['quantity'];
        $ticketAmount = $eventFee[strtolower($params['ticket-type'])."_ticket"];
        $amount = $ticketAmount * $quantity;
        $eventId = $params["event-id"];
        $ticket_type = strtoupper($params['ticket-type']."_ticket");

        $customFields = [
            0 => [
                "display_name" => "Payment For",
                "variable_name" => "payment_purpose",
                "value" => str_replace("_", " ", $ticket_type)
            ],
            1 => [
                "display_name" => "Event ID",
                "variable_name" => "event_id",
                "value" => $eventId
            ],
            2 => [
                "display_name" => "Quantity",
                "variable_name" => "quantity",
                "value" => $quantity
            ]
        ];

        $postFields = [
            'email' => $email,
            'amount' => $amount.'00',
            'metadata' => [
                "custom_fields" => $customFields
            ]
        ];

        $payload = $this->initializeTransaction($email, $amount, $postFields);

        if (isset($payload['code'])) {
            return $response->withJson($payload, $payload['code']);
        }
        
        return $response->withJson($payload);
    }

    public function verifyTicketTransaction(Request $request, Response $response, $args) {
        $link = $this->getPath($request);
        $email = $request->getParam('email');
        $referenceCode = $args['referenceCode'];

        try {
            $member = Member::where('email', $email)->first();
        } catch (QueryException $dbException) {
            $databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
            return $response->withJson($databaseErrorPayload, 500);
        }

        if (empty($member)) {
            $memberDoesNotExistPayload = $this->getMemberDoesNotExistErrorPayload($link);
            return $response->withJson($memberDoesNotExistPayload, 422);
        }

        $payload = $this->verifyTransaction($referenceCode, $link);

        if (isset($payload['code'])) {
            return $response->withJson($payload, $payload['code']);
        }

        $memberId = $member->id;
        $ticket_type_value = $payload['transaction']['data']->custom_fields[0]->value;
        $event_id_value = $payload['transaction']['data']->custom_fields[1]->value;
        $quantity = $payload['transaction']['data']->custom_fields[2]->value;

        setlocale(LC_MONETARY, "");
        $total = str_replace("£", "₦ ", money_format('%n', substr($payload['transaction']['amount'], 0, -2)));

        switch ($ticket_type_value) {
            case 'VIP TICKET':
                $ticketType = 'VIP';
                break;
            
            case 'REGULAR TICKET':
                $ticketType = 'REGULAR';
                break;
        }

        $dateTime = new DateTimeHelper();
        $ticketAttributes = [
            "member_id" => $memberId,
            "event_id" => $event_id_value,
            "ticket_type" => $ticketType,
            "reference_code" => $referenceCode,
            "created_at" => $dateTime->format('Y-m-d H:i:s'),
            "quantity" => $quantity
        ];
        $this->addEventTicket($ticketAttributes, $link);

        $rsvpAttributes = [
			'member_id' => $memberId,
			'event_id' => $event_id_value,
        ];
        $eventRSVP = $this->addRSVP($rsvpAttributes, $link);

        $messageType = "purchase_email";
        $eventTitle = $eventRSVP['title'];
        $vars = [
            'date' => date('Y-m-d'),
            'fname' => $member->fname,
            'message' => "You have purchased $quantity $ticket_type_value(s) for $eventTitle. Please see details below:",
            'reference-code' => $referenceCode,
            'event-title' => $eventTitle,
            'purchase-description' => $ticket_type_value,
            'quantity' => $quantity,
            'total' => $total,
            'copyright-year' => $this->getCopyrightYear(),
            'address' => $this->container->settings['address']
        ];

        try {
            $messageTemplate = $this->getMessageTemplate($messageType);

            if (empty($messageTemplate)) {
                $templateNotFoundPayLoad = $this->getTemplateNotFoundPayload('/member');
                return $response->withJson($templateNotFoundPayLoad, 500);
            }

            $subject = str_replace('[{FNAME}]', $member->fname, $messageTemplate->subject);
            $message = new MessageController($messageTemplate->body, $vars);

        } catch (QueryException $dbException) {
            $databaseErrorPayload = $this->getDatabaseErrorPayload('/member', $dbException);
            return $response->withJson($databaseErrorPayload, 500);
        }

        return $response->withJson(["event" => $eventRSVP]);
    }

    private function addEventTicket($ticketAttributes, $link)
    {
        try { 
            AppEventTicket::updateOrCreate($ticketAttributes);
        } catch(QueryException $dbException){ 
            $databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
            return $databaseErrorPayload;
        }
    }

    private function addRSVP($rsvpAttributes, $link)
    {   
        try {
			AppEventRSVP::updateOrCreate($rsvpAttributes);
			$event = AppEvent::where('id', $rsvpAttributes['event_id'])->get();

			$responsePayload = AppEventController::getEventsPayload($event, $rsvpAttributes['member_id']);

			return $responsePayload[0];
		} catch (QueryException $dbException) {
			$databaseErrorPayload = ErrorResponsePayload::getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, $databaseErrorPayload['code']);
		}
    }

    private function getMemberDoesNotExistErrorPayload($link)
    {
        $code = 422;
        $link = $link;
        $message = 'Member does not exist';
        $developerMessage = 'The member requested does not exist';

        return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
    }

    private function getEventDoesNotExistErrorPayload($link)
    {
        $code = 422;
        $link = $link;
        $message = 'Event fee does not exist';
        $developerMessage = 'No ticket fees was specified for the event requested.';

        return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
    }

    private function getNotPaidEventPayload($link)
    {
        $code = 422;
        $link = $link;
        $message = 'Event is not a paid event';
        $developerMessage = 'The event requested is not a paid event.';

        return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
    }

}
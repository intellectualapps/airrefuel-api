<?php
namespace pna\controllers\managers;

use \Datetime;
use \DateInterval;
use Illuminate\Database\QueryException;
use pna\controllers\message\MessageController;
use pna\controllers\TransactionController;
use pna\helpers\DateTimeHelper;
use pna\models\Member;
use pna\models\PremiumMember;
use pna\models\ResponsePayload;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

class PremiumMembershipSubscriptionManager extends TransactionController {

	public function __construct(Container $container) {
		parent::__construct($container);
    }

    public function checkMemberSubscription(Request $request, Response $response) {
        $link = $this->getPath($request);
        $dateTime = new DateTimeHelper();
        $today = $dateTime->format('Y-m-d H:i:s');
        $premiumMembers = PremiumMember::where('authorization_token', '!=', 'null')->get();

        if (empty($premiumMembers)) {
            $payload = [
                "message" => "No premium members."
            ];
        } else {
            $count = 0;
            foreach ($premiumMembers as $premiumMember) {
                if ($today >= $premiumMember->expiry_date) {
                    $authorizationToken = $premiumMember->authorization_token;
                    $member = Member::where('id', $premiumMember->member_id)->first();
                    $email = $member->email;
                    $premiumSubscriptionAmount = $this->container->settings['premiumSubscription'];
                    $amount = $premiumSubscriptionAmount['amount'].'00';

                    $customFields = [
                        0 => [
                            "display_name" => "Payment For",
                            "variable_name" => "payment_purpose",
                            "value" => "PREMIUM MEMBERSHIP SUBSCRIPTION"
                        ]
                    ];

                    $postFields = [
                        'authorization_code' => $authorizationToken,
                        'email' => $email,
                        'amount' => $amount,
                        'metadata' => [
                            "custom_fields" => $customFields
                        ]
                    ];

                    $payload = $this->chargeAuthorization($email, $amount, $postFields);


                    $dateTime = new DateTimeHelper();
                    $messageType = "premium_membership_email";
                    $vars = [
                        'fname' => $member->fname,
                        'payment-date' => date('d\th M, Y'),
                        'expiry-date' => null,
                        'member-id' => $member->id,
                        'reference-code' => null,
                        'address' => $this->container->settings['address'],
                    ];

                    if (isset($payload['code'])) {
                        $vars['message'] = "Thank you for using PlayNetwork Africa! Your payment for <strong>Premium Membership Subscription</strong>, billed monthly failed. Please log on the app to renew your subscription.";
                    } else if ($payload['gatewayStatus'] != 'success') {
                        $vars['message'] = "Thank you for using PlayNetwork Africa! Your payment for <strong>Premium Membership Subscription</strong>, billed monthly failed. Please log on the app to renew your subscription.";
                    } else {
                        $startDate = $dateTime->format('Y-m-d');
                        $dateUpdated = $dateTime->format('Y-m-d H:i:s');
                        $reference_code = $payload['referenceCode'];

                        $nMonths = 1;
                        $expiryDate = $this->endCycle($startDate, $nMonths);

                        $this->addPremiumMember($member->id, $reference_code, $authorizationToken, $dateUpdated, $expiryDate, $link);

                        $messageType = "premium_membership_email";
                        $vars['message'] = 'Thank you for using PlayNetwork Africa! You have paid for the <strong>Premium Membership Subscription</strong>, billed monthly.';
                        $vars['payment-date'] = $dateUpdated;
                        $vars['expiry-date'] = $expiryDate;
                        $vars['reference-code'] = $reference_code;

                        $count++;
                    }

                    $messageTemplate = $this->getMessageTemplate($messageType);
                    $subject = str_replace('[{FNAME}]', $member->fname, $messageTemplate->subject);
                    $message = new MessageController($messageTemplate->body, $vars);
                }
            }

            $payload = [
                "message" => "All done and $count members have been charged."
            ];
        }

        return $response->withJson($payload);
    }

    public function subscribeMember(Request $request, Response $response) {
        $link = $this->getPath($request);
        $this->requiredParams = ['email', 'amount'];

        $params = $request->getParsedBody();

        if ($this->hasMissingRequiredParams($params)) {
            $parametersErrorPayload = $this->getParametersErrorPayload($link);
            return $response->withJson($parametersErrorPayload, 401);
        }

        try {
            $member = Member::where('email', $params['email'])->first();
        } catch (QueryException $dbException) {
            $databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
            return $response->withJson($databaseErrorPayload, 500);
        }

        if (empty($member)) {
            $memberDoesNotExistPayload = $this->getMemberDoesNotExistErrorPayload($link);
            return $response->withJson($memberDoesNotExistPayload, 422);
        }

        $email = $params["email"];
        $amount = $params["amount"].'00';

        $customFields = [
            0 => [
                "display_name" => "Payment For",
                "variable_name" => "payment_purpose",
                "value" => "PREMIUM MEMBERSHIP SUBSCRIPTION"
            ]
        ];

        $postFields = [
            'email' => $email,
            'amount' => $amount,
            'metadata' => [
                "custom_fields" => $customFields
            ]
        ];

        $payload = $this->initializeTransaction($email, $amount, $postFields);

        if (isset($payload['code'])) {
            return $response->withJson($payload, $payload['code']);
        }
        
        return $response->withJson($payload);
    }

    public function verifySubscriptionTransaction(Request $request, Response $response, $args) {
        $link = $this->getPath($request);
        $email = $request->getParam('email');
        $reference_code = $args['referenceCode'];

        try {
            $member = Member::where('email', $email)->first();
        } catch (QueryException $dbException) {
            $databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
            return $response->withJson($databaseErrorPayload, 500);
        }

        if (empty($member)) {
            $memberDoesNotExistPayload = $this->getMemberDoesNotExistErrorPayload($link);
            return $response->withJson($memberDoesNotExistPayload, 422);
        }

        $payload = $this->verifyTransaction($reference_code, $link);

        if (isset($payload['code'])) {
            return $response->withJson($payload, $payload['code']);
        }

        $dateTime = new DateTimeHelper();

        $startDate = $dateTime->format('Y-m-d');
        $dateUpdated = $dateTime->format('Y-m-d H:i:s');
        $authorizationToken = $payload['transaction']['authorizationToken'];

        $nMonths = 1;
        $expiryDate = $this->endCycle($startDate, $nMonths);

        $this->addPremiumMember($member->id, $reference_code, $authorizationToken, $dateUpdated, $expiryDate, $link);

        $premiumMember = $member->fresh()->getPayload();

        return $response->withJson(["updatedMemberProfile" => $premiumMember]);
    }

    private function addPremiumMember($memberId, $accessToken, $authorizationToken, $dateUpdated, $expiryDate, $link)
    {
        try { 
            $premiumMember = PremiumMember::updateOrCreate(['member_id' => $memberId], ['access_token' => $accessToken, 'authorization_token' => $authorizationToken, 'updated_at' => $dateUpdated, 'expiry_date' => $expiryDate]);

            return $premiumMember;
        } catch(QueryException $dbException){ 
            $databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
            return $databaseErrorPayload;
        }
    }

    private function getMemberDoesNotExistErrorPayload($link)
    {
        $code = 422;
        $link = $link;
        $message = 'Member does not exist';
        $developerMessage = 'The member requested does not exist';

        return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
    }

    private function addMonths($months, DateTime $dateObject) {
        $next = new DateTime($dateObject->format('Y-m-d H:i:s'));
        $next->modify('last day of the month + ' . $months . ' month');

        if ($dateObject->format('d') > $next->format('d')) {
            return $dateObject->diff($next);
        } else {
            return new DateInterval('P' . $months . 'M');
        }
    }

    private function endCycle($d1, $months) {
        $date = new DateTime($d1);
        $newDate = $date->add($this->addMonths($months, $date));
        $newDate->sub(new DateInterval('P1D'));
        $dateReturned = $newDate->format('Y-m-d H:i:s');

        return $dateReturned;
    }

}
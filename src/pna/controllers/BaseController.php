<?php

namespace pna\controllers;

use Firebase\JWT\JWT;
use pna\controllers\message\MailController;
use pna\models\MessageTemplate;
use pna\models\ResponsePayload;
use Slim\Container;
use Slim\Http\Request;
use Tuupola\Base62;

class BaseController {
	protected $container;
	protected $requiredParams = [];

	public function __construct(Container $container) {
		$this->container = $container;
	}

	/**
	 * Checks if there any required parameters are missing
	 *
	 * @param array $parameters The array of parameters to check.
	 * @return bool True; if there is any required parameter missing. False otherwise.
	 */
	protected function hasMissingRequiredParams(array $parameters) {
		$requiredParams = $this->requiredParams;

		$value = 0;
		if (count($requiredParams) > 0) {
			foreach ($requiredParams as $requiredParam) {
				if (!array_key_exists($requiredParam, $parameters)) {
					$value++;
				}
			}
		}

		return $value > 0;
	}

	protected function getSettingsAttribute($attr) {
		return $this->container->settings[$attr];
	}

	protected function getAssetOnDomain($assetRelativeToDomain) {
		return $this->getSettingsAttribute('assetsDomain') . '/' . $assetRelativeToDomain;
	}

	/**
	 * Generate JSON Web Token
	 *
	 * @return Array
	 */
	protected function getJWT($email) {
		$now = new \DateTime();
		$jti = (new Base62)->encode(random_bytes(16));
		$payload = [
			'iss' => $this->container->settings['domain'],
			'iat' => $now->getTimeStamp(),
			'jti' => $jti,
			'email' => $email,
		];

		$secret = $this->container->settings['jwt']['authSecret'];
		$token = JWT::encode($payload, $secret, "HS256");

		$feedback['authToken'] = $token;

		return $feedback;
	}

	/**
	 * Decode JWT
	 */
	protected function getLoggedInUserEmail() {
		$token = str_replace('Bearer ', '', $_SERVER['HTTP_AUTHORIZATION']);
		if (empty($token)) {
			return null;
		}

		$secret = $this->container->settings['jwt']['authSecret'];
		$decoded = JWT::decode($token, $secret, ["HS256", "HS512", "HS384", "RS256"]);

		return $decoded->email;
	}

	/**
	 * Database error
	 *
	 * @return Array
	 */
	protected function getDatabaseErrorPayload($link, $dbException) {
		$code = 500;
		$link = $link;
		$message = "An error occured";
		$developerMessage = $dbException->getMessage();

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
	}

	protected function getCustomErrorPayload($link, $message, $code, $devMessage) {
		$link = $link;
		$message = $message;
		$code = $code;
		$developerMessage = $devMessage;

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
	}

	/**
	 * Parameters error
	 *
	 * @return Array
	 */
	protected function getParametersErrorPayload($link) {
		$code = 401;
		$developerMessage = 'Some required parameters are missing';
		$message = 'Invalid parameters';

		switch ($link) {
		case '/authenticate':
			$message = 'Invalid login credentials';
			break;

		case '/member/reset-password':
			$message = 'Invalid credentials';
			break;

		case '/member':
			$message = 'Invalid parameters';
			break;

		case '/member/reset':
			$message = 'Invalid parameters';

		case '/member/feed':
			$message = 'Required parameter missing';

			break;

		case '/member/device-token':
			$message = 'Required parameter / authorization token missing';
			break;

		case '/member/network':
			$message = "Required parameter / authorization token missing";
			break;

		default:
			# code...
			break;
		}

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
	}

	/**
	 * Message template
	 *
	 * @return Array
	 */
	protected function getMessageTemplate($id) {
		$messageTemplate = MessageTemplate::select("subject", "body")->where(["id" => $id])->first();

		return $messageTemplate;
	}

	/**
	 * Template not found error
	 *
	 * @return Array
	 */
	protected function getTemplateNotFoundPayload($link) {
		$code = 500;
		$link = $link;
		$message = 'Template not found';
		$developerMessage = 'We could not find any template with the id provided';

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
	}

	/**
	 * Copyright year
	 *
	 * @return String
	 */
	protected function getCopyrightYear() {
		return $this->container->copyrightYear;
	}

	/**
	 * PNA Address
	 *
	 * @return String
	 */
	protected function getAddress() {
		return $this->container->address;
	}

	/**
	 * Send Mail
	 *
	 * @return null
	 */
	protected function sendMail($subject, $message, $member) {
		try {
			$mail = new MailController(true, $message);
			$mail->addAddress($member->email, $member->flname);
			$mail->Subject = $subject;
			$mail->send();
		} catch (MailerException $ex) {
			return $ex;
		}
	}

	/**
	 * Send Message error
	 *
	 * @return Array
	 */
	protected function getMessageErrorPayload($link, $exception) {
		$code = 503;

		switch ($link) {
		case '/member/reset-password':
			$message = 'Failed to send password reset link';
			$developerMessage = 'We could not send the password reset link to the email provided due to a ' . get_class($exception) . ": " . $exception->getMessage();
			break;

		case '/member':
			$message = "An error occured while sending mail";
			$link = "/member";
			$developerMessage = "Caught a " . get_class($exception) . ": " . $exception->getMessage();

		default:
			# code...
			break;
		}

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
	}

	protected function getValidator(Request $request, $rules) {
		return $this->container->validator->validate($request, $rules,
			[
				'length' => 'This field must have a length between {{minValue}} and {{maxValue}} characters',
				'positive' => 'This field must be positive',
			]);
	}

	protected function getPath(Request $request) {
		return str_replace('/api/v1', '', $request->getUri()->getPath());
	}

	protected function getEndpointUrl($name, array $parameters = []) {
		return $this->container->get('router')->pathFor($name, $parameters);
	}

	protected function hasUploaded($file) {
		return file_exists($_FILES[$file]['tmp_name']) && is_uploaded_file($_FILES[$file]['tmp_name']);
	}
}

<?php
namespace pna\controllers;

use Illuminate\Database\Capsule\Manager as DB;
use Illuminate\Database\QueryException;
use pna\helpers\DateTimeHelper;
use pna\models\Country;
use pna\models\Connection;
use pna\models\ErrorResponsePayload;
use pna\models\Feed;
use pna\models\Industry;
use pna\models\Member;
use pna\models\ResponsePayload;
use pna\models\Skill;
use pna\traits\ParsesHtml;
use Respect\Validation\Validator as Rule;
use Slim\Http\Request;
use Slim\Http\Response;

class MemberProfileController extends BaseController {
	use ParsesHtml;

	protected $requiredParams = ['email'];
	protected $createPostFor = [
		'job-title', 'company', 'industry', 'instagram', 'facebook', 'twitter', 'website',
	];

	public function getFullMemberProfile(Request $request, Response $response, $args) {
		$memberId = $args['member-id'];
		$loggedInMemberId = $request->getQueryParam("logged-in-member-id");

		$link = $this->getPath($request);

		$this->requiredParams = ['member-id'];

		if (empty($memberId)) {
			$parametersErrorPayload = ErrorResponsePayload::getParametersErrorPayload($link);
			return $response->withJson($parametersErrorPayload, $parametersErrorPayload['code']);
		}

		try {
			$member = Member::findOrFail($memberId);
			$feeds = $member->feeds()->where('type', Feed::TYPE_PIC)->get();

			$profileDetails = $member->fresh()->getPayload();

			if($loggedInMemberId)
			{
				$loggedInMemberId = Connection::where("m_id",$loggedInMemberId)
												->where("user_id", $memberId)
												->first();

				$profileDetails['inNetwork'] = count($loggedInMemberId)? true: false;
			}

			$profileDetails['profilePhoto'] = $this->appendDomain($member->pic);
			$profileDetails['profilePhotos'] = $this->getPhotoUrls($feeds);

			return $response->withJson(['profile' => $profileDetails]);
		} catch (QueryException $dbException) {
			$databaseErrorPayload = ErrorResponsePayload::getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, $databaseErrorPayload['code']);
		} catch (ModelNotFoundException $modelException) {
			$modelNotFoundErrorPayload = ErrorResponsePayload::getModelNotFoundErrorPayload($link, $modelException);
			return $response->withJson($modelNotFoundErrorPayload, $modelNotFoundErrorPayload['code']);
		}
	}

	public function updateProfile(Request $request, Response $response) {
		$profileUpdateRules = $this->getRulesForUpdatingProfile();
		$validator = $this->getValidator($request, $profileUpdateRules);
		$link = $this->getPath($request);

		if (!$validator->isValid()) {
			$validationErrorPayload = ErrorResponsePayload::getValidationErrorPayload($link, $validator);
			return $response->withJson($validationErrorPayload, $validationErrorPayload['code']);
		}

		$requestParameters = $request->getParams();
		$requestParameters = $this->removeNullValues($requestParameters);

		if ($this->hasMissingRequiredParams($requestParameters)) {
			$parametersErrorPayload = ErrorResponsePayload::getParametersErrorPayload($link);
			return $response->withJson($parametersErrorPayload, $parametersErrorPayload['code']);
		}

		try {
			$member = Member::where('email', $requestParameters['email'])->first();

			if (is_null($member)) {
				$customErrorPayload = ErrorResponsePayload::getCustomErrorPayload(422, 'Member does not exist',
					$link, 'Member does not exist');
				return $response->withJson($customErrorPayload, $customErrorPayload['code']);
			}

			if (isset($requestParameters['country-code'])) {
				$country = Country::where('sortname', $requestParameters['country-code'])->first();
				if (empty($country)) {
					$customErrorPayload = ErrorResponsePayload::getCustomErrorPayload(422, 'Country supplied is invalid',
						$link, 'Country code supplied is invalid');
					return $response->withJson($customErrorPayload, $customErrorPayload['code']);
				}
			}

			DB::transaction(function () use ($member, $requestParameters) {
				$updatedMember = $this->updateMemberProfile($requestParameters, $member);
				$updatedMember->update();

				$posts = $this->getPostsToCreate($member);
				$member->feeds()->insert($posts);
			});

			$updatedMember = $member->fresh()->getPayload();

			return $response->withJson(['updatedMemberProfile' => $updatedMember]);
		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, 500);
		}
	}

	private function getPostsToCreate(Member $member, array $params = []) {
		$postsToCreate = [];

		if (empty($params)) {
			$userId = $member->id;
			$pId = '';
			$type = 'info';
			$feed = "Has an updated profile";
			$likes = 0;
			$date = new DateTimeHelper();

			array_push($postsToCreate, array(
				'user_id' => $userId,
				'p_id' => $pId,
				'type' => $type,
				'feed' => $feed,
				'likes' => $likes,
				'date' => $date->format('Y-m-d h:m:s'),
			));
		}

		return $postsToCreate;
	}

	private function removeNullValues(array $params) {
		return array_filter($params);
	}

	private function updateMemberProfile(array $details, Member $member) {
		if (array_key_exists('first-name', $details)) {
			$member->fname = $details['first-name'];
			$member->flname = $member->fname . ' ' . $member->lname;
		}

		if (array_key_exists('last-name', $details)) {
			$member->lname = $details['last-name'];
			$member->flname = $member->fname . ' ' . $member->lname;
		}

		if (array_key_exists('phone-number', $details)) {
			$member->phone = $details['phone-number'];
		}

		if (array_key_exists('state', $details)) {
			$member->state = $details['state'];
		}

		if (array_key_exists('country-code', $details)) {
			$member->country = $details['country-code'];
		}

		if (array_key_exists('sex', $details)) {
			$member->sex = $details['sex'];
		}

		if (array_key_exists('address', $details)) {
			$member->address = $details['address'];
		}

		if (array_key_exists('instagram', $details)) {
			$member->instagram = $details['instagram'];
		}

		if (array_key_exists('facebook', $details)) {
			$member->facebook = $details['facebook'];
		}

		if (array_key_exists('twitter', $details)) {
			$member->twitter = $details['twitter'];
		}

		if (array_key_exists('website', $details)) {
			$member->website = $details['website'];
		}

		if (array_key_exists('photo-url', $details)) {
			$member->pic = $details['photo-url'];
		}

		if (array_key_exists('company', $details)) {
			$member->company = $details['company'];
		}

		if (array_key_exists('job-title', $details)) {
			$member->job_title = $details['job-title'];
		}

		return $member;
	}

	public function getSlimProfile(Request $request, Response $response) {
		$params = $request->getQueryParams();
		$link = $this->getPath($request);

		try {
			$member = Member::where('id', $params['id'])->orWhere('email', $params['id'])->first();
		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, 500);
		}

		if (empty($member)) {
			$jwtErrorPayload = $this->getMemberErrorPayload();
			return $response->withJson($jwtErrorPayload, 401);
		}

		$slimProfilePayload = $this->getSlimProfilePayload($member);
		return $response->withJson($slimProfilePayload);
	}

	private function getMemberErrorPayload() {
		$code = 401;
		$link = '/member/slim';
		$message = 'Invalid id/email';
		$developerMessage = 'No member exists with the id/email';

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
	}

	private function getSlimProfilePayload($member) {
		return [
			'memberId' => $member->id,
			'fullName' => $member->flname,
			'jobTitle' => $member->job_title,
			'company' => $member->company,
			'industry' => $member->industry,
			'state' => $member->state,
			'profilePhoto' => $this->getAssetOnDomain($member->pic),
		];
	}

	private function getRulesForUpdatingProfile() {
		return [
			'first-name' => Rule::optional(Rule::stringType(), Rule::alpha(), Rule::nullType()),
			'last-name' => Rule::optional(Rule::stringType(), Rule::alpha(), Rule::nullType()),
			'email' => Rule::optional(Rule::stringType(), Rule::email(), Rule::nullType()),
			'phone-number' => Rule::optional(Rule::stringType(), Rule::phone(), Rule::nullType()),
			"state" => Rule::optional(Rule::stringType(), Rule::alpha(), Rule::nullType()),
			"country-code" => Rule::optional(Rule::stringType()->length(1, 3), Rule::alpha(), Rule::nullType()),
			"sex" => Rule::optional(Rule::stringType(), Rule::alpha(), Rule::nullType()),
			"address" => Rule::optional(Rule::stringType(), Rule::alpha(), Rule::nullType()),
			"instagram" => Rule::optional(Rule::stringType(), Rule::url(), Rule::nullType()),
			"twitter" => Rule::optional(Rule::stringType(), Rule::url(), Rule::nullType()),
			"facebook" => Rule::optional(Rule::stringType(), Rule::url(), Rule::nullType()),
			"website" => Rule::optional(Rule::stringType(), Rule::url(), Rule::nullType()),
		];
	}
}

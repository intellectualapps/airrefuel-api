<?php
namespace pna\controllers;

use Illuminate\Database\Capsule\Manager as DB;
use Illuminate\Database\QueryException;
use pna\controllers\BaseController;
use pna\helpers\DateTimeHelper;
use pna\models\ErrorResponsePayload;
use pna\models\Experience;
use pna\models\Feed;
use pna\models\Member;
use Slim\Http\Request;
use Slim\Http\Response;

class ExperienceController extends BaseController {
	protected $requiredParams = ['position', 'employer', 'period', 'location'];

	public function createExperience(Request $request, Response $response, $args) {
		$memberId = $args['member-id'];
		$requestParams = $request->getParams();
		$link = $this->getPath($request);

		if ($this->hasMissingRequiredParams($requestParams)) {
			$parametersErrorPayload = ErrorResponsePayload::getParametersErrorPayload($link);
			return $response->withJson($parametersErrorPayload, $parametersErrorPayload['code']);
		}

		try {
			$member = Member::findOrFail($memberId);
			$experience = '';

			DB::transaction(function () use ($requestParams, $member, &$experience) {
				$date = new DateTimeHelper();

				$experienceToCreate = [
					'position' => $requestParams['position'],
					'employer' => $requestParams['employer'],
					'location' => $requestParams['location'],
					'period' => $requestParams['period'],
					'dateadded' => $date->format('l, F d, Y'),
					'date' => $date->format('Y-m-d h:i:s'),
				];

				$experience = new Experience($experienceToCreate);
				$member->experience()->save($experience);

				$feed = "posted a new job. Say congratulations! <br /><span style='font-size:14px'>"
					. $requestParams['position'] . " at " . $requestParams['employer'] . ", " . $requestParams['location'] . "</span>";

				$member->feeds()->create([
					'p_id' => '',
					'type' => Feed::TYPE_JOB,
					'feed' => $feed,
					'likes' => 0,
					'date' => $date->format('Y-m-d h:i:s'),
				]);

			});

			$experienceArray = [
				"position" => $experience->position,
				"employer" => $experience->employer,
				"location" => $experience->location,
				"period" => $experience->period,
				"dateadded" => $experience->dateadded,
				"date" => $experience->date,
				"userId" => $experience->user_id,
				"id" => $experience->id,
			];

			return $response->withJson(['experience' => $experienceArray]);
		} catch (QueryException $dbException) {
			$databaseErrorPayload = ErrorResponsePayload::getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, $databaseErrorPayload['code']);
		} catch (ModelNotFoundException $modelException) {
			$customErrorPayload = ErrorResponsePayload::getModelNotFoundErrorPayload($link, $modelException);
			return $response->withJson($customErrorPayload, $customErrorPayload['code']);
		}
	}
}
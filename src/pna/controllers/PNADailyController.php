<?php
namespace pna\controllers;

use PHPHtmlParser\Dom;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;
use pna\models\PNADaily;

class PNADailyController extends BaseController
{
	protected $requiredParams = ['format'];

    public function getPNADaily(Request $request, Response $response)
    {
    	$params = $request->getQueryParams();
        
        try {
        	$pnaDaily = PNADaily::find(1);
        } catch (QueryException $dbException) {
        	$databaseErrorPayload = $this->getDatabaseErrorPayload('/pna-daily', $dbException);
        	return $response->withJson($databaseErrorPayload, 500);
        }

        if ($this->hasMissingRequiredParams($params)) {
        	$post = htmlentities($this->addHTML($pnaDaily));
        }else{
        	switch ($params['format']) {
        		case 'div':
        			$separatePost = $this->separateNewsPost($pnaDaily->post);
                    $post = $this->groupRelatedNews($separatePost);
        			break;
        		
        		default:
        			$post = htmlentities($this->addHTML($pnaDaily));
        			break;
        	}
        }
      
        if (empty($post)) {
            $noNewsErrorPayload = $this->getNoNewsErrorPayload();
            $response->withJson($noNewsErrorPayload);
        }
        return $response->withJson(['content' => $post]);
    }

    public function getPNADailyPage(Request $request, Response $response)
    {
        $pnaDaily = PNADaily::find(1);
        $posted = $pnaDaily->posted;
        $post = $this->addAssets($pnaDaily->post);

        return $this->container->view->render($response, 'pna-daily.phtml', [
                'posted' => $posted, 'post' => $post
            ]);
    }

    private function addHTML(PNADaily $pnaDaily)
    {
        $assetsDomain = $this->getSettingsAttribute('assetsDomain');
        $post = $this->addAssets($pnaDaily->post);
        return "<!DOCTYPE html>"
        . "<html>"
        . "<head>"
            . "<meta charset='utf-8'>"
            . "<meta name='viewport' content='width=device-width, minimum-scale=1.0'>"            
            . "<title>PNA Daily | Play Network Nigeria</title>"
            . "<link href= " . $assetsDomain . "'/styles/main.css' type='text/css' rel='stylesheet'>"
            . "<link href= " . $assetsDomain . "'/styles/style.css' type='text/css' rel='stylesheet'>"
            
        . "</head>"
        . "<body>"
            . "<div id='col-main'>"
                . "<div id='col-info'>"
                	."<div padding='2%'>"
                		."<div style='color:#999;font-size:11px;padding-bottom:1%;border-bottom:1px solid #ddd;'><span style='color:#333;text-transform:uppercase'>{$pnaDaily->posted}</span>"
                		."</div>"
                		."<div>"
                			. $post
                		."</div>"
                	."</div>"
                . "</div>"
            . "</div>"
        . "</body>"
        . "</html>";
    }

    private function addAssets($post)
    {
    	return str_replace('newsletters', $this->getSettingsAttribute('assetsDomain') . '/newsletters', $post);
    }

    protected function separateNewsPost($content)
    {
        $domParser = new Dom;
        $domParser->load($content);

        $ps = str_getcsv($content, '/*div*/');

        $post = [];

        foreach ($ps as $p) {
            $strippedTags = strip_tags($p);
            $replacedChars = str_replace(['p>', 'width', '110', '>', 'div>', 'strong>', 'strong','=','div', '"'],'', $strippedTags);
            array_push($post, $replacedChars);
        }
        $post = $this->cleanContent($post);
        return $post;
    }

    protected function cleanContent($contentArr)
    {
        $cleanContent = [];
        foreach ($contentArr as $content) {
            $content = trim($content);
            if(empty($content)){
                continue;
            }elseif (strpos($content, '.jpg') == true){
                $content = $this->getSettingsAttribute('assetsDomain') . "/newsletters/" . $content;
            }
            array_push($cleanContent, $content);
        }

        return $cleanContent;
    }

    protected function groupRelatedNews($contentArr)
    {
        $newsPost = [];
        $relatedNews = [];
        $news = [];
        $count = 1;
        foreach ($contentArr as $content) {
            if (strpos($content, '.jpg') == true){
                $content = str_getcsv($content, ' ');
                $logo = $content[0];

                if($count != 1){
                    array_push($newsPost, ['logo' => $logo, 'news' => $news]);

                    $news = [];
                }
            }else{

                if($count % 2 == 0){
                    $headLine = $content;
                }else{
                    $body = $content;
                }

                if(isset($body)){
                    array_push($news, ['headLine' => $headLine, 'body' => $body]);

                    unset($body);
                }
            }
            $count++;
        }

        return $newsPost;
    }

    protected function getNoNewsErrorPayload()
    {
        $code = 422;
        $link = '/pna-daily';
        $message = 'Could not get news updates at this time';
        $developerMessage = 'The structure of the news post has changed, therefore it could not be parsed';

        return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
    }


}
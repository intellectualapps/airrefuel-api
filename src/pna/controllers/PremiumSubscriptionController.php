<?php
namespace pna\controllers;

use Slim\App;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

class PremiumSubscriptionController extends BaseController
{
  protected $requiredParams = [];
  protected $requiredConfig = ['amount'];
  protected $missingRequiredConfig = [];

  public function getPremiumSubscriptionAmount(Request $request, Response $response)
  {
    $container = $this->container;
    
    $premiumSubsctiptionAmount = $this->container->settings['premiumSubscription'];

    if (empty($premiumSubsctiptionAmount)) {
      throw new \Exception('No configuration has been set for Premium subscription');
    } else {
      if ($this->hasMissingRequiredConfig($premiumSubsctiptionAmount)) {
        $missingRequiredConfigStr = implode(', ', $this->missingRequiredConfig);
        throw new \Exception("Required config not set: " . $missingRequiredConfigStr);
      }

      return $response->withJson(['amount' => $premiumSubsctiptionAmount['amount']]);
    }
  }

  private function hasMissingRequiredConfig(array $config)
  {
    $value = 0;
    $requiredConfig = $this->requiredConfig;

    foreach ($requiredConfig as $required) {
      if (!array_key_exists($required, $config)) {
        array_push($this->missingRequiredConfig, $required);
        $value++;
      }
    }

    return $value > 0;
  }

}
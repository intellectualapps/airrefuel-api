<?php
namespace pna\controllers;

use Illuminate\Database\QueryException;
use pna\controllers\BaseController;
use pna\models\Member;
use pna\models\Transaction;
use pna\models\PremiumMember;
use Slim\App;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

class TransactionController extends BaseController
{
  protected $requiredParams = [];
  protected $requiredConfig = ['secret', 'public'];
  protected $missingRequiredConfig = [];

  public function __construct(Container $container)
  {
    parent::__construct($container);

    $paystackConfig = $this->container->settings['paystack']['test'];

    if (empty($paystackConfig)) {
      throw new \Exception('No configuration has been set for Paystack payment gateway');
    } else {
      if ($this->hasMissingRequiredConfig($paystackConfig)) {
        $missingRequiredConfigStr = implode(', ', $this->missingRequiredConfig);
        throw new \Exception("Required config not set: " . $missingRequiredConfigStr);
      }

      $this->paystackConfig = $paystackConfig;
    }
  }

  protected function initializeTransaction($email, $amount, $postFields)
  {
    try {
      $member = Member::where('email', $email)->first();
      
      $this->createCustomer($member->email, $member->fname, $member->lname, $member->phone);

      $curl = curl_init();
      $secretKey = $this->paystackConfig['secret'];

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.paystack.co/transaction/initialize",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($postFields),
        CURLOPT_HTTPHEADER => [
          "Authorization: Bearer $secretKey",
          "content-type: application/json",
          "cache-control: no-cache"
        ],
      ));

      $response_init = curl_exec($curl);
      $err = curl_error($curl);

      if ($err) {
        return [
          "code" => 400,
          "message" => "Curl returned error: - " . $err
        ];
      }

      $tranx = json_decode($response_init);

      if (!$tranx->status) {
        return [
          "code" => 400,
          "message" => "Paystack API call returned error: " . $tranx->message
        ];
      }

      $responseData['status'] = $tranx->status;
      $responseData['accessCode'] = $tranx->data->access_code;
      $responseData['referenceCode'] = $tranx->data->reference;

      $formated_amount = substr_replace($amount, ".", -2, 0);

      $this->addTransaction($responseData['referenceCode'], $email, $tranx->data->access_code, $formated_amount, '025', 'Transaction Initialized');

      return ["transaction" => $responseData];
      
    } catch (Exception $exception) {
      return [
        "code" => 400,
        "message" => $exception->getMessage()
      ];
    }
  }

  protected function verifyTransaction($reference_code, $link)
  {
    try {
      $transaction = Transaction::where('reference_code', $reference_code);
    } catch (QueryException $dbException) {
      $databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
      $databaseErrorPayload['code'] = 500;
      return $databaseErrorPayload;
    }

    if (empty($transaction)) {
      $transactionDoesNotExistErrorPayload = $this->getTransactionDoesNotExistErrorPayload($link);
      return $response->withJson($transactionDoesNotExistErrorPayload);
    }

    try {
      $curl = curl_init();
      $secretKey = $this->paystackConfig['secret'];

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.paystack.co/transaction/verify/" . rawurlencode($reference_code),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTPHEADER => [
          "accept: application/json",
          "authorization: Bearer $secretKey",
          "cache-control: no-cache"
        ],
      ));

      $curl_response = curl_exec($curl);
      $err = curl_error($curl);

      if ($err) {
        return [
          "code" => 400,
          "message" => 'Curl returned error: - ' . $err
        ];
      }

      $tranx = json_decode($curl_response);

      if (!$tranx->status) {
        return [
          "code" => 400,
          "message" => 'Paystack API call returned error: ' . $tranx->message
        ];
      }

      $transactionDetails['response_code'] = "025";
      $transactionDetails['status'] = $tranx->data->status;

      if ($tranx->data->status === 'success') {
        $transactionDetails['response_code'] = "00";
        $transactionDetails['status'] = true;
      }

      $transactionDetails['reference_code'] = $tranx->data->reference;
      $transactionDetails['response_message'] = $tranx->data->gateway_response;
      
      $this->updateTransaction($transactionDetails['reference_code'], $transactionDetails['response_code'], $transactionDetails['response_message']);

      $responseData = [
        "status" => $transactionDetails['status'],
        "referenceCode" => $transactionDetails['reference_code'],
        "authorizationToken" => $tranx->data->authorization->authorization_code,
        "responseCode" => $transactionDetails['response_code'],
        "responseMessage" => $transactionDetails['response_message'],
        "data" => $tranx->data->metadata,
        "amount" => $tranx->data->amount
      ];

      return ["transaction" => $responseData];
      
    } catch (Exception $exception) {
      return [
        "code" => 400,
        "message" => $exception->getMessage()
      ];
    }
  }

  private function createCustomer($email, $firstname, $lastname, $phone)
  {
    $parameter['first_name'] = strtoupper($firstname);
    $parameter['last_name'] = strtoupper($lastname);
    $parameter['email'] = $email;
    $parameter['phone'] = $phone;

    $customFields[0] = [
      "display_name" => "Customer Type",
      "variable_name" => "customer_type",
      "value" => "PNA Member"
    ];

    $metadata = ["custom_fields" => $customFields];

    try {
      $curl = curl_init();
      $secretKey = $this->paystackConfig['secret'];

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.paystack.co/customer",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode([
          'first_name' => $parameter['first_name'],
          'last_name' => $parameter['last_name'],
          'email' => $parameter['email'],
          'phone' => $parameter['phone'],
          'metadata' => $metadata
        ]),
        CURLOPT_HTTPHEADER => [
          "Authorization: Bearer $secretKey",
          "content-type: application/json",
          "cache-control: no-cache"
        ],
      ));

      $response_init = curl_exec($curl);
      $err = curl_error($curl);

      if ($err) {
        return [
          "code" => 400,
          "message" => "Curl returned error: - " . $err
        ];
      }

      $tranx = json_decode($response_init);

      if (!$tranx->status) {
        return [
          "code" => 400,
          "message" => "Paystack API call returned error: " . $tranx->message
        ];
      }

      $customerCode = $tranx->data->customer_code;

      $this->updateCustomer($customerCode, $email, $firstname, $lastname, $phone);
      
    } catch (Exception $exception) {

      $errorMessage = array("status" => $statuscode, "message" => $exception->getMessage());
      return $errorMessage;
    }
  }

  private function updateCustomer($customerCode, $email, $firstname, $lastname, $phone)
  {
    $parameter['first_name'] = strtoupper($firstname);
    $parameter['last_name'] = strtoupper($lastname);
    $parameter['email'] = $email;
    $parameter['phone'] = $phone;

    $customFields[0] = [
      "display_name" => "Customer Type",
      "variable_name" => "customer_type",
      "value" => "PNA Member"
    ];

    $metadata = ["custom_fields" => $customFields];

    try {
      $curl = curl_init();
      $secretKey = $this->paystackConfig['secret'];

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.paystack.co/customer/$customerCode",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST => "PUT",
        CURLOPT_POSTFIELDS => json_encode([
          'first_name' => $parameter['first_name'],
          'last_name' => $parameter['last_name'],
          'email' => $parameter['email'],
          'phone' => $parameter['phone'],
          'metadata' => $metadata
        ]),
        CURLOPT_HTTPHEADER => [
          "Authorization: Bearer $secretKey",
          "content-type: application/json",
          "cache-control: no-cache"
        ],
      ));

      $response_init = curl_exec($curl);
      $err = curl_error($curl);

      if ($err) {
        return [
          "code" => 400,
          "message" => "Curl returned error: - " . $err
        ];
      }

      $tranx = json_decode($response_init);

      if (!$tranx->status) {
        return [
          "code" => 400,
          "message" => "Paystack API call returned error: " . $tranx->message
        ];
      }
      
    } catch (Exception $exception) {

      $errorMessage = array("status" => $statuscode, "message" => $exception->getMessage());
      return $errorMessage;
    }
  }

  protected function chargeAuthorization($email, $amount, $postFields)
  {
    try {
      $curl = curl_init();
      $secretKey = $this->paystackConfig['secret'];

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.paystack.co/transaction/charge_authorization",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($postFields),
        CURLOPT_HTTPHEADER => [
          "Authorization: Bearer $secretKey",
          "content-type: application/json",
          "cache-control: no-cache"
        ],
      ));

      $response_init = curl_exec($curl);
      $err = curl_error($curl);

      if ($err) {
        return [
          "code" => 400,
          "message" => "Curl returned error: - " . $err
        ];
      }

      $tranx = json_decode($response_init);

      if (!$tranx->status) {
        return [
          "code" => 400,
          "message" => "Paystack API call returned error: " . $tranx->message
        ];
      }

      $responseData['status'] = $tranx->status;
      $responseData['message'] = $tranx->message;
      $responseData['gatewayStatus'] = $tranx->data->status;
      $responseData['gatewayResponse'] = $tranx->data->gateway_response;
      $responseData['referenceCode'] = $tranx->data->reference;

      $formated_amount = substr_replace($amount, ".", -2, 0);

      $this->addTransaction($responseData['referenceCode'], $email, $tranx->data->access_code, $formated_amount, '00', $responseData['gatewayResponse']);

      return $responseData;
      
    } catch (Exception $exception) {
      return [
        "code" => 400,
        "message" => $exception->getMessage()
      ];
    }
  }

  private function addTransaction($reference_code, $email, $access_code, $amount, $response_code, $response_message)
  {
    try { 
      $transaction = new Transaction([
        "reference_code" => $reference_code,
        "email" => $email,
        "access_code" => $access_code,
        "amount" => $amount,
        "response_code" => $response_code,
        "response_message" => $response_message
      ]);
      $transaction->save();

      return $transaction;
    } catch(QueryException $dbException){ 
      $databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
      return $databaseErrorPayload;
    }
  }

  private function updateTransaction($reference_code, $response_code, $response_message)
  {
    $transactionDetails = [
      "response_code" => $response_code,
      "response_message" => $response_message
    ];
    
    try {
      Transaction::where('reference_code', $reference_code)->update($transactionDetails);

      return Transaction::where('reference_code', $reference_code)->first();;
    } catch(QueryException $dbException){ 
      $databaseErrorPayload = $this->getDatabaseErrorPayload('/member/payment', $dbException);
      return $databaseErrorPayload;
    }
  }

  private function getTransactionDoesNotExistErrorPayload($link)
  {
    $code = 422;
    $link = $link;
    $message = 'Transaction does not exist.';
    $developerMessage = 'Transaction with reference number does not exist.';

    return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
  }

  public function getPublicKey(Request $request, Response $response)
  {
    $publicKey = $this->container->settings['paystack']['test']['public'];
    return $response->withJson(['pk' => $publicKey]);
  }
    
  private function hasMissingRequiredConfig(array $config)
  {
    $value = 0;
    $requiredConfig = $this->requiredConfig;

    foreach ($requiredConfig as $required) {
      if (!array_key_exists($required, $config)) {
        array_push($this->missingRequiredConfig, $required);
        $value++;
      }
    }

    return $value > 0;
  }

}
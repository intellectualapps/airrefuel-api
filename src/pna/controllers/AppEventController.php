<?php
namespace pna\controllers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use pna\controllers\BaseController;
use pna\helpers\DateTimeHelper;
use pna\helpers\StringTrimmer;
use pna\helpers\UniqueIdHelper;
use pna\models\AppEvent;
use pna\models\AppEventFee;
use pna\models\AppEventRSVP;
use pna\models\DeviceToken;
use pna\models\ErrorResponsePayload;
use pna\models\Member;
use pna\models\State;
use Slim\Http\Request;
use Slim\Http\Response;

class AppEventController extends BaseController {
	protected $requiredParams = [
		'title', 'state-id', 'location', 'street',
		'street-number', 'date', 'time', 'venue',
		'organizer', 'posted-by',
	];

	public function listEventsPage(Request $request, Response $response) {
		$requestParams = $request->getParams();

		$messages['error'] = $this->container->flash->getFirstMessage('error');
		$messages['success'] = $this->container->flash->getFirstMessage('success');

		$start = isset($requestParams['start']) ? $requestParams['start'] : 0;
		$pageSize = 5;

		$today = new DateTimeHelper();

		$events = AppEvent::with('state')->imminent($today->format('Y-m-d'),
			$today->format('H:i:s'))
			->skip($start)->take($pageSize)->orderBy('date', 'asc')->get();
		$eventsCount = AppEvent::with('state')->imminent($today->format('Y-m-d'),
			$today->format('H:i:s'))
			->orderBy('date', 'asc')->count();

		$eventsIndexUrl = $this->getEndpointUrl('events.index');
		$eventsSearchUrl = $this->getEndpointUrl('events.search');
		$eventsCreateUrl = $this->getEndpointUrl('events.create');
		$eventsEditUrl = $this->getEndpointUrl('events.edit', ['event-id' => '']);

		$offersIndexUrl = $this->getEndpointUrl('offers.index');

		return $this->container->view->render($response, AppEvent::TEMPLATES['index'], [
			'events' => $events, 'start' => $start, 'count' => $eventsCount, 'eventsIndex' => $eventsIndexUrl,
			'eventsSearch' => $eventsSearchUrl, 'eventsCreate' => $eventsCreateUrl,
			'eventsEdit' => $eventsEditUrl, 'offersIndex' => $offersIndexUrl, 'messages' => $messages,
		]);
	}

	public function searchEvents(Request $request, Response $response) {
		$requestParams = $request->getParams();

		$start = isset($requestParams['start']) ? $requestParams['start'] : 0;
		$pageSize = 5;

		$today = new DateTimeHelper();
		$messages = $this->container->flash->getMessages();

		switch ($requestParams['field']) {
		case 'state':
			$states = State::where('name', 'like', '%' . $requestParams['search-term'] . '%')
				->get();
			$events = [];
			foreach ($states as $state) {
				array_push($events, $this->getEventsForState($state, $today));
			}
			$events = collect($events)->flatten(1);
			break;

		case 'date':
			$date = DateTimeHelper::createFromFormat('jS, F, Y', $requestParams['search-term']);
			$events = AppEvent::with('state')->imminent($today->format('Y-m-d'),
				$today->format('H:i:s'))
				->where('date', 'like', '%' . $date->format('Y-m-d') . '%')
				->orderBy('date', 'asc')->get();
			break;

		case 'time':
			$time = DateTimeHelper::createFromFormat('h:i a', $requestParams['search-term']);
			$events = AppEvent::with('state')->imminent($today->format('Y-m-d'),
				$today->format('H:i:s'))
				->where('time', 'like', '%' . $time->format('H:i:s') . '%')
				->orderBy('date', 'asc')->get();
			break;

		default:
			$events = AppEvent::with('state')->imminent($today->format('Y-m-d'),
				$today->format('H:i:s'))
				->where($requestParams['field'], 'like', '%' . $requestParams['search-term'] . '%')
				->skip($start)->take($pageSize)->orderBy('date', 'asc')->get();
			break;
		}

		return $this->container->view->render($response, AppEvent::TEMPLATES['index'], [
			'events' => $events, 'start' => $start, 'count' => count($events),
			'messages' => $messages,
		]);
	}

	private function getEventsForState($state, DateTimeHelper $today) {
		return $state->appEvents()->with('state')
			->imminent($today->format('Y-m-d'), $today->format('H:i:s'))
			->orderBy('date', 'asc')->get();
	}

	public function createEventPage(Request $request, Response $response) {
		$states = State::orderBy('name')->get();
		$today = new DateTimeHelper();

		$messages['error'] = $this->container->flash->getFirstMessage('error');
		$messages['success'] = $this->container->flash->getFirstMessage('success');

		$eventsIndexUrl = $this->getEndpointUrl('events.index');
		$eventsCreateUrl = $this->getEndpointUrl('api.event.create');

		return $this->container->view->render($response, AppEvent::TEMPLATES['eventsForm'], [
			'states' => $states, 'today' => $today->format('Y-m-d'), 'action' => $eventsCreateUrl,
			'eventsIndex' => $eventsIndexUrl, 'messages' => $messages,
		]);
	}

	public function getEditEventPage(Request $request, Response $response, $args) {
		$eventId = $args['event-id'];
		$states = State::orderBy('name')->get();
		$today = new DateTimeHelper();
		$link = $this->getPath($request);

		$messages['error'] = $this->container->flash->getFirstMessage('error');
		$messages['success'] = $this->container->flash->getFirstMessage('success');

		try {
			$eventsIndexUrl = $this->getEndpointUrl('events.index');
			$event = AppEvent::findOrFail($eventId);
			$eventsUpdateUrl = $this->getEndpointUrl('api.event.edit', ['event-id' => $event->id]);
			$state = State::findOrFail($event->state_id);
			$tickets = $event->getEventTicket($event->id);
			$tables = $event->getEventTable($event->id);

			return $this->container->view->render($response, AppEvent::TEMPLATES['eventsForm'], [
				'states' => $states, 'today' => $today->format('Y-m-d'), 'event' => $event,
				'form' => 'edit', 'action' => $eventsUpdateUrl, 'eventsIndex' => $eventsIndexUrl,
				'messages' => $messages, 'tickets' => $tickets, 'tables' => $tables,
			]);

		} catch (QueryException $dbException) {
			$databaseErrorPayload = ErrorResponsePayload::getDatabaseErrorPayload($link, $dbException);
			$this->container->flash->addMessage('error', $databaseErrorPayload['message']);
			return $response->withRedirect($eventsIndexUrl);
		} catch (ModelNotFoundException $modelException) {
			$modelNotFoundErrorPayload = ErrorResponsePayload::getModelNotFoundErrorPayload($link, $modelException);
			$this->container->flash->addMessage('error', $modelNotFoundErrorPayload['message']);
			return $response->withRedirect($eventsIndexUrl);
		}
	}

	public function updateEvent(Request $request, Response $response, $args) {
		$eventId = $args['event-id'];
		$states = State::orderBy('name')->get();
		$today = new DateTimeHelper();
		$link = $this->getPath($request);
		$requestParams = $request->getParams();

		try {
			$attributesToUpdate = [];
			$event = AppEvent::findOrFail($eventId);
			$eventsEditUrl = $this->getEndpointUrl('events.edit', ['event-id' => $event->id]);

			if ($event->event_type != $requestParams['event-type']) {
				$event->event_type = $requestParams['event-type'];
			}

			if ($event->title != $requestParams['title']) {
				$event->title = $requestParams['title'];
			}

			if ($event->location != $requestParams['location']) {
				$event->location = $requestParams['location'];
			}

			if ($event->street != $requestParams['street']) {
				$event->street = $requestParams['street'];
			}

			if ($event->street_number != $requestParams['street-number']) {
				$event->street_number = $requestParams['street-number'];
			}

			if ($event->date != $requestParams['date']) {
				$event->date = $requestParams['date'];
			}

			if ($event->time != $requestParams['time']) {
				$event->time = $requestParams['time'];
			}

			if ($event->venue != $requestParams['venue']) {
				$event->venue = $requestParams['venue'];
			}

			if ($event->dress_code != $requestParams['dress-code']) {
				$event->dress_code = $requestParams['dress-code'];
			}

			if ($event->organizer != $requestParams['organizer']) {
				$event->organizer = $requestParams['organizer'];
			}

			if ($this->hasUploaded("pic-url")) {
				$config = $this->getSettingsAttribute('cloudinary');
				\Cloudinary::config($config);
				$uploadResponse = \Cloudinary\Uploader::upload($_FILES["pic-url"]["tmp_name"]);
				$picUrl = $uploadResponse['url'];
				$event->pic_url = $picUrl;
			}

			if ($event->info != $requestParams['info']) {
				$event->info = $requestParams['info'];
			}

			if ($event->posted_by != $requestParams['posted-by']) {
				$event->posted_by = $requestParams['posted-by'];
			}

			$event->update();
			$this->container->flash->addMessage('success', 'Update successful.');
			return $response->withRedirect($eventsEditUrl);
		} catch (QueryException $dbException) {
			$databaseErrorPayload = ErrorResponsePayload::getDatabaseErrorPayload($link, $dbException);
			$this->container->flash->addMessage('error', $databaseErrorPayload['message']);
			return $response->withRedirect($eventsEditUrl);
		} catch (ModelNotFoundException $modelException) {
			$modelNotFoundErrorPayload = ErrorResponsePayload::getModelNotFoundErrorPayload($link, $modelException);
			$this->container->flash->addMessage('error', $modelNotFoundErrorPayload['message']);
			return $response->withRedirect($eventsEditUrl);
		}
	}

	public function createEvent(Request $request, Response $response) {
		$requestParams = StringTrimmer::trimSpaces($request->getParams());
		$link = $this->getPath($request);
		$states = State::orderBy('name')->get();
		$eventsCreateUrl = $this->getEndpointUrl('events.create');

		if ($this->hasMissingRequiredParams($requestParams)) {
			$parametersErrorPayload = ErrorResponsePayload::getParametersErrorPayload($link);

			$this->container->flash->addMessage('error', $parametersErrorPayload['message']);
			return $response->withRedirect($eventsCreateUrl);
		}

		$eventDate = new DateTimeHelper($requestParams['date'] . ' ' . $requestParams['time']);

		if ($eventDate->isBefore('now')) {

			$errorMessage = 'The date provided has passed.';

			$dateTimeErrorPayload = ErrorResponsePayload::getDateErrorPayload($link);

			$this->container->flash->addMessage('error', $dateTimeErrorPayload['message']);
			return $response->withRedirect($eventsCreateUrl);
		}

		$dressCode = array_key_exists('dress-code', $requestParams) ? $requestParams['dress-code'] : '';
		$info = array_key_exists('info', $requestParams) ? $requestParams['info'] : '';
		$eventType = array_key_exists('event-type', $requestParams) ? $requestParams['event-type'] : 'regular';

		if ($this->hasUploaded("pic-url")) {
			$config = $this->getSettingsAttribute('cloudinary');
			\Cloudinary::config($config);
			$uploadResponse = \Cloudinary\Uploader::upload($_FILES["pic-url"]["tmp_name"]);
			$picUrl = $uploadResponse['url'];
		}

		$isPaidEvent = ($requestParams['regular-ticket'] > 0 || $requestParams['vip-ticket'] > 0) ? 1 : 0;
		$hasTableReservation = ($requestParams['regular-table-reservation'] > 0 || $requestParams['vip-table-reservation'] > 0) ? 1 : 0;

		$eventAttributes = [
			'id' => UniqueIdHelper::getUniqueId(),
			'title' => $requestParams['title'],
			'location' => $requestParams['location'],
			'street' => $requestParams['street'],
			'street_number' => $requestParams['street-number'],
			'date' => $eventDate->format('Y-m-d'),
			'time' => $eventDate->format('H:i:s'),
			'venue' => $requestParams['venue'],
			'dress_code' => $dressCode,
			'organizer' => $requestParams['organizer'],
			'pic_url' => $picUrl,
			'info' => $info,
			'posted_by' => $requestParams['posted-by'],
			'is_paid_event' => $isPaidEvent,
			'has_table_reservation' => $hasTableReservation,
			'event_type' => $eventType
		];

		try {
			$state = State::findOrFail($requestParams['state-id']);
			$eventCreated = $state->appEvents()->create($eventAttributes);

			$eventFees = [
				'event_id' => $eventCreated->id,
				'regular_ticket' => $requestParams['regular-ticket'],
				'vip_ticket' => $requestParams['vip-ticket'],
				'regular_table_reservation' => $requestParams['regular-table-reservation'],
				'vip_table_reservation' => $requestParams['vip-table-reservation'],
			];

			$eventCreatedFee = new AppEventFee();
			$eventCreatedFee->create($eventFees);

			$successMessage = 'Event creation was successful.';

			$this->container->flash->addMessage('success', $successMessage);
			return $response->withRedirect($eventsCreateUrl);
		} catch (QueryException $dbException) {
			$this->container->flash->addMessage('error', $databaseErrorPayload['message']);
			return $response->withRedirect($eventsCreateUrl);
		} catch (ModelNotFoundException $modelException) {
			$modelNotFoundErrorPayload = ErrorResponsePayload::getModelNotFoundErrorPayload($link, $modelException);
			$this->container->flash->addMessage('error', $modelNotFoundErrorPayload['message']);
			return $response->withRedirect($eventsEditUrl);
		}
	}

	private function notifyMembers() {
		$membersToNotify = DeviceToken::select('device.email', 'device_token.token as device_token', 'device_token.platform as device_platform', 'device_token.uuid as device_uuid', 'pnn_users.id', 'pnn_users.fname', 'pnn_users.lname')
			->leftjoin('pnn_users', 'pnn_users.email', '=', 'device_token.email')
			->first();

		foreach ($membersToNotify as $memberToNotify) {
			if (empty($memberToNotify)) {
				$memberDoesNotExistPayload = $this->getMemberNotFoundPayload($link);
				$message = $memberDoesNotExistPayload['message'];

				continue;
			}

			$date = date_format(date_create($eventCreated->date), 'l jS \of F, Y');

			$notification_controller = new NotificationController($memberToNotify->device_platform);
			$notificationPayload = [
				"notificationType" => "event-added",
				"memberId" => "$memberToNotify->id",
				"memberName" => "$memberToNotify->fname $memberToNotify->lname",
				"notificationContent" => "$eventCreated->title at $eventCreated->venue on $date. Time: $eventCreated->time",
				"notificationTitle" => "New Event by $eventCreated->organizer",
				"eventId" => $eventCreated->id,
			];

			$notification_controller->sendNotification($memberToNotify->device_token, $memberToNotify->device_platform, $notificationPayload, $request, $response);
		}
	}

	public function getEvents(Request $request, Response $response, $args) {
		$link = $this->getPath($request);
		$requestParams = StringTrimmer::trimSpaces($request->getParams());
		$this->requiredParams = ['member-id'];

		if ($this->hasMissingRequiredParams($request->getQueryParams())) {
			$parametersErrorPayload = ErrorResponsePayload::getParametersErrorPayload($link);
			return $response->withJson($parametersErrorPayload, $parametersErrorPayload['code']);
		}

		$requestParams['start'] = array_key_exists('start', $requestParams)
		? $requestParams['start'] : 0;
		$requestParams['page-size'] = array_key_exists('page-size', $requestParams)
		? $requestParams['page-size'] : 10;

		$today = new DateTimeHelper();

		try {
			$events = AppEvent::imminent($today->format('Y-m-d'), $today->format('H:i:s'))

				->skip($requestParams['start'])
				->take($requestParams['page-size'])
				->orderBy('date', 'asc')
				->get();

			$memberId = $request->getQueryParam('member-id');
			$eventsPayload = $this->getEventsPayload($events, $memberId);
			
			return $response->withJson(['events' => $eventsPayload]);
		} catch (QueryException $dbException) {
			$databaseErrorPayload = ErrorResponsePayload::getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, $databaseErrorPayload['code']);
		}
	}

	public function getEventsPayload(Collection $events, $memberId) {
		$eventPayload = [];

		foreach ($events as $event) {
			$eventDateTime = new DateTimeHelper($event->date . ' ' . $event->time);
			$eventArray = [
				'id' => $event->id,
				'title' => $event->title,
				'address' => $event->address,
				'parsedDate' => $eventDateTime->getParsedDate(),
				'date' => $event->date,
				'time' => $eventDateTime->getParsedTime(),
				'venue' => $event->venue,
				'organizer' => $event->organizer,
				'picUrl' => $event->pic_url,
				'dressCode' => $event->dress_code,
				'info' => $event->info,
				'isPaidEvent' => $event->isPaidEvent(),
				'hasRSVP' => $event->getEventMemberRSVP($memberId, $event->id),
				'hasTableReservation' => $event->hasTableReservation(),
				'hasReservedTable' => $event->getEventMemberTableReservation($memberId, $event->id),
				'numberOfRSVPs' => $event->getNumberOfRSVPs($event->id),
				'tickets' => $event->getEventTicket($event->id),
				'tables' => $event->getEventTable($event->id),
				'eventGroup' => $event->getEventGroup($eventDateTime->getParsedDate()),
				'eventType' => $event->event_type
			];

			if (!$eventArray['isPaidEvent']) {
				unset($eventArray['tickets']);
			}

			if (!$eventArray['hasTableReservation']) {
				unset($eventArray['tables']);
			}

			array_push($eventPayload, $eventArray);
		}

		return $eventPayload;
	}

	public function addRSVP(Request $request, Response $response) {
		$link = $this->getPath($request);
		$this->requiredParams = ['member-id', 'event-id'];

		$requestParams = $request->getParsedBody();

		if ($this->hasMissingRequiredParams($requestParams)) {
			$parametersErrorPayload = ErrorResponsePayload::getParametersErrorPayload($link);
			return $response->withJson($parametersErrorPayload, $parametersErrorPayload['code']);
		}

		$rsvpAttributes = [
			'member_id' => $requestParams['member-id'],
			'event_id' => $requestParams['event-id'],
		];

		try {
			$event = AppEvent::find($rsvpAttributes['event_id']);

			$eventDate = new DateTimeHelper($event->date . ' ' . $event->time);

			if ($eventDate->isBefore('now')) {
				$customErrorPayload = ErrorResponsePayload::getCustomErrorPayload(422, 'Invalid date.', $link, 'The event has passed.');
				return $response->withJson($customErrorPayload, $customErrorPayload['code']);
			}

			$rsvpEvent = AppEventRSVP::where($rsvpAttributes)->first();
			if (empty($rsvpEvent)) {
				$paidEvent = $event->isPaidEvent();
				if ($paidEvent) {
					$paidEventErrorPayload = ErrorResponsePayload::getCustomErrorPayload(422, 'Payment is required.', $link, 'The event is a paid event and requires member to purchase a ticket.');
					return $response->withJson($paidEventErrorPayload, 422);
				}

				AppEventRSVP::create($rsvpAttributes);
			} else {
				AppEventRSVP::where($rsvpAttributes)->delete();
			}

			$event = AppEvent::where('id', $rsvpAttributes['event_id'])->get();

			$responsePayload = $this->getEventsPayload($event, $rsvpAttributes['member_id']);

			return $response->withJson(["event" => $responsePayload[0]]);
		} catch (QueryException $dbException) {
			$databaseErrorPayload = ErrorResponsePayload::getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, $databaseErrorPayload['code']);
		} catch (ModelNotFoundException $modelException) {
			$modelNotFoundErrorPayload = ErrorResponsePayload::getModelNotFoundErrorPayload($link, $modelException);
			return $response->withJson($modelNotFoundErrorPayload, $modelNotFoundErrorPayload['code']);
		}
	}

	public function getEventById(Request $request, Response $response, $args) {
		$link = $this->getPath($request);
		$this->requiredParams = ['member-id', 'event-id'];

		$params = [
			"member-id" => $request->getQueryParam('member-id'),
			"event-id" => $args['event-id'],
		];

		if ($this->hasMissingRequiredParams($params)) {
			$parametersErrorPayload = ErrorResponsePayload::getParametersErrorPayload($link);
			return $response->withJson($parametersErrorPayload, $parametersErrorPayload['code']);
		}

		try {
			$member = Member::findOrFail($params['member-id']);
			$event = AppEvent::where('id', $params['event-id'])->get();
			$eventPayload = $this->getEventsPayload($event, $params['member-id']);

			return $response->withJson(['event' => $eventPayload[0]]);
		} catch (QueryException $dbException) {
			$databaseErrorPayload = ErrorResponsePayload::getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, $databaseErrorPayload['code']);
		} catch (ModelNotFoundException $modelException) {
			$customErrorPayload = ErrorResponsePayload::getModelNotFoundErrorPayload($link, $modelException);
			return $response->withJson($customErrorPayload, $customErrorPayload['code']);
		}
	}
}

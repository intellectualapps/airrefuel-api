<?php
namespace pna\controllers;

use Illuminate\Database\QueryException;
use pna\controllers\BaseController;
use pna\controllers\MemberController;
use pna\controllers\message\MessageController;
use pna\models\Member;
use pna\models\ResponsePayload;
use Respect\Validation\Validator as Rule;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

class MemberController extends BaseController {
	protected $requiredParams = ['email', 'password', 'first-name', 'last-name', 'phone-number'];

	public function __construct(Container $container) {
		parent::__construct($container);
	}

	public function addMember(Request $request, Response $response) {
		$params = $request->getParsedBody();

		if ($this->hasMissingRequiredParams($params)) {
			$parametersErrorPayload = $this->getParametersErrorPayload('/member');
			return $response->withJson($parametersErrorPayload, 401);
		}

		$memberDetails = [
			"email" => $params["email"],
			"password" => password_hash($params['password'], PASSWORD_BCRYPT),
			"fname" => $params["first-name"],
			"lname" => $params["last-name"],
			"flname" => $params["first-name"] . ' ' . $params["last-name"],
			"phone" => $params["phone-number"],
		];

		$signUpRules = $this->getRulesForSignUp();
		$link = '/member';

		$validator = $this->getValidator($request, $signUpRules);
		if (!$validator->isValid()) {
			$customErrorPayload = $this->getCustomErrorPayload($link, 'Invalid parameter(s).', 422,
				'Some parameters provided are invalid.');
			return $response->withJson($customErrorPayload, $customErrorPayload['code']);
		}

		$accountExist = $this->performSignUpAuthentication($memberDetails["email"]);
		if ($accountExist) {
			$memberExistsErrorPayload = $this->getMemberExistsErrorPayload();
			return $response->withJson($memberExistsErrorPayload, 400);
		}

		try {
			$member = new Member($memberDetails);
			$member->setSexAttribute('');
			$member->setAddressAttribute('');
			$member->setCountryAttribute('');
			$member->setStateAttribute('');
			$member->setUrlAttribute($this->getSettingsAttribute('assetsDomain'));
			$member->setPicAttribute('images/avatar.jpg');
			$member->setPicIdAttribute('');
			$member->setPicLikesAttribute(0);
			$member->setJobTitleAttribute('');
			$member->setCompanyAttribute('');
			$member->setIndustryAttribute('');
			$member->setSkillsAttribute('');
			$member->setWebsiteAttribute('');
			$member->setFacebookAttribute('');
			$member->setTwitterAttribute('');
			$member->setInstagramAttribute('');
			$member->setViewsAttribute(0);
			$member->setLastActiveAttribute('');
			$member->setStatusAttribute('Unverified');
			$member->setPrivacyAttribute('Yes');
			$member->setDateAttribute(date('Y-m-d H:i:s'));
			$member->setJoinedAttribute('');
			$member->save();

			$feedback = $this->getJWT($memberDetails["email"]);
			$feedback['member'] = $member->fresh()->getPayload();

			return $response->withJson($feedback, 200);
		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload('/member', $dbException);
			return $response->withJson($databaseErrorPayload, 500);
		}
	}

	/**
	 * More sign up endpoint
	 */
	public function completeSignUp(Request $request, Response $response) {
		$params = $request->getParsedBody();

		$this->requiredParams = ['email', 'state', 'country-code', 'job-title', 'company', 'industry'];

		if ($this->hasMissingRequiredParams($params)) {
			$parametersErrorPayload = $this->getParametersErrorPayload('/member');
			return $response->withJson($parametersErrorPayload, 422);
		}

		$memberDetails = [
			"email" => $params["email"],
			"state" => $params["state"],
			"country" => $params["country-code"],
			"job_title" => $params["job-title"],
			"company" => $params["company"],
			"industry" => $params["industry"],
		];

		$member = Member::where('email', $params['email'])->first();
		if (empty($member)) {
			$memberDoesNotExistPayload = $this->getMemberDoesNotExistErrorPayload();
			return $response->withJson($memberDoesNotExistPayload, 422);
		}

		try {
			$member->update($memberDetails);

			$messageType = "welcome_email";
			$vars = [
				'id' => $member->id,
				'fname' => $member->fname,
				'flname' => $member->flname,
				'email' => $member->email,
				'password' => $member->password,
				'copyright-year' => $this->getCopyrightYear(),
				'address' => $this->container->settings['address'],
			];

			try {
				$messageTemplate = $this->getMessageTemplate($messageType);

				if (empty($messageTemplate)) {
					$templateNotFoundPayLoad = $this->getTemplateNotFoundPayload('/member');
					return $response->withJson($templateNotFoundPayLoad, 500);
				}

				$subject = str_replace('[{FNAME}]', $member->fname, $messageTemplate->subject);
				$message = new MessageController($messageTemplate->body, $vars);

			} catch (QueryException $dbException) {
				$databaseErrorPayload = $this->getDatabaseErrorPayload('/member', $dbException);
				return $response->withJson($databaseErrorPayload, 500);
			}

			$feedback = $this->getJWT($memberDetails["email"]);
			$feedback['member'] = $member->fresh()->getPayload();
			return $response->withJson($feedback, 200);

		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload('/member', $dbException);
			return $response->withJson($databaseErrorPayload, 500);
		}
	}

	/**
	 * Retrieve validator for this entity
	 *
	 * @param  Slim\Http\Request $reponse Request to be validated
	 * @param  Array $rules Rules used to validate $data
	 * @return Awurth\SlimValidation\Validator
	 */
	protected function getValidator(Request $request, $rules) {
		return $this->container->validator->validate($request, $rules);
	}

	/**
	 * Retrieve email authentication rules
	 *
	 * @return Array
	 */
	private function getRulesForSignUp() {
		return [
			'first-name' => Rule::stringType()->length(1, null),
			'last-name' => Rule::stringType()->length(1, null),
			'email' => Rule::email(),
			'password' => Rule::stringType()->length(6, null),
			'phone-number' => Rule::stringType()->length(11, 11),
		];
	}

	private function performSignUpAuthentication($email) {
		$accountExist = Member::where(['email' => $email])->count();
		if ($accountExist > 0) {
			return true;
		}

		return false;
	}

	private function getMemberExistsErrorPayload() {
		$code = 400;
		$link = '/member';
		$message = 'Member already exists';
		$developerMessage = 'Member with email address already exists';

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
	}

	private function getMemberDoesNotExistErrorPayload() {
		$code = 422;
		$link = '/member';
		$message = 'Member does not exist';
		$developerMessage = 'Member with email address does not exist';

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
	}

	private function getFeedback($member) {
		$code = 200;
		$message = "Sign up complete";
		$link = "/member";
		$developerMessage = "Member has completed sign up";
		$data = $member;

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage, $data);
	}
}

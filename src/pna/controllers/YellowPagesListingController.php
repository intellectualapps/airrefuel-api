<?php
namespace pna\controllers;

use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use PHPHtmlParser\Dom;
use pna\helpers\UniqueIdHelper;
use pna\helpers\StringTrimmer;
use pna\models\Member;
use pna\models\State;
use pna\models\YellowPagesListing;
use pna\models\YellowPagesCategory;
use pna\models\ErrorResponsePayload;
use pna\models\ResponsePayload;
use Respect\Validation\Validator as Rule;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

class YellowPagesListingController extends BaseController
{
	protected $requiredParams = ['member-id', 'business-name', 'business-category', 'business-phone', 'business-email', 'business-address', 'business-state', 'business-details', 'business-hours', 'business-method-of-payment'];

	public function __construct(Container $container) {
		parent::__construct($container);
	}
	
	public function getAllListings(Request $request, Response $response)
	{
		$link = $this->getPath($request);
		$memberId = $request->getQueryParam('member-id');

		try {
			$member = Member::findOrFail($memberId)->first();
			$skip = $request->getQueryParam('start');
			$page_size = $request->getQueryParam('page-size');

			if (!is_null($skip) && !is_null($page_size)) {
				try {
					$yellowPages = $this->getCustomListing($skip, $page_size);

					if (count($yellowPages) < 1) {
						$yellowPages = [];
					}

					return $response->withJson(["yellowPages" => $yellowPages], 200);

				} catch (QueryException $dbException) {
					$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
					return $response->withJson($databaseErrorPayload, 500);
				}
			} else {
				try {
					$yellowPages = $this->getDefaultListing();

					if (count($yellowPages) < 1) {
						$yellowPages = [];
					}

					return $response->withJson(["yellowPages" => $yellowPages], 200);

				} catch (QueryException $dbException) {
					$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
					return $response->withJson($databaseErrorPayload, 500);
				}
			}
		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, 500);
		} catch (ModelNotFoundException $modelException) {
			$modelNotFoundErrorPayload = ErrorResponsePayload::getModelNotFoundErrorPayload($link, $modelException);
			return $response->withJson($modelNotFoundErrorPayload, $modelNotFoundErrorPayload['code']);
		}
	}

	private function getDefaultListing()
	{
		$yellowPages = YellowPagesListing::orderBy('biz_name', 'asc')
			->take(20)
			->get();

		return $this->getPayload($yellowPages);
	}

	private function getCustomListing($skip = 0, $page_size = 10)
	{
		$yellowPages = YellowPagesListing::orderBy('biz_name', 'asc')
			->skip($skip)
			->take($page_size)
			->get();

		return $this->getPayload($yellowPages);
	}

	private function getPayload($yellowPages)
  	{
		$data = [];

		foreach ($yellowPages as $yellowPage) {
			$formatedListings = [
				'id' => $yellowPage->id,
				'businessId' => $yellowPage->biz_id,
				'businessCategory' => $this->getBusinessCategoryName($yellowPage->biz_cat),
				'businessName' => $yellowPage->biz_name,
				'businessPhone' => $yellowPage->biz_phone,
				'businessAddress' => $yellowPage->biz_address,
				'businessState' => $yellowPage->biz_state,
				'businessLogo' => $this->getSettingsAttribute('assetsDomain') . "/" .$yellowPage->biz_logo
			];

			array_push($data, $formatedListings);
		}

		return $data;
	}

	public static function getBusinessCategoryName($id)
	{
		$yp_category = YellowPagesCategory::find($id);

		return $yp_category->cat_name;
	}

	public function getListingById(Request $request, Response $response, $args)
	{
		$link = $this->getPath($request);
		$listingId = $args['listing-id'];

		try {
			$yellowPage = YellowPagesListing::find($listingId);
		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, 500);
		}

		if (empty($yellowPage)) {
			$listingDoesNotExistPayload = $this->getListingDoesNotExistErrorPayload($link);
			return $response->withJson($listingDoesNotExistPayload, 422);
		}

		$yellowPage = $this->incrementPageViewCount($listingId);

		$yellowPageListing = [
			'id' => $yellowPage->id,
			'memberId' => (int) $yellowPage->user_id,
			'businessName' => $yellowPage->biz_name,
			'businessUrl' => $yellowPage->biz_url,
			'businessId' => $yellowPage->biz_id,
			'businessCategory' => $this->getBusinessCategoryName($yellowPage->biz_cat),
			'businessPhone' => $yellowPage->biz_phone,
			'otherPhones' => $yellowPage->other_phone,
			'businessEmail' => $yellowPage->biz_email,
			'businessAddress' => $yellowPage->biz_address,
			'businessState' => $yellowPage->biz_state,
			'businessDetails' => strip_tags($yellowPage->biz_details),
			'businessOffers' => ($yellowPage->biz_offers == "") ? [] : explode("\r\n", strip_tags($yellowPage->biz_offers)),
			'businessWebsite' => $yellowPage->biz_website,
			'businessTags' => ($yellowPage->biz_tags == "") ? [] : explode(', ', $yellowPage->biz_tags),
			'businessLogo' => $this->getSettingsAttribute('assetsDomain') . "/" .$yellowPage->biz_logo,
			'businessHours' => ($yellowPage->biz_hours == "") ? [] : explode("\r\n", $yellowPage->biz_hours),
			'methodsOfPayment' => ($yellowPage->biz_mop == "") ? [] : explode(', ', $yellowPage->biz_mop),
			'dateAdded' => $yellowPage->dateadded,
			'views' => $yellowPage->views
		];

		return $response->withJson(["yellowPage" => $yellowPageListing], 200);
	}

	private function getListingDoesNotExistErrorPayload($link) {
		$code = 422;
		$link = $link;
		$message = 'Listing does not exist';
		$developerMessage = 'Listing with id does not exist';

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
	}

	private function incrementPageViewCount($listingId)
	{
		YellowPagesListing::where('id', $listingId)->increment('views', 1);

		return YellowPagesListing::find($listingId);
	}

	public function createListing(Request $request, Response $response)
	{
		$link = $this->getPath($request);
		$params = $request->getParsedBody();

		if ($this->hasMissingRequiredParams($params)) {
			$parametersErrorPayload = $this->getParametersErrorPayload($link);
			return $response->withJson($parametersErrorPayload, 401);
		}

		try {
			Member::findOrFail($params['member-id']);
			YellowPagesCategory::findOrFail($params['business-category']);
			State::findOrFail($params['business-state']);
		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, 500);
		}

		$generatedId =  UniqueIdHelper::getUniqueBusinessId();
		$generatedIdExists = $this->isIdAvailable($generatedId);
		if ($generatedIdExists) {
			$memberExistsErrorPayload = $this->getMemberExistsErrorPayload();
			return $response->withJson($memberExistsErrorPayload, 400);
		}

		$createListingRules = $this->getRulesToCreateListing();
		$validator = $this->getValidator($request, $createListingRules);
		if (!$validator->isValid()) {
			$customErrorPayload = $this->getCustomErrorPayload($link, 'Invalid parameter(s).', 422,
				'Some parameters provided are invalid.');
			return $response->withJson($customErrorPayload, $customErrorPayload['code']);
		}

		$listingDetails = [
			"user_id" => $params["member-id"],
			"biz_name" => $params["business-name"],
			"biz_cat" => $params["business-category"],
			"biz_id" => $generatedId,
			"biz_phone" => $params["business-phone"],
			"biz_email" => $params["business-email"],
			"biz_address" => $params["business-address"],
			"biz_state" => $params["business-state"],
			"biz_details" => $params["business-details"],
			"biz_hours" => $params["business-hours"],
			"biz_mop" => $params["business-method-of-payment"]
		];

		try {
			$yellowPage = new YellowPagesListing($listingDetails);
			$yellowPage->setUrlAttribute((empty($params['business-url'])) ? "" : $params['business-url']);
			$yellowPage->setOtherPhoneAttribute((empty($params['other-phone'])) ? "" : $params['other-phone']);
			$yellowPage->setOffersAttribute((empty($params['business-offers'])) ? "" : $params['business-offers']);
			$yellowPage->setWebsiteAttribute((empty($params['business-website'])) ? "" : $params['business-website']);
			$yellowPage->setTagsAttribute((empty($params['business-tags'])) ? "" : $params['business-tags']);
			$yellowPage->setLogoAttribute((empty($params['business-logo'])) ? "" : $params['business-logo']);
			$yellowPage->setViewsAttribute();
			$yellowPage->setDateAttribute();
			$yellowPage->setJoinedAttribute();
			$yellowPage->save();

			$feedback['yellowPageListing'] = $yellowPage->getPayload();

			return $response->withJson($feedback, 200);
		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, 500);
		}

		return $response->withJson($listingDetails);
	}

	private function isIdAvailable($searchId)
	{
		$businessIdCount = YellowPagesListing::where('biz_id', $searchId)->count();
		if ($businessIdCount > 0) {
			return true;
		}

		return false;
	}

	private function getRulesToCreateListing() {
		return [
			'member-id' => Rule::stringType()->length(1, null),
			'business-name' => Rule::stringType()->length(1, null),
			'business-category' => Rule::stringType()->length(8, null),
			'business-phone' => Rule::stringType()->length(11, 11),
			'business-email' => Rule::email(),
			'business-address' => Rule::stringType()->length(1, null),
			'business-state' => Rule::stringType()->length(1, null),
			'business-details' => Rule::stringType()->length(1, null),
			'business-hours' => Rule::stringType()->length(1, null),
			'business-method-of-payment' => Rule::stringType()->length(1, null)
		];
	}

	public function searchListing(Request $request, Response $response, $args)
	{
		$link = $this->getPath($request);
		$searchTerm = $args['search-term'];

		$this->requiredParams = ['search-term'];

		if ($this->hasMissingRequiredParams($args)) {
			$parametersErrorPayload = $this->getParametersErrorPayload($link);
			return $response->withJson($parametersErrorPayload, 422);
		}

		try {
			$skip = $request->getQueryParam('start');
			$page_size = $request->getQueryParam('page-size');

			if (is_null($skip) && is_null($page_size)) {
				$yellowPages = $this->search($searchTerm);
			} else {
				$yellowPages = $this->search($searchTerm, $skip, $page_size);
			}

			return $response->withJson(['yellowPages' => $yellowPages], 200);
		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, 500);
		}
	}

	private function search($searchTerm, $skip = 0, $page_size = 10) {
		$searchResult = YellowPagesListing::where(function ($query) use ($searchTerm) {
			$query->where('biz_name', 'like', "%{$searchTerm}%")
				->orWhere('biz_cat', 'like', "%{$searchTerm}%")
				->orWhere('biz_phone', 'like', "%{$searchTerm}%")
				->orWhere('biz_email', 'like', '%' . str_replace(' ', '', $searchTerm) . '%')
				->orWhere('biz_address', 'like', "%{$searchTerm}%")
				->orWhere('biz_state', 'like', "%{$searchTerm}%");
			})
			->orderBy('biz_name', 'asc')
			->skip($skip)
			->take($page_size)
			->get();

		return $this->getPayload($searchResult);
	}

	public function getAllYellowPagesCategory(Request $request, Response $response) {
		$link = $this->getPath($request);
		$memberId = $request->getQueryParam('member-id');

		try {
			Member::findOrFail($memberId);
			$yellowPageCategories = YellowPagesCategory::all();

			$data = [];

			foreach ($yellowPageCategories as $yellowPageCategory) {
				$formatedListings = [
					'id' => $yellowPageCategory->id,
					'categoryId' => $yellowPageCategory->cat_id,
					'categoryName' => $yellowPageCategory->cat_name
				];

				array_push($data, $formatedListings);
			}

			return $response->withJson(["category" => $data]);
		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, 500);
		} catch (ModelNotFoundException $modelException) {
			$modelNotFoundErrorPayload = ErrorResponsePayload::getModelNotFoundErrorPayload($link, $modelException);
			return $response->withJson($modelNotFoundErrorPayload, $modelNotFoundErrorPayload['code']);
		}
	}

	public function filterYellowPages(Request $request, Response $response) {
		$link = $this->getPath($request);
		$requestParams = StringTrimmer::trimSpaces($request->getQueryParams());
		$this->requiredParams = ['member-id'];

		if ($this->hasMissingRequiredParams($request->getQueryParams())) {
			$parametersErrorPayload = ErrorResponsePayload::getParametersErrorPayload($link);
			return $response->withJson($parametersErrorPayload, $parametersErrorPayload['code']);
		}

		$memberId = $request->getQueryParam('member-id');
		$filterStates = array_key_exists('states', $requestParams) ? $requestParams['states'] : null;
		$filterCategories = array_key_exists('categories', $requestParams) ? $requestParams['categories'] : null;

		try {
			Member::findOrFail($memberId);
			$skip = array_key_exists('start', $requestParams) ? $requestParams['start'] : 0;
			$pageSize = array_key_exists('page-size', $requestParams) ? $requestParams['page-size'] : 10;

			if (is_null($filterStates)) {
				$filterResults['states'] = [];
			} else {
				$filterResults['states'] = $this->filterByState($filterStates, $skip, $pageSize);
			}
			
			if (is_null($filterCategories)) {
				$filterResults['categories'] = [];
			} else {
				$filterResults['categories'] = $this->filterByCategory($filterCategories, $skip, $pageSize);
			}

			$filterResults = call_user_func_array('array_merge', $filterResults);
			$uniqueResults = array_unique($filterResults, SORT_REGULAR);
			$payload = [];

			foreach ($uniqueResults as $uniqueResult) {
				array_push($payload, $uniqueResult);
			}

			return $response->withJson(["yellowPages" => $payload]);
		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, 500);
		} catch (ModelNotFoundException $modelException) {
			$modelNotFoundErrorPayload = ErrorResponsePayload::getModelNotFoundErrorPayload($link, $modelException);
			return $response->withJson($modelNotFoundErrorPayload, $modelNotFoundErrorPayload['code']);
		}
	}

	private function filterByCategory($filterTerms, $skip, $page_size) {
		$filterTerms = explode(",", $filterTerms);

		foreach ($filterTerms as $filterTerm) {
			$searchResult = YellowPagesListing::where("biz_cat", $filterTerm)
				->orderBy('biz_name', 'asc')
				->skip($skip)
				->take($page_size)
				->get();

			$formatedListings[] = $this->getPayload($searchResult);
		}

		return call_user_func_array('array_merge', $formatedListings);
	}

	private function filterByState($filterTerms, $skip, $page_size) {
		$filterTerms = explode(",", $filterTerms);

		foreach ($filterTerms as $filterTerm) {
			$state = State::where('id', $filterTerm)->first();

			$searchResult = YellowPagesListing::where('biz_state', 'like', "%{$state->name}%")
				->orderBy('biz_name', 'asc')
				->skip($skip)
				->take($page_size)
				->get();

			$formatedListings[] = $this->getPayload($searchResult);
		}

		return call_user_func_array('array_merge', $formatedListings);
	}
  
}
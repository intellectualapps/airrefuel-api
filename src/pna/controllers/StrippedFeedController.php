<?php
namespace pna\controllers;

use Illuminate\Database\QueryException;
use PHPHtmlParser\Dom;
use PHPHtmlParser\Exceptions\EmptyCollectionException;
use pna\models\Feed;
use pna\models\Member;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

class StrippedFeedController extends FeedController {
	public function __construct(Container $container) {
		parent::__construct($container);
	}

	/**
	 * Get All Feeds
	 */
	public function getFeed(Request $request, Response $response) {
		$email = $request->getQueryParam('email');

		if (is_null($email)) {
			$parametersErrorPayload = $this->getParametersErrorPayload('/member/feed');
			return $response->withJson($parametersErrorPayload, 401);
		}

		try {
			$member = Member::where('email', $email)->first();
			if (empty($member)) {
				$memberDoesNotExistPayload = $this->getMemberDoesNotExistErrorPayload();
				return $response->withJson($memberDoesNotExistPayload, 422);
			}

			$skip = $request->getQueryParam('start');
			$page_size = $request->getQueryParam('page-size');
			if (!is_null($skip) && !is_null($page_size)) {
				try {
					$feed = $this->getCustomFeed($member, $skip, $page_size);

					if (count($feed) < 1) {
						$feed = [];
					}

					return $response->withJson(["feed" => $feed], 200);
				} catch (QueryException $dbException) {
					$databaseErrorPayload = $this->getDatabaseErrorPayload('/member/feed', $dbException);
					return $response->withJson($databaseErrorPayload, 500);
				}
			} else {
				try {
					$feed = $this->getDefaultFeed($member);

					if (count($feed) < 1) {
						$feed = [];
					}

					return $response->withJson(["feed" => $feed], 200);
				} catch (QueryException $dbException) {
					$databaseErrorPayload = $this->getDatabaseErrorPayload('/member/feed', $dbException);
					return $response->withJson($databaseErrorPayload, 500);
				}
			}
		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload('/member/feed', $dbException);
			return $response->withJson($databaseErrorPayload, 500);
		}
	}

	protected function getDefaultFeed($member) {
		$feed = Feed::select('feed.id', 'feed.user_id', 'feed.p_id', 'feed.feed', 'feed.date', 'feed.type', 'feed.likes')
			->join('pnn_users', 'feed.user_id', '=', 'pnn_users.id')
			->where(['pnn_users.state' => $member->state])
			->orderBy('feed.date', 'desc')
			->take(20)
			->get();

		return $this->getStrippedFeedPayload($member, $feed);
	}

	protected function getStrippedFeedPayload(Member $member, $feed) {
		$newFeed = [];
		$formatted_feed = [];

		foreach ($feed as $singleFeed) {

			$formatted_feed["feedId"] = $singleFeed->id;
			$formatted_feed["memberId"] = $singleFeed->user_id;
			$formatted_feed["author"] = $this->getFeedAuthor($singleFeed->user_id);
			$formatted_feed["description"] = $this->getFeedDescription($singleFeed->type);
			$formatted_feed["feedAuthorPicture"] = $this->getFeedAuthorPicture($singleFeed->user_id);
			$formatted_feed["content"] = $this->getStrippedContent($singleFeed);
			$formatted_feed["altContent"] = $this->getAltContent($singleFeed);
			$formatted_feed["type"] = $singleFeed->type;
			if ($formatted_feed["type"] === "share") {
				$formatted_feed["shareType"] = $this->getShareType($formatted_feed);
			}
			$formatted_feed["likes"] = $singleFeed->likes;
			$formatted_feed["hasUserLiked"] = $this->checkIfUserLiked($member->id, $singleFeed->id);
			$formatted_feed["feedDate"] = $singleFeed->date;
			$formatted_feed["networkState"] = $this->isMemberInNetwork($member->id, $singleFeed->user_id);

			array_push($newFeed, $formatted_feed);
		}

		return $newFeed;
	}

	protected function getCustomFeed($member, $skip, $page_size) {
		$feed = Feed::select('feed.id', 'feed.user_id', 'feed.p_id', 'feed.feed', 'feed.date', 'feed.type', 'feed.likes')
			->join('pnn_users', 'feed.user_id', '=', 'pnn_users.id')
			->where(['pnn_users.state' => $member->state])
			->orderBy('feed.date', 'desc')
			->skip($skip)
			->take($page_size)
			->get();

		return $this->getStrippedFeedPayload($member, $feed);
	}

	public function getAltContent(Feed $feed) {
		if ($feed->type == 'share') {
			$sharedContent = $this->getAssetOnDomain($this->findElement('img', $feed->feed));
			if ($sharedContent === "http://www.playnetworknigeria.com/") {
				return '';
			} else {
				return strip_tags($feed->feed);
			}
		}

		return '';
	}

	protected function getShareType($feed) {
		if ($feed["content"] != '' && $feed["altContent"] != '') {
			return "both";
		} else if ($feed["content"] != '' && $feed["altContent"] === '') {
			$stringShared = $feed["content"];
			if (substr($stringShared, 0, 8) === "https://" || substr($stringShared, 0, 7) === "http://") {
				return "picture";
			} else {
				return "text";
			}
		}	

		return '';
	}

	public function getStrippedContent(Feed $feed) {
		switch ($feed->type) {
			case "pic":
				$content = $this->getAssetOnDomain($this->findElement('img', $feed->feed));
				break;

			case "share":
				$sharedContent = $this->getAssetOnDomain($this->findElement('img', $feed->feed));
				if ($sharedContent === "http://www.playnetworknigeria.com/") {
					$content = $feed->feed;
				} else {
					$imageUrl = $this->findElement('img', $feed->feed);
					if (substr($imageUrl, 0, 8) === "https://" || substr($imageUrl, 0, 7) === "http://") {
						$content = $imageUrl;
					} else {
						$content = $this->getAssetOnDomain($imageUrl);
					}
				}
				break;

			case "info":
				$content = $feed->feed;
				break;

			case "job":
				$content = $this->findElement('span', $feed->feed);

			default:
				$content = strip_tags($this->findElement('span', $feed->feed));
				break;
		}
		return $content;
	}

	protected function findElement($element, $content) {
		$domParser = new Dom;
		$domParser->load($content);

		if ($element == 'img') {
			$img = $domParser->find($element);

			if (count($img) > 1) {
				$a = $domParser->find('a');
				return $a[0]->href;
			}

			try {
				$src = $img->src;
				return $src;
			} catch (EmptyCollectionException $ec) {
				return "";
			}
		}

		return $domParser->find($element)->text;
	}

}
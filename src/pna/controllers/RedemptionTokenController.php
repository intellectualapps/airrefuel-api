<?php

namespace pna\controllers;

use Illuminate\Database\QueryException;
use pna\controllers\BaseController;
use pna\models\ErrorResponsePayload;
use pna\models\RedemptionToken;
use Slim\Http\Request;
use Slim\Http\Response;

class RedemptionTokenController extends BaseController {
	protected $requiredParams = ['purpose', 'token'];

	public function createRedemptionToken(Request $request, Response $response) {
		$requestParameters = $request->getParams();
		$link = $this->getPath($request);

		if ($this->hasMissingRequiredParams($requestParameters)) {
			$customErrorPayload = ErrorResponsePayload::getCustomErrorPayload(422, 'Missing parameters.',
				$link, "Some required parameters are missing.");
			return $response->withJson($customErrorPayload, $customErrorPayload['code']);
		}

		$redemptionTokenDetails = [
			'purpose' => $requestParameters['purpose'],
			'token' => $requestParameters['token'],
		];

		try {
			$previousToken = RedemptionToken::first();

			if (!empty($previousToken)) {
				$previousToken->delete();
			}

			$redemptionToken = new RedemptionToken($redemptionTokenDetails);
			$redemptionToken->save();

			return $response->withJson(['redemptionToken' => $redemptionToken]);
		} catch (QueryException $dbException) {
			$databaseErrorPayload = ErrorResponsePayload::getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, $databaseErrorPayload['code']);
		}
	}
}

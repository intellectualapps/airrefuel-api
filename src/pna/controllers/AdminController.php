<?php
namespace pna\controllers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use pna\controllers\BaseController;
use pna\helpers\DateTimeHelper;
use pna\helpers\StringTrimmer;
use pna\models\Admin;
use pna\models\ErrorResponsePayload;
use Slim\Http\Request;
use Slim\Http\Response;

class AdminController extends BaseController {
	protected $requiredParams = [
		'email', 'password'
    ];
    
    public function loginPage(Request $request, Response $response) {
		$messages['error'] = $this->container->flash->getFirstMessage('error');
        $messages['success'] = $this->container->flash->getFirstMessage('success');
        
        $eventsIndexUrl = $this->getEndpointUrl('events.index');
		$offersIndexUrl = $this->getEndpointUrl('offers.index');

		return $this->container->view->render($response, Admin::TEMPLATES['index'], [
			'eventsIndex' => $eventsIndexUrl, 'offersIndex' => $offersIndexUrl, 'messages' => $messages
		]);
    }
}
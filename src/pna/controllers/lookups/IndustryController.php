<?php
namespace pna\controllers\lookups;

use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;
use pna\controllers\BaseController;
use pna\controllers\lookups\IndustryController;
use pna\models\IndustryInterest as MemberIndustry;
use pna\models\Industry;
use pna\models\Member;
use Illuminate\Database\Capsule\Manager as DB;

class IndustryController extends BaseController
{
    protected $requiredParams = ["industry-id"];

    public function __construct(Container $container)
    {
        parent::__construct($container);
    }

    /**
     * Get All Industry
     */
    public function getAllIndustry(Request $request, Response $response)
    {
        $member_id =  $request->getQueryParam("member-id");
        $skip = $request->getQueryParam("start");
        $page_size = $request->getQueryParam("page-size");
        $link = $this->getPath($request);
        $memberIndustry = new MemberIndustry;

        if(!is_null($member_id))
        {
            $memberIndustries = $memberIndustry->getInterest($member_id);
        }
        else{
            $memberIndustries = [];
        }
        
        if(count($memberIndustries))
        {
            try
            {
                $memberIndustriesArray = $memberIndustries->lists("industry")->toArray();

                $prepareMemberIndustriesSQL =[];

                foreach ($memberIndustriesArray as $memberIndustry) {

                    if($memberIndustry !== end($memberIndustriesArray))
                    {
                        $prepareMemberIndustriesSQL[] = "'" .$memberIndustry. "',"; 
                    }
                    else{
                        $prepareMemberIndustriesSQL[] = "'" .$memberIndustry. "'"; 
                    }
                }

                $prepareMemberIndustriesSQL = implode(" ", $prepareMemberIndustriesSQL);

                $allIndustries = Industry::select("id", "industry", "image_url")
                    ->orderBy(DB::raw("case when industry in ({$prepareMemberIndustriesSQL}) then -1 else industry end, industry" ))
                    ->get();

                if(!is_null($page_size) && !is_null($skip))
                {
                    
                    $allIndustries = $allIndustries->slice($skip)->take($page_size)->values();
                    return $response->withJson(["industries"=>$allIndustries]);
                }
                else{
                    return $response->withJson(["industries"=>$allIndustries]);
                }
    
            }
            catch(Exception $dbException)
            {
                $databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
                return $response->withJson($databaseErrorPayload, 500);
            }
        }

        else{
            try{

                if(!is_null($page_size) && !is_null($skip))
                {
                    $allIndustries = Industry::select("id", "industry", "image_url")
                                                ->skip($skip)
                                                ->take($page_size)
                                                ->orderBy("industry", "ASC")
                                                ->get();

                    return $response->withJson(["industries"=>$allIndustries]);

                }
                {
                    $allIndustries = Industry::all();
                    return $response->withJson(["industries"=>$allIndustries]);
                }


            }
            catch(Exception $dbException)
            {
                $databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
                return $response->withJson($databaseErrorPayload, 500);
            }

        }
    }
        

    public function getAllMembersInIndustry(Request $request, Response $response, $args)
    {
        $industry_id =  $args["industry-id"];
        $skip = $request->getQueryParam("start");
        $page_size = $request->getQueryParam("page-size");
        $link = $this->getPath($request);

        try{

            if(!is_null($skip) && !is_null($page_size))
            {
                $membersInIndustry = Member::where("industry", $industry_id)
                                            ->orderBy("fname","ASC")
                                            ->skip($skip)
                                            ->take($page_size)
                                            ->get();            
            }
            else{
                $membersInIndustry = Member::where("industry", $industry_id)
                                            ->orderBy("fname","ASC")
                                            ->get();            
            }

            $membersInIndustrySlimProfile = $membersInIndustry->map(function($member){

                return Member::getMembersInIndustrySlimProfile($member,$this->getAssetOnDomain($member->pic));
            });

            return $response->withJson(["members"=> $membersInIndustrySlimProfile], 200);
            
        }
        catch(Exception $dbException)
        {
            $databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
            return $response->withJson($databaseErrorPayload, 500);
        }
    }


    private function getIndustryNotFoundErrorPayload()
    {
        $code = 422;
        $link = '/lookup/industry/all';
        $message = 'No industry found';
        $developerMessage = 'We cound not find any industry in the database';

        return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
    }

}
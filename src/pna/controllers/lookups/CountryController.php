<?php
namespace pna\controllers\lookups;

use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;
use Illuminate\Database\QueryException;
use pna\controllers\BaseController;
use pna\controllers\lookups\CountryController;
use pna\models\Country;

class CountryController extends BaseController
{
    public function __construct(Container $container)
    {
        parent::__construct($container);
    }

    /**
     * Get All Country
     */
    public function getAllCountry(Request $request, Response $response)
    {
        try { 
            $country_all = Country::getPayload();

            if (count($country_all) < 1) {
                $countryNotFoundPayload = $this->getCountryNotFoundErrorPayload();
                return $response->withJson($countryNotFoundPayload, 422);
            } else {
                return $response->withJson(["countries"=>$country_all], 200);
            }
        } catch(QueryException $dbException){ 
            $databaseErrorPayload = $this->getDatabaseErrorPayload('/lookup/country/all', $dbException);
            return $response->withJson($databaseErrorPayload, 500);
        }
    }

    private function getCountryNotFoundErrorPayload()
    {
        $code = 422;
        $link = '/lookup/country/all';
        $message = 'No country found';
        $developerMessage = 'We cound not find any country in the database';

        return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
    }

}
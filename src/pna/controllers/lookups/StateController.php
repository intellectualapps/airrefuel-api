<?php
namespace pna\controllers\lookups;

use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;
use Illuminate\Database\QueryException;
use pna\controllers\BaseController;
use pna\models\State;
use pna\models\ResponsePayload;

class StateController extends BaseController
{
    public function __construct(Container $container)
    {
        parent::__construct($container);
    }

    /**
     * Get All States by country
     */
    public function getStatesByCountryID(Request $request, Response $response)
    {
        $country_id = $request->getAttribute('country_id');

        try { 
            $states = State::select('id','name')->where(['country_id'=>$country_id])->get();

            if (count($states) < 1) {
                $statesNotFoundPayload = $this->getStatesNotFoundErrorPayload();
                return $response->withJson($statesNotFoundPayload, 422);
            } else {
                return $response->withJson(["states"=>$states], 200);
            }
        } catch(QueryException $dbException){ 
            $databaseErrorPayload = $this->getDatabaseErrorPayload('/lookup/states', $dbException);
            return $response->withJson($databaseErrorPayload, 500);
        }
    }

    private function getStatesNotFoundErrorPayload()
    {
        $code = 422;
        $link = '/lookup/states';
        $message = 'No states found';
        $developerMessage = 'We cound not find state with country id provided in the database';

        return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
    }

}
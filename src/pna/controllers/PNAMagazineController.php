<?php
namespace pna\controllers;

use PHPHtmlParser\Dom;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;
use pna\models\PNADaily;

class PNAMagazineController extends BaseController {
	protected $requiredParams = ['format'];

    public function getBusinessMagazine(Request $request, Response $response) {
        $link = $this->getPath($request);
        
        try {
            $pnaBusiness = file_get_contents('https://playnetwork.africa/magazine.php?cid=1');
        } catch (QueryException $dbException) {
        	$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
        	return $response->withJson($databaseErrorPayload, 500);
        }

        $post = htmlentities($pnaBusiness);
      
        if (empty($post)) {
            $noPostErrorPayload = $this->getCustomErrorPayload($link, 'There is not content at the moment.', 422,
				'PNA Business Magazine is empty.');
            $response->withJson($noPostErrorPayload, $noPostErrorPayload['code']);
        }
        return $response->withJson(['content' => $post]);
    }

    public function getLifestyleMagazine(Request $request, Response $response) {
        $link = $this->getPath($request);
        
        try {
            $pnaLifestyle = file_get_contents('https//playnetwork.africa/magazine.php?cid=2');
        } catch (QueryException $dbException) {
        	$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
        	return $response->withJson($databaseErrorPayload, 500);
        }

        $post = htmlentities($pnaLifestyle);
      
        if (empty($post)) {
            $noPostErrorPayload = $this->getCustomErrorPayload($link, 'There is not content at the moment.', 422,
				'PNA Lifestyle Magazine is empty.');
            $response->withJson($noPostErrorPayload, $noPostErrorPayload['code']);
        }
        return $response->withJson(['content' => $post]);
    }
}

<?php
namespace pna\controllers;

use Illuminate\Database\QueryException;
use pna\controllers\BaseController;
use pna\controllers\StrippedFeedController;
use pna\models\Connection;
use pna\exceptions\BaseException;
use pna\helpers\DateTimeHelper;
use pna\models\ErrorResponsePayload;
use pna\models\Feed;
use pna\models\Like;
use pna\models\Member;
use pna\models\ResponsePayload;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

class FeedController extends BaseController {
	public function __construct(Container $container) {
		parent::__construct($container);
	}

	public function addFeed(Request $request, Response $response, $args) {
		$memberId = $args['member-id'];
		$requestParams = $request->getParams();
		$link = $this->getPath($request);

		try {

			if (!array_key_exists('text', $requestParams) && !array_key_exists('picUrl', $requestParams)) {
				throw new BaseException('Nothing to share. Type some text or upload a picture.');
			}

			$feedText = (array_key_exists('text', $requestParams)) ? $requestParams['text'] : '';
			$feedPicUrl = (array_key_exists('pic-url', $requestParams)) ? $requestParams['pic-url'] : '';

			$dateTime = new DateTimeHelper();

			$member = Member::findOrFail($memberId);

			$photoCreated = null;

			if (!empty($feedPicUrl)) {
				$photoAttributes = [
					'album_id' => '',
					'p_id' => '',
					'likes' => 0,
					'photo' => $feedPicUrl,
					'caption' => $feedText,
					'date' => $dateTime->format('Y-m-d H:i:s'),
				];

				$photoCreated = $member->photos()->create($photoAttributes);
			}

			$photoId = is_null($photoCreated) ? '' : $photoCreated->id;
			$feedContent = $this->createFeedContent($feedText, $feedPicUrl);

			$feedAttributes = [
				'p_id' => $photoId,
				'type' => Feed::TYPE_SHARE,
				'feed' => $feedContent,
				'likes' => 0,
				'date' => $dateTime->format('Y-m-d H:i:s'),
			];

			$feedCreated = $member->feeds()->create($feedAttributes);

			$strippedFeedController = new StrippedFeedController($this->container);

			$feedPayload = $strippedFeedController->getStrippedFeedPayload($member, [$feedCreated]);

			return $response->withJson(['feed' => $feedPayload[0]]);
		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, $databaseErrorPayload['code']);
		} catch (ModelNotFoundException $modelException) {
			$modelNotFoundErrorPayload = ErrorResponsePayload::getModelNotFoundErrorPayload($link, $modelException);
			return $response->withJson($modelNotFoundErrorPayload, $modelNotFoundErrorPayload['code']);
		} catch (BaseException $apiException) {
			$message = 'Missing post.';
			$customErrorPayload = ErrorResponsePayload::getCustomErrorPayload(422, $message,
				$link, $apiException->getMessage());
			return $response->withJson($customErrorPayload, $customErrorPayload['code']);
		}
	}

	private function createFeedContent($feedText, $feedPicUrl) {
		if (!empty($feedText) && !empty($feedPicUrl)) {
			$feedContent = "<img onerror='this.src='images/avatar.jpg'' src = " . $feedPicUrl
				. " width = '100%' style='margin-top:2%;border:1px solid #f1f1f1;padding:2px'>"
				. $feedText;
		} elseif (!empty($feedText) && empty($feedPicUrl)) {
			$feedContent = $feedText;
		} else {
			$feedContent = "<img onerror='this.src='images/avatar.jpg'' src = " . $feedPicUrl
				. " width = '100%' style='margin-top:2%;border:1px solid #f1f1f1;padding:2px'>";
		}

		return $feedContent;
	}

	public function getFeed(Request $request, Response $response) {
		$email = $request->getQueryParam('email');

		if (is_null($email)) {
			$parametersErrorPayload = $this->getParametersErrorPayload('/member/feed');
			return $response->withJson($parametersErrorPayload, 401);
		}

		try {
			$member = Member::where('email', $email)->first();
			if (empty($member)) {
				$memberDoesNotExistPayload = $this->getMemberDoesNotExistErrorPayload();
				return $response->withJson($memberDoesNotExistPayload, 422);
			}

			$skip = $request->getQueryParam('start');
			$page_size = $request->getQueryParam('page-size');
			if (!is_null($skip) && !is_null($page_size)) {
				try {
					$feed = $this->getCustomFeed($member, $skip, $page_size);

					if (count($feed) < 1) {
						$feed = [];
					}

					return $response->withJson(["feed" => $feed], 200);

				} catch (QueryException $dbException) {
					$databaseErrorPayload = $this->getDatabaseErrorPayload('/member/feed', $dbException);
					return $response->withJson($databaseErrorPayload, 500);
				}
			} else {
				try {
					$feed = $this->getDefaultFeed($member);

					if (count($feed) < 1) {
						$feed = [];
					}

					return $response->withJson(["feed" => $feed], 200);

				} catch (QueryException $dbException) {
					$databaseErrorPayload = $this->getDatabaseErrorPayload('/member/feed', $dbException);
					return $response->withJson($databaseErrorPayload, 500);
				}
			}
		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload('/member/feed', $dbException);
			return $response->withJson($databaseErrorPayload, 500);
		}
	}

	public function getFeedDescription($feed_type) {
		switch ($feed_type) {
		case "pic":
			$feed_description = "Profile Photo Update";
			break;

		case "share":
			$feed_description = "Shared a post";
			break;

		case "info":
			$feed_description = "Profile Information Update";
			break;

		case "job":
			$feed_description = "Job update";
			break;

		default:
			$feed_description = "";
			break;
		}

		return $feed_description;
	}

	public function getFeedAuthor($user_id) {
		$member = Member::select("fname", "lname")->where("id", $user_id)->first();

		return $feed_author = $member['fname'] . ' ' . $member['lname'];
	}

	public function getFeedAuthorPicture($user_id) {
		$member = Member::select(["url", "pic"])->where("id", $user_id)->first();
		$picString = $member["pic"];
		if (substr($picString, 0, 8) === "https://" || substr($picString, 0, 7) === "http://") {
			return $member['pic'];
		} else {
			return $this->getAssetOnDomain($member['pic']);
		}
	}

	public function checkIfUserLiked($user_id, $feed_id) {
		$user_like = Like::where(['user_id' => $user_id, 'p_id' => $feed_id])->count();

		if ($user_like === 0) {
			return false;
		} else {
			return true;
		}
	}

	public static function isMemberInNetwork($memberId, $userId) {
		$connection = Connection::where(['m_id' => $memberId, 'user_id' => $userId])
			->orWhere(['m_id' => $userId, 'user_id' => $memberId])
			->first();
		if (is_null($connection)) {
			$networkState = 'none';
		} else {
			$networkState = lcfirst($connection->status);
		}
		return $networkState;
	}

	protected function getMemberDoesNotExistErrorPayload() {
		$code = 422;
		$link = '/member/feed';
		$message = 'Member does not exist';
		$developerMessage = 'Member with email address does not exist';

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
	}

	protected function getFeedPayload($feed, $user_id) {
		$newFeed = [];
		$formatted_feed = [];

		foreach ($feed as $singleFeed) {

			$formatted_feed["feedId"] = $singleFeed->id;
			$formatted_feed["memberId"] = $singleFeed->user_id;
			$formatted_feed["author"] = $this->getFeedAuthor($singleFeed->user_id);
			$formatted_feed["description"] = $this->getFeedDescription($singleFeed->type);
			$formatted_feed["feedAuthorPicture"] = $this->getFeedAuthorPicture($singleFeed->user_id);
			$formatted_feed["content"] = $singleFeed->feed;
			$formatted_feed["type"] = $singleFeed->type;
			$formatted_feed["likes"] = $singleFeed->likes;
			$formatted_feed["hasUserLiked"] = $this->checkIfUserLiked($user_id, $singleFeed->id);
			$formatted_feed["feedDate"] = $singleFeed->date;
			$formatted_feed["networkState"] = $this->isMemberInNetwork($user_id, $singleFeed->user_id);

			array_push($newFeed, $formatted_feed);
		}

		return $newFeed;
	}

	protected function getDefaultFeed($member) {
		$feed = Feed::select('feed.id', 'feed.user_id', 'feed.p_id', 'feed.feed', 'feed.date', 'feed.type', 'feed.likes')
			->join('pnn_users', 'feed.user_id', '=', 'pnn_users.id')
			->where(['pnn_users.state' => $member->state])
			->orderBy('feed.date', 'desc')
			->take(20)
			->get();

		return $this->getFeedPayload($feed, $member->id);
	}

	protected function getCustomFeed($member, $skip, $page_size) {
		$feed = Feed::select('feed.id', 'feed.user_id', 'feed.p_id', 'feed.feed', 'feed.date', 'feed.type', 'feed.likes')
			->join('pnn_users', 'feed.user_id', '=', 'pnn_users.id')
			->where(['pnn_users.state' => $member->state])
			->orderBy('feed.date', 'desc')
			->skip($skip)
			->take($page_size)
			->get();

		return $this->getFeedPayload($feed, $member->id);
	}

}

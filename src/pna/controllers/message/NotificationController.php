<?php
namespace pna\controllers\message;

use Slim\App;
use paragraph1\phpFCM\Client;
use paragraph1\phpFCM\Message;
use paragraph1\phpFCM\Recipient\Device;
use paragraph1\phpFCM\Notification;
use pna\controllers\BaseController;
use GuzzleHttp\Exception\ConnectException;

class NotificationController extends BaseController
{
    private $requiredFCMConfig = ['server_key'];
    private $requiredAPNSConfig = ['host','cert','port','pass'];
    private $missingRequiredConfig = [];

    public function __construct($platform)
    {
        chdir(dirname(__DIR__));
        $settings = require '../../../src/settings.php';
        $app = new App($settings);
        $container = $app->getContainer();

        switch ($platform) {
            case 'android':
                $fcm_config = $container->settings['fcm'];
                if (empty($fcm_config)) {
                    throw new \Exception('No configuration has been set for Firebase Cloud Messaging');
                } else {
                    if ($this->hasMissingRequiredConfig($fcm_config, 'android')) {
                        $missingRequiredConfigStr = implode(', ', $this->missingRequiredConfig);
                        throw new \Exception("Required config not set: " . $missingRequiredConfigStr);               
                    }

                    $this->server_key = $fcm_config['server_key'];
                }
                break;
            
            case 'ios':
                $apns_config = $container->settings['apns'];
                if (empty($apns_config)) {
                    throw new \Exception('No configuration has been set for Apple Push Notification Service');
                } else {
                    if ($this->hasMissingRequiredConfig($apns_config, 'ios')) {
                        $missingRequiredConfigStr = implode(', ', $this->missingRequiredConfig);
                        throw new \Exception("Required config not set: " . $missingRequiredConfigStr);               
                    }

                    $this->apns_host = $apns_config['host'];
                    $this->apns_cert = $apns_config['cert'];
                    $this->apns_port = $apns_config['port'];
                    $this->apns_pass = $apns_config['pass'];
                }
                break;
        }
    }

    private function hasMissingRequiredConfig(array $config, $platform)
    {
        $value = 0;
        switch ($platform) {
            case 'android':
                $requiredConfig = $this->requiredFCMConfig;
                break;

            case 'ios':
                $requiredConfig = $this->requiredAPNSConfig;
                break;
        }

        foreach ($requiredConfig as $required) {
            if (!array_key_exists($required, $config)) {
                array_push($this->missingRequiredConfig, $required);
                $value++;
            }
        }

        return $value > 0;
    }

    public function sendNotification($device_token, $device_platform, $payload, $request, $response)
    {
        $link = str_replace('/api/v1', '', $request->getUri()->getPath());

        $this->requiredParams = ['token', 'platform', 'payload'];

        $params = [
            "token" => $device_token,
            "platform" => $device_platform,
            "payload" => $payload
        ];

        if ($this->hasMissingRequiredParams($params)) {
            $parametersErrorPayload = $this->getParametersErrorPayload($link);
            return $response->withJson($parametersErrorPayload, 401);
        }

        return $this->send($device_platform, $device_token, $payload);
    }

    public function sendNetworkNotification(Request $request, Response $response)
    {
        $link = str_replace('/api/v1', '', $request->getUri()->getPath());

        $this->requiredParams = ['token', 'platform', 'title', 'body'];

        $params = $request->getParsedBody();

        if ($this->hasMissingRequiredParams($params)) {
            $parametersErrorPayload = $this->getParametersErrorPayload($link);
            return $response->withJson($parametersErrorPayload, 401);
        }
        
        return $this->send($params->title, $params->body, $params->token);
    }

    private function send($platform, $token, $payload)
    {
        switch ($platform) {
            case 'android':
                $apiKey = $this->server_key;
                $client = new Client();
                $client->setApiKey($apiKey);
                $client->injectHttpClient(new \GuzzleHttp\Client());

                $message = new Message();
                $message->addRecipient(new Device($token));
                $message->setPriority('high');
                $message->setData($payload);

                try {
                    $response = $client->send($message);
                    return $response->getStatusCode();
                } catch(ConnectException $connectException) {
                    return $connectException->getMessage();
                }
                break;

            case 'ios':
                $apnsHost = $this->apns_host;
                $apnsCert = $this->apns_cert;
                $apnsPort = $this->apns_port;
                $apnsPass = $this->apns_pass;

                $streamContext = stream_context_create();
                stream_context_set_option($streamContext, 'ssl', 'local_cert', $apnsCert);
                stream_context_set_option($streamContext, 'ssl', 'passphrase', $apnsPass);

                $apns = stream_socket_client('ssl://'.$apnsHost.':'.$apnsPort, $error, $errorString, 60, STREAM_CLIENT_CONNECT, $streamContext);

                if (!$apns) {
                    exit("Failed to connect: $error $errorString\n" . PHP_EOL);
                }

                $apnsPayload = [
                    'aps' => [
                        'badge' => 1,
                        'sound' => 'default',
                        'alert' => [
                            "title" => $payload['notificationTitle'],
                            "body" => $payload['notificationContent']
                        ],
                        'notificationType' => $payload['notificationType'],
                        'networkId' => $payload['networkId']
                    ],
                    'acme2' => [
                        "memberId" => $payload['memberId'],
                        "memberName" => $payload['memberName']
                    ]
                ];
                
                $payload = json_encode($apnsPayload);
                $deviceToken = pack('H*', str_replace(' ', '', $token));
                $apnsMessage = chr(0) . chr(0) . chr(32) . $deviceToken . chr(0) . chr(strlen($payload)) . $payload;

                fwrite($apns, $apnsMessage);
                fclose($apns);
                return 200;
                break;
        }
    }
}
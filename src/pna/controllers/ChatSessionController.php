<?php
namespace pna\controllers;

use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use pna\helpers\DateTimeHelper;
use pna\helpers\UniqueIdHelper;
use pna\models\ChatSession;
use pna\models\Connection;
use pna\models\ErrorResponsePayload;
use pna\models\Member;
use pna\models\ResponsePayload;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

class ChatSessionController extends BaseController {
	protected $requiredParams = ['member-id', 'other-member-id'];

	public function __construct(Container $container) {
		parent::__construct($container);
    }

    public function startSession(Request $request, Response $response)
    {
        $link = $this->getPath($request);
        $params = $request->getParsedBody();

        if ($this->hasMissingRequiredParams($params)) {
			$parametersErrorPayload = $this->getParametersErrorPayload($link);
			return $response->withJson($parametersErrorPayload, 401);
        }

        try {
			$member = Member::findOrFail($params['member-id']);
            $otherMember = Member::findOrFail($params['other-member-id']);
			$chatSession = ChatSession::select('id as chatId', 'member_id', 'other_member_id', 'created_at')
				->where(['member_id' => $params['member-id'], 'other_member_id' => $params['other-member-id']])
				->orWhere(['member_id' => $params['other-member-id'], 'other_member_id' => $params['member-id']])
				->first();

		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, 500);
        } catch (ModelNotFoundException $modelException) {
			$modelNotFoundErrorPayload = ErrorResponsePayload::getModelNotFoundErrorPayload($link, $modelException);
			return $response->withJson($modelNotFoundErrorPayload, $modelNotFoundErrorPayload['code']);
		}

		if (empty($chatSession)) {
			$dateTime = new DateTimeHelper();

			$generatedId =  UniqueIdHelper::getUniqueId();
			$generatedIdExists = $this->isIdAvailable($generatedId);
			if ($generatedIdExists) {
				$this->startSession($request, $response);
			}
			
			$chatDetails = [
				"id" => $generatedId,
				"member_id" => $params["member-id"],
				"other_member_id" => $params["other-member-id"],
				"created_at" => $dateTime->format('Y-m-d H:i:s')
			];

			try {
				$chatSession = new ChatSession($chatDetails);
				$chatSession->save();

				$feedback['chatSession'] = $chatSession->getPayload();

				return $response->withJson($feedback, 200);
			} catch (QueryException $dbException) {
				$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
				return $response->withJson($databaseErrorPayload, 500);
			}
		} else {
			$feedback['chatSession'] = $chatSession->getPayload();
			return $response->withJson($feedback, 200);
		}
		
    }

	private function getNetworkDoesNotExistErrorPayload($link) {
		$code = 422;
		$link = $link;
		$message = 'Network does not exist';
		$developerMessage = 'The requested network does not exist';

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
	}
	
	private function isIdAvailable($searchId)
	{
		$chatIdCount = ChatSession::where('id', $searchId)->count();
		if ($chatIdCount > 0) {
			return true;
		}

		return false;
	}
    
}
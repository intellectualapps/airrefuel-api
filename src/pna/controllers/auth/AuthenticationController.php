<?php

namespace pna\controllers\auth;

use Illuminate\Database\QueryException;
use Respect\Validation\Validator as Rule;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;
use pna\controllers\BaseController;
use pna\models\Member;
use pna\models\ResponsePayload;

class AuthenticationController extends BaseController {
    protected $requiredParams = ['email', 'auth-type'];

    public function __construct(Container $container) {
        parent::__construct($container);
    }

    public function authenticate(Request $request, Response $response) {
        $credentials = $request->getParsedBody();

        if ($this->hasMissingRequiredParams($credentials)) {
            $parametersErrorPayload = $this->getParametersErrorPayload('/authenticate');
            return $response->withJson($parametersErrorPayload, 401);
        }

        if (!$this->isRequestValid($request, $credentials['auth-type'])) {
            $validationErrorPayload = $this->getValidationErrorPayload();
            return $response->withJson($validationErrorPayload, 402);
        }

        try {
            $authMember = $this->getAuthenticMember($credentials);
        } catch (QueryException $dbException) {
            $databaseErrorPayload = $this->getDatabaseErrorPayload('/authenticate', $dbException);

            return $response->withJson($databaseErrorPayload, 500);
        }

        if (empty($authMember)) {
            $authenticationErrorPayload = $this->getAuthenticationErrorPayload();
            return $response->withJson($authenticationErrorPayload, 403);
        }

        $authMember->update(['lastactive'=> date('Y-m-d H:i:s')]);

        $feedback = $this->getJWT($credentials['email']);
        $feedback['member'] = $authMember->fresh()->getPayload();

        return $response->withJson($feedback);
    }

    protected function getValidator(Request $request, $rules) {
        return $this->container->validator->validate($request, $rules);
    }

    private function getRulesForEmailValidation() {
        return [
            'email' => Rule::email()->length(null, 100),
            'password' => Rule::alnum()->noWhitespace()->length(1, null),
            'auth-type' => Rule::stringType()->length(5, 8),
        ];
    }

    private function getRulesForFacebookValidation() {
        return [
            'email' => Rule::email()->length(null, 100),
            'auth-type' => Rule::stringType()->length(5, 8),
        ];
    }

    protected function isRequestValid(Request $request, $type) {
        switch ($type) {
            case 'facebook':
                $validationRules = $this->getRulesForFacebookValidation();
                $validator = $this->getValidator($request, $validationRules);
                break;

            default:
                $validationRules = $this->getRulesForEmailValidation();
                $validator = $this->getValidator($request, $validationRules);
                break;
        }

        return $validator->isValid();
    }

    private function getAuthenticMember($credentials) {
        switch ($credentials['auth-type']) {
            case 'facebook':
                $member = Member::where('email', $credentials['email'])->first();
                break;
            
            default:
                $member = Member::where('email', $credentials['email'])->first();
                $verifiedPassword = $this->verifyPassword($credentials['password'], $member->password);
                if (!$verifiedPassword) {
                    return null;
                }
                break;
        }

        return $member;
    }

    private function verifyPassword($password, $hash) {
        return password_verify($password, $hash);
    }

    private function getAuthenticationErrorPayload() {
        $code = 401;
        $link = '/authenticate';
        $message = 'Invalid login credentials';
        $developerMessage = 'We could not find any member with those credentials';

        return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
    }

    private function getValidationErrorPayload() {
        $code = 401;
        $link = '/authenticate';
        $message = 'Invalid login credentials';
        $developerMessage = 'Some parameters are invalid';

        return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
    }

}

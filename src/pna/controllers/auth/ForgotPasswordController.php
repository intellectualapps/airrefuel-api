<?php
namespace pna\controllers\auth;

use Illuminate\Database\QueryException;
use pna\controllers\BaseController;
use pna\controllers\message\MessageController;
use pna\models\Member;
use pna\models\PasswordResetToken;
use pna\models\ResponsePayload;
use Respect\Validation\Validator as Rule;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;
use Tuupola\Base62;

class ForgotPasswordController extends BaseController
{
    protected $requiredParams = ['email'];

    public function __construct(Container $container)
    {
        parent::__construct($container);
    }

    /**
     * Sends a password reset link to a member
     *
     * @param Request $request An instance of Slim's Request object
     * @param Response $reponse An instance of Slim's Response object
     * @return Response
     */
    public function sendPasswordResetLink(Request $request, Response $response)
    {
        $credentials = $request->getParsedBody();

        if ($this->hasMissingRequiredParams($credentials)) {
            $parametersErrorPayload = $this->getParametersErrorPayload('/member/reset-password');
            return $response->withJson($parametersErrorPayload, 401);
        }

        if (!$this->isRequestValid($request)) {
            $validationErrorPayload = $this->getValidationErrorPayload();
            return $response->withJson($validationErrorPayload, 401);
        }

        $member = Member::where('email', $credentials['email'])->first();

        if (empty($member)) {
            $invalidEmail = $this->getMemberNotFoundPayload();
            return $response->withJson($invalidEmail, 404);
        }

        try {

            $messageType = "password_reset_email";
            $messageTemplate = $this->getMessageTemplate($messageType);

            if (empty($messageTemplate)) {
                $templateNotFoundPayLoad = $this->getTemplateNotFoundPayload('/member/reset-password');
                return $response->withJson($templateNotFoundPayLoad, 500);
            }

            $passwordResetURL = $this->getPasswordResetURL($member->email, $this->getToken());

            $subject = str_replace('[{FNAME}]', $member->fname, $messageTemplate->subject);
            $vars = [
                'fname' => $member->fname,
                'reset-url' => $passwordResetURL,
                'copyright-year' => $this->getCopyrightYear(),
                'address' => $this->container->settings['address'],
            ];
            $message = new MessageController($messageTemplate->body, $vars);

            try {
                $this->sendMail($subject, $message, $member);
            } catch (Exception $emailException) {
                $mailSendFailurePayload = $this->getMessageErrorPayload('/member/reset-password', $emailException);
                return $response->withJson($mailSendFailurePayload, 503);
            }

        } catch (QueryException $dbException) {
            $databaseErrorPayload = $this->getDatabaseErrorPayload('/member/reset-password', $dbException);
            return $response->withJson($databaseErrorPayload, 500);
        }

        return $response->withJson([
            'message' => 'A mail containing a password reset link has been sent to the email you provided.'
        ], 200);
    }

    /**
     * Retrieve validator for this entity
     *
     * @param  Slim\Http\Request $reponse Request to be validated
     * @param  Array $rules Rules used to validate $data
     * @return Awurth\SlimValidation\Validator
     */
    protected function getValidator(Request $request, $rules)
    {
        return $this->container->validator->validate($request, $rules);
    }

    /**
     * Retrieve email validation rules
     *
     * @return Array
     */
    private function getRulesForEmailValidation()
    {
        return [
            'email' => Rule::email()->length(null, 100),
        ];
    }

    private function getToken()
    {
        return random_bytes(10);
    }

    /**
     * Retrieve password reset url
     *
     * @param string $email
     */
    private function getPasswordResetURL($email, $token)
    {
        $domain = $this->container->settings['domain'];

        $passwordResetToken = PasswordResetToken::where('id', $email)->first();

        if (empty($passwordResetToken)) {
            $encodedEmail = $this->getEncoded($email);
            $encodedToken = $this->getEncoded($token);

            $passwordResetToken = new PasswordResetToken(
                ['id' => $email, 'token' => $encodedToken]
            );
            $passwordResetToken->save();
        } else {
            $encodedEmail = $this->getEncoded($passwordResetToken->id);
            $encodedToken = $passwordResetToken->token;
        }

        $passwordResetURL = $domain . '/reset?e=' . $encodedEmail . '&t=' . $encodedToken;
        return $passwordResetURL;
    }

    private function isRequestValid(Request $request)
    {

        $validationRules = $this->getRulesForEmailValidation();
        $validator = $this->getValidator($request, $validationRules);

        return $validator->isValid();
    }

    private function getInvalidEmailPayload()
    {
        $code = 401;
        $link = '/member/reset-password';
        $message = 'Invalid credentials';
        $developerMessage = 'The email provided is invalid';

        return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
    }

    private function getMemberNotFoundPayload()
    {
        $code = 401;
        $link = '/member/reset-password';
        $message = 'Invalid credentials';
        $developerMessage = 'We could not find any member with the email provided';

        return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
    }

    private function getEncoded($data)
    {
        return (new Base62)->encode($data);
    }

    private function getDecoded($data)
    {
        return (new Base62)->decode($email);
    }

}

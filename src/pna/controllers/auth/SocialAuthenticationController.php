<?php

namespace pna\controllers\auth;

use Illuminate\Database\QueryException;
use pna\controllers\BaseController;
use pna\controllers\message\MessageController;
use pna\models\Member;
use pna\models\ResponsePayload;
use Respect\Validation\Validator as Rule;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

class SocialAuthenticationController extends BaseController
{
    protected $requiredParams = ['id', 'type'];

    public function __construct(Container $container)
    {
        parent::__construct($container);
    }

    public function authenticate(Request $request, Response $response)
    {
        $link = $this->getPath($request);
        $credentials = $request->getParsedBody();

        if ($this->hasMissingRequiredParams($credentials)) {
            $parametersErrorPayload = $this->getParametersErrorPayload($link);
            return $response->withJson($parametersErrorPayload, 401);
        }

        try {
            $authMember = $this->getAuthenticMember($credentials);
        } catch (QueryException $dbException) {
            $databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);

            return $response->withJson($databaseErrorPayload, 500);
        }

        if (empty($authMember)) {
            $accountExist = $this->performSignUpAuthentication($credentials["id"]);
            if ($accountExist) {
                $memberExistsErrorPayload = $this->getMemberExistsErrorPayload();
                return $response->withJson($memberExistsErrorPayload, 400);
            }

            switch ($credentials['type']) {
                case 'phone':
                    $memberDetails['phone'] = $credentials["id"];
                    break;
                
                default:
                    $memberDetails['email'] = $credentials["id"];
                    break;
            }
            
            $memberDetails['password'] = "";

            try {
                $member = new Member($memberDetails);
                switch ($credentials['type']) {
                    case 'phone':
                        $member->setEmailAttribute('');
                        break;
                    
                    default:
                        $member->setPhoneAttribute('');
                        break;
                }
                $member->setFNameAttribute('');
                $member->setLNameAttribute('');
                $member->setFlnameAttribute('');
                $member->setSexAttribute('');
                $member->setAddressAttribute('');
                $member->setCountryAttribute('');
                $member->setStateAttribute('');
                $member->setUrlAttribute($this->getSettingsAttribute('assetsDomain'));
                $member->setPicAttribute('images/avatar.jpg');
                $member->setPicIdAttribute('');
                $member->setPicLikesAttribute(0);
                $member->setJobTitleAttribute('');
                $member->setCompanyAttribute('');
                $member->setIndustryAttribute('');
                $member->setSkillsAttribute('');
                $member->setWebsiteAttribute('');
                $member->setFacebookAttribute('');
                $member->setTwitterAttribute('');
                $member->setInstagramAttribute('');
                $member->setViewsAttribute(0);
                $member->setLastActiveAttribute('');
                $member->setStatusAttribute('Unverified');
                $member->setPrivacyAttribute('Yes');
                $member->setDateAttribute(date('Y-m-d H:i:s'));
                $member->setJoinedAttribute('');
                $member->save();

                switch ($credentials['type']) {
                    case 'phone':
                        $feedback["authToken"] = "";
                        break;
                    
                    default:
                        $feedback = $this->getJWT($credentials["id"]);
                        break;
                }

                $feedback['member'] = $member->fresh()->getPayload();

                return $response->withJson($feedback, 200);
            } catch (QueryException $dbException) {
                $databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
                return $response->withJson($databaseErrorPayload, 500);
            }
        }

        $feedback = $this->getJWT($credentials['id']);
        $feedback['member'] = $authMember->fresh()->getPayload();

        return $response->withJson($feedback);
    }

    public function completeSignUp(Request $request, Response $response) {
        $params = $request->getParsedBody();
        $link = $this->getPath($request);

		$this->requiredParams = [
            'member-id',
            'email',
            'first-name',
            'last-name',
            'phone-number',
            'state',
            'country-code',
            'job-title',
            'company',
            'industry'
        ];

		if ($this->hasMissingRequiredParams($params)) {
			$parametersErrorPayload = $this->getParametersErrorPayload($link);
			return $response->withJson($parametersErrorPayload, 422);
        }
        
        $sex =  (isset($params["sex"])) ? $params["sex"] : "";
        $address = (isset($params["address"])) ? $params["address"] : "";

		$memberDetails = [
            "email" => $params["email"],
            "password" => "",
            "fname" => $params["first-name"],
            "lname" => $params["last-name"],
            "flname" => $params["first-name"] . " " . $params["last-name"],
            "phone" => $params["phone-number"],
            "sex" => $sex,
            "address" => $address,
			"state" => $params["state"],
			"country" => $params["country-code"],
			"job_title" => $params["job-title"],
			"company" => $params["company"],
			"industry" => $params["industry"]
        ];

		$member = Member::where('id', $params['member-id'])->first();
		if (empty($member)) {
			$memberDoesNotExistPayload = $this->getMemberDoesNotExistErrorPayload($link);
			return $response->withJson($memberDoesNotExistPayload, 422);
        }

		try {
			$member->update($memberDetails);

			$messageType = "welcome_email";
			$vars = [
				'id' => $member->id,
				'fname' => $member->fname,
				'flname' => $member->flname,
				'email' => $member->email,
				'password' => $member->password,
				'copyright-year' => $this->getCopyrightYear(),
				'address' => $this->container->settings['address'],
			];

			try {
				$messageTemplate = $this->getMessageTemplate($messageType);

				if (empty($messageTemplate)) {
					$templateNotFoundPayLoad = $this->getTemplateNotFoundPayload($link);
					return $response->withJson($templateNotFoundPayLoad, 500);
				}

				$subject = str_replace('[{FNAME}]', $member->fname, $messageTemplate->subject);
				$message = new MessageController($messageTemplate->body, $vars);

			} catch (QueryException $dbException) {
				$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
				return $response->withJson($databaseErrorPayload, 500);
			}

			$feedback = $this->getJWT($memberDetails["email"]);
			$feedback['member'] = $member->getPayload();
			return $response->withJson($feedback, 200);

		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, 500);
		}
	}

    protected function getValidator(Request $request, $rules)
    {
        return $this->container->validator->validate($request, $rules);
    }

    private function getRulesForSocialValidation()
    {
        return [
            'id' => Rule::string()->length(1, 100)
        ];
    }

    private function performSignUpAuthentication($id) {
        $accountExist = Member::where(['email' => $id])
            ->orWhere('phone', $id)
            ->count();

		if ($accountExist > 0) {
			return true;
		}

		return false;
	}

    private function getAuthenticationErrorPayload($link)
    {
        $code = 401;
        $link = $link;
        $message = 'Invalid login credentials';
        $developerMessage = 'We could not find any member with those credentials';

        return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
    }

    private function getValidationErrorPayload($link)
    {
        $code = 401;
        $link = $link;
        $message = 'Invalid login credentials';
        $developerMessage = 'Some parameters are invalid';

        return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
    }

	private function getMemberDoesNotExistErrorPayload($link) {
		$code = 422;
		$link = $link;
		$message = 'Member does not exist';
		$developerMessage = 'Member with email address does not exist';

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
	}

    private function getAuthenticMember($credentials)
    {
        $member = Member::where('email', $credentials['id'])
            ->orWhere('phone', $credentials['id'])
            ->first();

        return $member;
    }

}

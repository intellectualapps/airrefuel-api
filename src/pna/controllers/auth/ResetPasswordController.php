<?php
namespace pna\controllers\auth;

use pna\controllers\BaseController;
use pna\models\Member;
use pna\models\PasswordResetToken;
use pna\models\ResponsePayload;
use Respect\Validation\Validator;
use Slim\Http\Request;
use Slim\Http\Response;
use Tuupola\Base62;

class ResetPasswordController extends BaseController
{

    protected $requiredParams = ['e', 't'];

    public function showResetPasswordPage(Request $request, Response $response)
    {
        $params = $request->getQueryParams();

        if ($this->hasMissingRequiredParams($params)) {
            $parametersErrorPayload = $this->getMissingParametersErrorPayload();
            return $response->withJson($parametersErrorPayload, 422);
        }

        $emailParam = $params['e'];
        $tokenParam = $params['t'];

        $decodedEmail = $this->getDecoded($emailParam);

        if (!$this->isEmailValid($decodedEmail)) {
            $emailValidationErrorPayload = $this->getEmailValidationErrorPayload();
            return $response->withJson($emailValidationErrorPayload, 422);
        }

        $passwordResetToken = PasswordResetToken::find($decodedEmail);

        if (!($passwordResetToken->token == $tokenParam)) {
            $invalidParametersPayload = $this->getInvalidParametersPayload();
            return $response->withJson($invalidParametersPayload);
        }

        $passwordResetToken->delete();
        return $this->container->view->render($response, 'reset.phtml', [
            'email' => $passwordResetToken->id
        ]);
    }

    public function updatePassword(Request $request, Response $response)
    {
        $this->requiredParams = ['email', 'password', 'password_confirmation'];
        $params = $request->getParsedBody();

        if ($this->hasMissingRequiredParams($params)) {
            $parametersError = 'Some required fields not set.';

            return $this->container->view->render($response, 'reset.phtml', [
                'email' => $params['email'], 'error' => $parametersError
            ]);
        }

        if ($params['password'] !== $params['password_confirmation']) {
        	$passwordError = 'Passwords do not match';
        	return $this->container->view->render($response, 'reset.phtml', [
        	    'email' => $params['email'], 'error' => $passwordError
        	]);
        }

        if (!$this->isRequestValid($request)) {
            $validationError = 'Some of the values provided are invalid';
            return $this->container->view->render($response, 'reset.phtml', [
        	    'email' => $params['email'], 'error' => $validationError
        	]);
        }

        try {
            $member = Member::where('email', $params['email']);

            $member->update(
                ['password' => $params['password']]
            );
        } catch (QueryException $dbException) {
            $databaseErrorPayload = $this->getDatabaseErrorPayload('/member/reset', $dbException);

            return $response->withJson($databaseErrorPayload, 500);
        }

        $passwordUpdated = 'Your password has been reset';
        return $this->container->view->render($response, 'reset.phtml', [
        	    'email' => $params['email'], 'success' => $passwordUpdated
        	]);
    }

    private function getDecoded($data)
    {
        return (new Base62)->decode($data);
    }

    private function getInvalidParametersPayload()
    {
        $code = 422;
        $link = "/member/reset";
        $message = "It seems this password reset link has been used.";
        $developerMessage = "The email provided in the link can not be found in the password reset token table in the db.";

        return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
    }

    private function isEmailValid($email)
    {

        $emailValidator = $this->getEmailValidator();

        return $emailValidator->validate($email);
    }

    /**
     * Retrieve email validator
     *
     * @return Respect\Validation\Validator
     */
    private function getEmailValidator()
    {
        return Validator::email()->length(null, 100);
    }

    private function getEmailValidationErrorPayload()
    {
        $code = 422;
        $link = '/member/reset';
        $message = 'Invalid parameters';
        $developerMessage = 'The email in the password reset link is invalid';

        return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
    }

    private function isRequestValid(Request $request)
    {

        $validationRules = $this->getRulesForResetValidation();
        $validator = $this->getValidator($request, $validationRules);

        return $validator->isValid();
    }

    /**
     * Retrieve validator for this entity
     *
     * @param  Slim\Http\Request $reponse Request to be validated
     * @param  Array $rules Rules used to validate $data
     * @return Awurth\SlimValidation\Validator
     */
    protected function getValidator(Request $request, $rules)
    {
        return $this->container->validator->validate($request, $rules);
    }

    /**
     * Retrieve reset password rules
     *
     * @return Array
     */
    private function getRulesForResetValidation()
    {
        return [
            'email' => Validator::email()->length(null, 100),
            'password' => Validator::alnum()->noWhitespace()->length(1, null),
        ];
    }
}

<?php
namespace pna\controllers;

use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;
use pna\models\Industry;
use pna\models\IndustryInterest;
use pna\controllers\BaseController;
use Illuminate\Database\QueryException;
use pna\controllers\lookups\IndustryController;

class IndustryInterestController extends BaseController
{
    protected $industryInterest;
    protected $requiredParams = ["industry-ids"];

    public function __construct(Container $container)
    {
        parent::__construct($container);
        $this->industryInterest = new IndustryInterest;
    }


    public function getAllInterest(Request $request, Response $response, $arg)
    {
        $link = $this->getPath($request);

         try{

           $allInterests =  $this->industryInterest->getInterest($arg);
           return $response->withJson(["interests"=>$allInterests], 200);

        }
        catch(Exception $dbException)
        {
            $databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
            return $response->withJson($databaseErrorPayload, 500);
        }
    }

    public function createInterest(Request $request, Response $response, $arg)
    {
        $params = $request->getParsedBody();
        $link = $this->getPath($request);
        
        if ($this->hasMissingRequiredParams($params)) {
            $parametersErrorPayload = $this->getParametersErrorPayload($link);
            return $response->withJson($parametersErrorPayload, 401);
        }
        try{
            
            $industry_ids = explode(',', $params['industry-ids']);
            
            foreach ($industry_ids as $industry_id) {

                if($this->industryInterest->interestAlreadyExist($arg, $industry_id))
                {
                    continue;
                }

                $this->industryInterest->createIndustryInterest($industry_id, $arg);
            }
        }
        catch(Exception $dbException)
        {
            $databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
            return $response->withJson($databaseErrorPayload, 500);
        }

        return $this->getAllInterest($request, $response, $arg);

    }

    public function errorPayload($error, $link)
    {
        $payload = [
            "code"=>409,
            "message"=>"Interest Already Exists",
            "developerMessage"=>"This user already registered this interest",
            "link"=>$link,
        ];

        switch ($error) {
            case 'interest_already_exist':
                $payload["code"] = 409;
                $payload["message"] = "Interest Already Exists";
                $payload["developerMessage"] = "This user already registered this interest";
                break;

             case 'industry_does_not_exist':
                $payload["code"] = 404;
                $payload["message"] = "Industry does Exists";
                $payload["developerMessage"] = "The selected Industry does not exist";
                break;

        }
         
         return ["payload"=>$payload, "code"=>$payload["code"]];
    }

}
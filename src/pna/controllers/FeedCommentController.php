<?php
namespace pna\controllers;

use Illuminate\Database\QueryException;
use pna\controllers\BaseController;
use pna\controllers\FeedController;
use pna\helpers\DateTimeHelper;
use pna\models\Feed;
use pna\models\FeedComment;
use pna\models\Member;
use pna\models\ResponsePayload;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

class FeedCommentController extends BaseController {
	protected $requiredParams = ['email', 'feed-id'];

	public function __construct(Container $container) {
		parent::__construct($container);
	}

	public function getFeedComments(Request $request, Response $response, $args) {
		$link = str_replace('/api/v1', '', $request->getUri()->getPath());

		$params = [
			"email" => $request->getQueryParam('email'),
			"feed-id" => $args['feed-id'],
		];

		if ($this->hasMissingRequiredParams($params)) {
			$parametersErrorPayload = $this->getParametersErrorPayload($link);
			return $response->withJson($parametersErrorPayload, 401);
		}

		try {
			$member = Member::where('email', $params["email"])->first();
			if (empty($member)) {
				$memberDoesNotExistPayload = $this->getMemberDoesNotExistErrorPayload();
				return $response->withJson($memberDoesNotExistPayload, 422);
			}

			$feed = Feed::where('id', $params["feed-id"])->first();
			if (empty($feed)) {
				$feedDoesNotExistPayload = $this->getFeedDoesNotExistErrorPayload($link);
				return $response->withJson($feedDoesNotExistPayload, 422);
			}

			$skip = $request->getQueryParam('start');
			$page_size = $request->getQueryParam('page-size');
			if (!is_null($skip) && !is_null($page_size)) {
				try {
					$feed_comments = $this->getCustomFeedComments($feed, $member->id, $skip, $page_size);

					if (count($feed_comments) < 1) {
						$feed_comments = [];
					}

					return $response->withJson(["feedComments" => $feed_comments], 200);

				} catch (QueryException $dbException) {
					$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
					return $response->withJson($databaseErrorPayload, 500);
				}
			} else {
				try {
					$feed_comments = $this->getDefaultFeedComments($feed, $member->id);

					if (count($feed_comments) < 1) {
						$feed_comments = [];
					}

					return $response->withJson(["feedComments" => $feed_comments], 200);

				} catch (QueryException $dbException) {
					$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
					return $response->withJson($databaseErrorPayload, 500);
				}
			}
		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, 500);
		}
	}

	public function addCommentToFeed(Request $request, Response $response, $args) {
		$this->requiredParams = ['feed-id', 'email', 'comment'];

		$link = str_replace('/api/v1', '', $request->getUri()->getPath());

		$params = [
			"feed-id" => $args['feed-id'],
			"email" => $request->getParam('email'),
			"comment" => $request->getParam('comment'),
		];

		if ($this->hasMissingRequiredParams($params)) {
			$parametersErrorPayload = $this->getParametersErrorPayload('/member');
			return $response->withJson($parametersErrorPayload, 401);
		}

		try {
			$feed = Feed::where('id', $params["feed-id"])->first();
			if (empty($feed)) {
				$feedDoesNotExistPayload = $this->getFeedDoesNotExistErrorPayload($link);
				return $response->withJson($feedDoesNotExistPayload, 422);
			}

			$member = Member::where('email', $params['email'])->first();
			if (empty($member)) {
				$memberDoesNotExistPayload = $this->getMemberDoesNotExistErrorPayload();
				return $response->withJson($memberDoesNotExistPayload, 422);
			}

			$dateTime = new DateTimeHelper();

			$commentDetails = [
				"feed_id" => $params['feed-id'],
				"comment" => $params['comment'],
				"user_id" => $member->id,
				"comment_date" => $dateTime->format('Y-m-d H:i:s')
			];

			try {
				$feed_comment = $this->addComment($commentDetails);

				return $response->withJson($feed_comment, 200);

			} catch (QueryException $dbException) {
				$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
				return $response->withJson($databaseErrorPayload, 500);
			}
		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, 500);
		}
	}

	protected function getFeedDoesNotExistErrorPayload($link) {
		$code = 422;
		$link = $link;
		$message = 'Feed does not exist';
		$developerMessage = 'Feed with id address does not exist';

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
	}

	protected function getDefaultFeedComments($feed, $memberId) {
		$feed_comments = FeedComment::select('feed_comment.*')
			->join('pnn_users', 'feed_comment.user_id', '=', 'pnn_users.id')
			->where('feed_comment.feed_id', $feed->id)
			->take(20)
			->get();

		$formated_feed = $this->formatFeedComment($feed_comments, $memberId);

		return $formated_feed;
	}

	protected function getCustomFeedComments($feed, $memberId, $skip, $page_size) {
		$feed_comments = FeedComment::select('feed_comment.*')
			->join('pnn_users', 'feed_comment.user_id', '=', 'pnn_users.id')
			->where('feed_comment.feed_id', $feed->id)
			->skip($skip)
			->take($page_size)
			->get();

		$formated_feed = $this->formatFeedComment($feed_comments, $memberId);

		return $formated_feed;
	}

	protected function formatFeedComment($feed_comments, $memberId) {
		$newFeedComment = [];
		$formatted_feed_comment = [];

		foreach ($feed_comments as $feed_comment) {

			$formatted_feed_comment["commentId"] = $feed_comment->id;
			$formatted_feed_comment["memberId"] = $feed_comment->user_id;
			$formatted_feed_comment["author"] = $this->getFeedCommentAuthor($feed_comment->user_id);
			$formatted_feed_comment["commentAuthorPicture"] = $this->getFeedCommentAuthorPicture($feed_comment->user_id);
			$formatted_feed_comment["content"] = $feed_comment->comment;
			$formatted_feed_comment["commentDate"] = $feed_comment->comment_date;
			$formatted_feed_comment['networkState'] = FeedController::isMemberInNetwork($memberId, $feed_comment->user_id);

			array_push($newFeedComment, $formatted_feed_comment);
		}

		return $newFeedComment;
	}

	private function getFeedCommentAuthor($user_id) {
		$member = Member::select("fname", "lname")->where("id", $user_id)->first();

		return $comment_author = $member['fname'] . ' ' . $member['lname'];
	}

	protected function getFeedCommentAuthorPicture($user_id) {
		$member = Member::select(["url", "pic"])->where("id", $user_id)->first();
		$picString = $member["pic"];
		if (substr($picString, 0, 8) === "https://" || substr($picString, 0, 7) === "http://") {
			return $member['pic'];
		} else {
			return $this->getAssetOnDomain($member['pic']);
		}
	}

	protected function addComment($comment_details) {
		$feed_comment = new FeedComment($comment_details);
		$feed_comment->save();

		$feedComment = [
			"commentId" => $feed_comment->id,
			"feedId" => $feed_comment->feed_id,
			"comment" => $feed_comment->comment,
			"userId" => $feed_comment->user_id,
		];

		return $feedComment;
	}

	protected function getMemberDoesNotExistErrorPayload($link) {
		$code = 422;
		$link = $link;
		$message = 'Member does not exist';
		$developerMessage = 'Member with email address does not exist';

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
	}

}

<?php
namespace pna\controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use PHPHtmlParser\Dom;
use pna\controllers\BaseController;
use pna\controllers\FeedController;
use pna\helpers\DateTimeHelper;
use pna\models\AppEvent;
use pna\models\AppVideo;
use pna\models\Offer;
use pna\models\AppPNADaily;
use pna\models\Member;
use pna\models\ErrorResponsePayload;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

class HomeScreenContentController extends BaseController {
    public function __construct(Container $container) {
		parent::__construct($container);
    }
    
    public function getHomeScreenContent(Request $request, Response $response) {
        $link = $this->getPath($request);
        $memberId = $request->getQueryParam('member-id');

        if (is_null($memberId) || $memberId == '') {
			$parametersErrorPayload = ErrorResponsePayload::getParametersErrorPayload($link);
			return $response->withJson($parametersErrorPayload, $parametersErrorPayload['code']);
		}

        $homeScreenItems['memberPhotos'] = $this->getMemberPhotosPayload($memberId);
        $homeScreenItems['pnaDaily'] = $this->getPnaDailyPayload();
        $homeScreenItems['event'] = $this->getEventPayload();
        $homeScreenItems['offer'] = $this->getOfferPayload();
        $homeScreenItems['video'] = $this->getVideoPayload();

        $member = Member::find($memberId);
        $member->update(['lastactive'=> date('Y-m-d H:i:s')]);

        return $response->withJson($homeScreenItems, 200);
    }

    private function getMemberPhotosPayload($memberId) {
        try {
            $members = Member::select(['id', 'email', 'fname', 'lname', 'job_title', 'company', 'url', 'pic'])
                ->where('id', '!=', $memberId)
                ->where('pic', '!=', 'images/avatar.jpg')
                ->get()
                ->random(8);
            
            $membersPayload = [];

            foreach ($members as $member) {
                $picString = $member["pic"];
                if (substr($picString, 0, 8) === "https://" || substr($picString, 0, 7) === "http://") {
                    $imageUrl =  $member['pic'];
                } else {
                    $imageUrl = $this->getAssetOnDomain($member['pic']);
                }

                array_push(
                    $membersPayload,
                    [
                        'memberId' => $member->id,
                        'imageUrl' => $imageUrl,
                        'networkState' => FeedController::isMemberInNetwork($memberId, $member->id)
                    ]
                );
            }
        } catch (\Exception $xception) {
			$membersPayload = null;
		}

        return $membersPayload;
    }

    private function getPnaDailyPayload() {
        try {
            $pnaDaily = AppPNADaily::get()->random(1);
            $pnaDailyPayload = [
                'title' => $pnaDaily['title'],
                'imageUrl' => $pnaDaily['image_url'],
                'contentUrl' => $pnaDaily['content_url']
			];
        } catch (\Exception $xception) {
            $pnaDailyPayload = null;
		}  

		return $pnaDailyPayload;
    }

    private function getEventPayload() {
        $today = new DateTimeHelper();

        try {
            $randomEvent = AppEvent::imminent($today->format('Y-m-d'), $today->format('H:i:s'))->get()->random(1);
            $eventPayload = [
				'id' => $randomEvent->id,
				'title' => $randomEvent->title,
				'imageUrl' => $randomEvent->pic_url
			];
        } catch (\Exception $xception) {
			$eventPayload = null;
		}     

		return $eventPayload;
    }

    private function getOfferPayload() {
        try {
            $randomOffer = Offer::where('status', 'active')->get()->random(1);
            $offerPayload = [
				'id' => $randomOffer->id,
				'title' => $randomOffer->title,
				'imageUrl' => $randomOffer->photo
			];
        } catch (\Exception $xception) {
			$offerPayload = null;
		}   

		return $offerPayload;
    }

    private function getVideoPayload() {
        try {
            $randomVideo = AppVideo::get()->random(1);
        } catch (\Exception $xception) {
			return null;
		}

        if (empty($randomVideo)) {
            $videoPayload = null;
        } else {
            $videoPayload = [
                'title' => $randomVideo->title,
                'videoUrl' => $randomVideo->video_url
            ];
        }

        return $videoPayload;
    }
}
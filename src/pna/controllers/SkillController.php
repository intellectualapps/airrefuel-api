<?php
namespace pna\controllers;

use Illuminate\Database\Capsule\Manager as DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use pna\controllers\BaseController;
use pna\exceptions\BaseException;
use pna\helpers\DateTimeHelper;
use pna\models\ErrorResponsePayload;
use pna\models\Feed;
use pna\models\Member;
use Slim\Http\Request;
use Slim\Http\Response;

class SkillController extends BaseController {
	protected $requiredParams = ['skills'];

	public function createSkill(Request $request, Response $response, $args) {
		$memberId = $args['member-id'];
		$skillsString = $request->getParam('skills');
		$skills = $this->trimArray(str_getcsv($skillsString));
		$link = $this->getPath($request);

		try {
			$member = Member::findOrFail($memberId);

			if (empty($skills)) {
				throw new BaseException('No skill was provided.');
			}

			$skillsToCreate = [];
			$memberSkills = $member->skills()->select('skill')->get()->pluck('skill');

			$skillsArray = array_filter($skills, function ($skill) use ($memberSkills) {
				return !$memberSkills->contains($skill);
			});

			if (empty($skillsArray)) {
				throw new BaseException("You have already added those skills");
			} else {
				foreach ($skillsArray as $skill) {
					array_push($skillsToCreate, [
						'skill' => $skill,
					]);
				}
			}

			DB::transaction(function () use ($skillsToCreate, $member) {
				$skills = $member->skills()->createMany($skillsToCreate);

				$date = new DateTimeHelper();

				foreach ($skillsToCreate as $skill) {
					$feed = "Has a new skill <br /><span style='font-size:14px'>" . $skill['skill'] . "</span>";
					$member->feeds()->create([
						'user_id' => $member->id,
						'p_id' => '',
						'type' => Feed::TYPE_JOB,
						'feed' => $feed,
						'likes' => 0,
						'date' => $date->format('Y-m-d h:m:s'),
					]);
				}

			});
			return $response->withJson(['skills' => $skills]);

		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, $databaseErrorPayload['code']);
		} catch (ModelNotFoundException $modelException) {
			$modelNotFoundErrorPayload = ErrorResponsePayload::getModelNotFoundErrorPayload($link, $modelException);
			return $response->withJson($modelNotFoundErrorPayload, $modelNotFoundErrorPayload['code']);
		} catch (BaseException $apiException) {
			$message = 'Missing skill(s).';
			$customErrorPayload = ErrorResponsePayload::getCustomErrorPayload(422, $message,
				$link, $apiException->getMessage());
			return $response->withJson($customErrorPayload, $customErrorPayload['code']);
		} catch (\Error $apiError) {
			$message = 'Something is wrong.';
			$customErrorPayload = ErrorResponsePayload::getCustomErrorPayload(500, $message,
				$link, $apiError->getMessage());
			return $response->withJson($customErrorPayload, $customErrorPayload['code']);
		}
	}

	private function trimArray(array $stringsArray) {
		$trimmed = [];
		foreach ($stringsArray as $string) {
			array_push($trimmed, trim($string));
		}

		return $trimmed;
	}
}

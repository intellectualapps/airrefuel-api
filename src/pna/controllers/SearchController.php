<?php
namespace pna\controllers;

use Illuminate\Database\QueryException;
use pna\models\Member;
use pna\models\Industry;
use pna\models\ResponsePayload;
use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

class SearchController extends BaseController {
	protected $requiredParams = ['member-id', 'search-term'];

	public function __construct(Container $container) {
		parent::__construct($container);
	}

	public function searchMember(Request $request, Response $response, $args) {
		$link = str_replace('/api/v1', '', $request->getUri()->getPath());

		$params = [
			'member-id' => $args['member-id'],
			'search-term' => $args['search-term'],
		];

		if ($this->hasMissingRequiredParams($params)) {
			$parametersErrorPayload = $this->getParametersErrorPayload($link);
			return $response->withJson($parametersErrorPayload, 422);
		}

		try {
			$member = Member::find($params['member-id']);
		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, 500);
		}

		if (empty($member)) {
			$memberNotFoundPayload = $this->getMemberNotFoundPayload($link);
			return $response->withJson($memberNotFoundPayload, 422);
		}

		try {
			$skip = $request->getQueryParam('start');
			$page_size = $request->getQueryParam('page-size');

			try { 
				$industry = Industry::find($params['search-term']);

				if (is_null($skip) && is_null($page_size)) {
					$members = $this->search($params['search-term'], $member->id, $industry->industry);
				} else {
					$members = $this->search($params['search-term'], $member->id, $industry->industry, $skip, $page_size);
				}
			} catch(QueryException $dbException) { 
				if (is_null($skip) && is_null($page_size)) {
					$members = $this->search($params['search-term'], $member->id);
				} else {
					$members = $this->search($params['search-term'], $member->id, null, $skip, $page_size);
				}
			}

			$membersPayload = [];

			foreach ($members as $memberSearched) {
				if ($memberSearched['id'] != $member->id) {
					array_push($membersPayload, [
						'id' => $memberSearched['id'],
						'email' => $memberSearched['email'],
						'fname' => $memberSearched['fname'],
						'lname' => $memberSearched['lname'],
						'jobTitle' => $memberSearched['jobTitle'],
						'company' => $memberSearched['company'],
						'networkState' => $memberSearched['networkState'],
						'url' => $memberSearched['url'],
						'pic' => $memberSearched['pic'],
					]);
				}
			}

			return $response->withJson(['members' => $membersPayload], 200);
		} catch (QueryException $dbException) {
			$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
			return $response->withJson($databaseErrorPayload, 500);
		}
	}

	public function searchUserAndIndustryGlobally (Request $request, Response $response, $args) {
		$link = str_replace('/api/v1', '', $request->getUri()->getPath());

		$this->requiredParams = ["search-type", "search-term"];
		$params = [
			"search-type" => $args["search-type"],
			"search-term" => $args["search-term"]
		];
		$skip = $request->getQueryParam('start');
		$page_size = $request->getQueryParam('page-size');

		if ($this->hasMissingRequiredParams($params)) {
			$parametersErrorPayload = $this->getParametersErrorPayload($link);
			return $response->withJson($parametersErrorPayload, 422);
		}
		
		if ($params['search-type'] === 'industry') {
			try {
				if (is_null($skip) && is_null($page_size)) {
					$searchResults = $this->search($params["search-term"], "industry");
				} else {
					$searchResults = $this->search($params["search-term"], "industry", $skip, $page_size);
				}
			} catch(QueryException $dbException) {
				$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
				return $response->withJson($databaseErrorPayload, 500);
			}
		} else if ($params['search-type'] === 'users') {
			try {
				if (is_null($skip) && is_null($page_size)) {
					$searchResults = $this->search($params["search-term"], "users");
				} else {
					$searchResults = $this->search($params["search-term"], "users", $skip, $page_size);
				}
			} catch(QueryException $dbException) {
				$databaseErrorPayload = $this->getDatabaseErrorPayload($link, $dbException);
				return $response->withJson($databaseErrorPayload, 500);
			}
		} else {
			$searchResults = [];
		}

		return $response->withJson(['searchResult' => $searchResults])->withStatus(200);
	}

	public function searchUserAndIndustryRelative(Request $request, Response $response, $args) {
		$link = $this->getPath($request);
		$this->requiredParams = [
			"member-id", "search-term"
		];
		$params = [
			"member-id" => $args['member-id'],
			"search-term" => $args['search-term']
		];
		$skip = $request->getQueryParam('start');
		$page_size = $request->getQueryParam('page-size');

		if ($this->hasMissingRequiredParams($params)) {
			$parametersErrorPayload = $this->getParametersErrorPayload($link);
			return $response->withJson($parametersErrorPayload)->withStatus(422);
		}

		return $response->withJson(["params"=> $params])->withStatus(200);
	}

	private function search($searchTerm, $searchType = null, $skip = 0, $page_size = 10) {
		switch ($searchType) {
			case "users":
				$searchResult = Member::where(function ($query) use ($searchTerm) {
					$query->where('phone', 'like', "%{$searchTerm}%")
						->orWhere('email', 'like', '%' . str_replace(' ', '', $searchTerm) . '%')
						->orWhere('fname', 'like', "%{$searchTerm}%")
						->orWhere('lname', 'like', "%{$searchTerm}%")
						->orWhere('company', 'like', "%{$searchTerm}%");
					})
					->skip($skip)
					->take($page_size)
					->get();

				return $searchResult->map(function($member) {
					return Member::getMembersInIndustrySlimProfile($member, $this->getAssetOnDomain($member->pic));
				});
				break;

			case "industry":
				$searchResults = Industry::where('industry', 'like', "%{$searchTerm}%")
					->skip($skip)
					->take($page_size)
					->get();
				
				$data = [];

				foreach ($searchResults as $searchResult) {
					$formatedListings = Industry::getPayload($searchResult);
					array_push($data, $formatedListings);
				}

				return $data;
				break;
			
			default:
				return [];
				break;
		}
	}

	private function getPayload($members, $memberId) {
		$data = [];

		foreach ($members as $member) {
			array_push($data, [
				'id' => $member->id,
				'email' => $member->email,
				'fname' => $member->fname,
				'lname' => $member->lname,
				'jobTitle' => $member->job_title,
				'company' => $member->company,
				'networkState' => FeedController::isMemberInNetwork($memberId, $member->id),
				'url' => $member->url,
				'pic' => $this->getAssetOnDomain($member->pic),
			]);
		}
		return $data;
	}

	private function getMemberNotFoundPayload($link) {
		$code = 422;
		$link = $link;
		$message = 'Invalid credentials';
		$developerMessage = 'We could not find any member with the details provided';

		return ResponsePayload::getPayload($code, $message, $link, $developerMessage);
	}

}
<?php

use pna\controllers\auth\AuthenticationController;
use pna\controllers\auth\ResetPasswordController;
use pna\controllers\auth\SocialAuthenticationController;
use pna\controllers\HomeScreenContentController;
use pna\controllers\PNADailyController;
use pna\controllers\PNAMagazineController;
use pna\controllers\PNATravelController;
use pna\controllers\PremiumSubscriptionController;
use pna\controllers\managers\PremiumMembershipSubscriptionManager;
use pna\controllers\TransactionController;
use Slim\Http\Request;
use Slim\Http\Response;

$app->get('/', function (Request $request, Response $response, array $args) {

	$this->logger->info("PNA-API '/' route");

	return $this->renderer->render($response, 'index.phtml', $args);
});

$app->get('/pna-daily-page', PNADailyController::class . ":getPNADailyPage");

$app->put('/subscriptions/monthly/cycle-check', PremiumMembershipSubscriptionManager::class . ':checkMemberSubscription');

$app->group('/admin', function () {
	include 'routes/adminPages.php';
});

$app->get('/reset', ResetPasswordController::class . ':showResetPasswordPage');

$app->group('/api/v1', function () {

	$this->post('/authenticate', AuthenticationController::class . ':authenticate');

	$this->post('/auth/social', SocialAuthenticationController::class . ':authenticate');

	$this->put('/auth/social', SocialAuthenticationController::class . ':completeSignUp');

	$this->get('/home-screen-content', HomeScreenContentController::class . ':getHomeScreenContent');

	$this->group('/member', function () {
		include 'routes/member.php';
	});

	$this->group('/lookup', function () {
		include 'routes/lookup.php';
	});

	$this->get('/pna-daily', PNADailyController::class . ":getPNADaily");
	
	$this->get('/pna-business-magazine', PNAMagazineController::class . ":getBusinessMagazine");

	$this->get('/pna-lifestyle-magazine', PNAMagazineController::class . ":getLifestyleMagazine");

	$this->get('/pna-travel', PNATravelController::class . ":getPNATravel");

	$this->group('/offer', function () {
		include 'routes/offer.php';
	});

	$this->group('/redemption-token', function () {
		include 'routes/token.php';
	});

	$this->get('/premium-subscription/amount', PremiumSubscriptionController::class . ':getPremiumSubscriptionAmount');

	$this->get('/payments/pk', TransactionController::class . ':getPublicKey');

	$this->group('/yellow-pages', function () {
		include 'routes/yellowpages.php';
	});

	$this->group('/event', function () {
		include 'routes/event.php';
	});

});

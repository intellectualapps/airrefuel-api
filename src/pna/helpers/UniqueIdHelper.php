<?php
namespace pna\helpers;

class UniqueIdHelper
{
    public static function getUniqueId()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVXYZ';
        $uniqueId = '';
        for ($i = 0; $i < 24; $i++) {
            $randomInt = rand(0, strlen($characters) - 1);
            $uniqueId .= $characters[$randomInt];
        }
        return $uniqueId;
    }

    public static function getUniqueBusinessId()
    {
        $characters = '0123456789';
        $uniqueBusinessId = '';
        for ($i = 0; $i < 8; $i++) {
            $randomInt = rand(0, strlen($characters) - 1);
            $uniqueBusinessId .= $characters[$randomInt];
        }
        return $uniqueBusinessId;
    }
}

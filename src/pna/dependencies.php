<?php

use Awurth\SlimValidation\Validator;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Monolog\Logger;
use pna\exceptions\Handler;
use Slim\Flash\Messages;

// DIC configuration
$container = $app->getContainer();

// Database
$container['capsule'] = function ($c) {
	$capsule = new Illuminate\Database\Capsule\Manager;
	$capsule->addConnection($c['settings']['db']);
	$capsule->setAsGlobal();
	$capsule->bootEloquent();
	$capsule->getContainer()->singleton(
		ExceptionHandler::class,
		Handler::class
	);
	return $capsule;
};

// monolog
$container['logger'] = function ($c) {
	$settings = $c->get('settings')['logger'];
	$logger = new Monolog\Logger($settings['name']);
	$logger->pushProcessor(new Monolog\Processor\UidProcessor());
	$logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
	return $logger;
};

// view renderer
$container['renderer'] = function ($c) {
	$settings = $c->get('settings')['renderer'];
	return new Slim\Views\PhpRenderer($settings['template_path']);
};

// validator
$container['validator'] = function () {
	return new Validator(false);
};

// copyright year
$container['copyrightYear'] = function () {
	return date('Y');
};

// Register Twig View helper
$container['view'] = function ($c) {
	$settings = $c->get('settings')['renderer'];
	$view = new \Slim\Views\Twig($settings['template_path'], [
		'cache' => false, //change to $settings['cache_path'] in production server
	]);

	// Instantiate and add Slim specific extension
	// $basePath = rtrim(str_ireplace('index.php', '', $c['request']->getUri()->getBasePath()), '/');
	// $view->addExtension(new \Slim\Views\TwigExtension($c['router'], $basePath));

	return $view;
};

$container['flash'] = function () {
	return new Messages();
};
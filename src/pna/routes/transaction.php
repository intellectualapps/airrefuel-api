<?php

use pna\controllers\managers\EventTableReservationManager;
use pna\controllers\managers\EventTicketManager;
use pna\controllers\managers\PremiumMembershipSubscriptionManager;

$this->post('/premium-membership-subscription', PremiumMembershipSubscriptionManager::class . ':subscribeMember');

$this->get('/premium-membership-subscription/{referenceCode}', PremiumMembershipSubscriptionManager::class . ':verifySubscriptionTransaction');

$this->post('/event-ticket', EventTicketManager::class . ':purchaseTicket');

$this->get('/event-ticket/{referenceCode}', EventTicketManager::class . ':verifyTicketTransaction');

$this->post('/event-table', EventTableReservationManager::class . ':reserveTable');

$this->get('/event-table/{referenceCode}', EventTableReservationManager::class . ':verifyTableReservationTransaction');
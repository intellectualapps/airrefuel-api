<?php

use Slim\Http\Request;
use Slim\Http\Response;
use pna\controllers\lookups\IndustryController;
use pna\controllers\lookups\CountryController;
use pna\controllers\lookups\StateController;

// Industry
$this->get('/industry/all', IndustryController::class . ':getAllIndustry');

$this->get('/industry/{industry-id:[0-9]+}/members', IndustryController::class . ':getAllMembersInIndustry');

// Country
$this->get('/country/all', CountryController::class . ':getAllCountry');

// State
$this->get('/states/{country_id}', StateController::class . ':getStatesByCountryID');
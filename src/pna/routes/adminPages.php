<?php

use pna\controllers\AdminController;
use pna\controllers\AppEventController;
use pna\controllers\OfferController;

$this->get('', AdminController::class . ':loginPage')->setName('admin.index');

$this->group('/events', function () {
    $this->get('', AppEventController::class . ":listEventsPage")->setName('events.index');
    $this->post('/search', AppEventController::class . ":searchEvents")->setName('events.search');
    $this->get('/create', AppEventController::class . ":createEventPage")->setName('events.create');
    $this->get('/{event-id}', AppEventController::class . ":getEditEventPage")->setName('events.edit');
});

$this->group('/discounts', function () {
    $this->get('', OfferController::class . ":listOffersPage")->setName('offers.index');
    $this->post('/search', OfferController::class . ":searchOffers")->setName('offers.search');
    $this->get('/create', OfferController::class . ":createOfferPage")->setName('offers.create');
    $this->get('/{offer-id}', OfferController::class . ":getEditOfferPage")->setName('offers.edit');
});

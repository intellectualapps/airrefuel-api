<?php

use pna\controllers\YellowPagesListingController;

$this->get('/category/all', YellowPagesListingController::class . ':getAllYellowPagesCategory');

$this->get('/filter', YellowPagesListingController::class . ':filterYellowPages');

$this->get('/search/{search-term}', YellowPagesListingController::class . ':searchListing');

$this->get('/{listing-id}', YellowPagesListingController::class . ':getListingById');

$this->get('', YellowPagesListingController::class . ':getAllListings');

$this->post('', YellowPagesListingController::class . ':createListing');
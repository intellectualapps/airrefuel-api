<?php

use pna\controllers\OfferController;

$this->post('', OfferController::class . ":createOffer")->setName('api.offer.create');

$this->put('/{offer-id:[a-zA-Z0-9]+}', OfferController::class . ":updateOffer")->setName('api.offer.edit');

$this->get('', OfferController::class . ":getOffers");

$this->post('/{offer-id:[a-zA-Z0-9]+}/redeem', OfferController::class . ":redeemOffer");

$this->get('/{offer-id:[a-zA-Z0-9]+}', OfferController::class . ":getOfferById");
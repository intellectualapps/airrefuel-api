<?php

use pna\controllers\auth\ForgotPasswordController;
use pna\controllers\auth\ResetPasswordController;
use pna\controllers\ChatSessionController;
use pna\controllers\DeviceTokenController;
use pna\controllers\ExperienceController;
use pna\controllers\FeedCommentController;
use pna\controllers\FeedController;
use pna\controllers\LikeController;
use pna\controllers\MemberController;
use pna\controllers\MemberProfileController;
use pna\controllers\NetworkController;
use pna\controllers\SearchController;
use pna\controllers\SkillController;
use pna\controllers\StrippedFeedController;
use pna\controllers\IndustryInterestController;

$this->post('', MemberController::class . ':addMember');

$this->put('', MemberController::class . ':completeSignUp');

$this->post('/reset-password', ForgotPasswordController::class . ':sendPasswordResetLink');

$this->post('/{member-id:[0-9]+}/feed', FeedController::class . ':addFeed');

$this->get('/feed', FeedController::class . ':getFeed');

$this->get('/feed/stripped-content', StrippedFeedController::class . ':getFeed');

$this->get('/feed/{feed-id:[0-9]+}/comment/all', FeedCommentController::class . ':getFeedComments');

$this->post('/feed/{feed-id:[0-9]+}/comment', FeedCommentController::class . ':addCommentToFeed');

$this->post('/feed/{feed-id:[0-9]+}/like', LikeController::class . ':updateFeedLike');

$this->get('/reset', ResetPasswordController::class . ':showResetPasswordPage');

$this->post('/reset', ResetPasswordController::class . ':updatePassword');

$this->get('/slim', MemberProfileController::class . ':getSlimProfile');

$this->get('/network/suggestion', NetworkController::class . ':getNetworkSuggestion');

$this->post('/device-token', DeviceTokenController::class . ':addDeviceToken');

$this->post('/network', NetworkController::class . ':sendNetworkInvitation');

$this->post('/network/add', NetworkController::class . ':addMemberToNetwork');

$this->delete('/network', NetworkController::class . ':removeMemberFromNetwork');

$this->get('/{member-id:[0-9]+}/network', NetworkController::class . ':getNetwork');

$this->get('/{member-id:[0-9]+}/network/categories', NetworkController::class . ':getNetworkCategories');

$this->get('/{member-id:[0-9]+}/network/request', NetworkController::class . ':getNetworkRequest');

$this->put('/network/request/{network-id:[0-9]+}/{option}', NetworkController::class . ':updateNetworkRequest');

$this->get('/{requesting-member-id:[0-9]+}/network/profile/full/{other-member-id:[0-9]+}', NetworkController::class . ':getNetworkMemberFullProfile');

$this->get('/{member-id:[0-9]+}/network/{network-id:[0-9]+}/profile/full', NetworkController::class . ':getNetworkMemberFullProfileWithNetworkId');

$this->get('/{member-id:[0-9]+}/search/{search-term}', SearchController::class . ':searchMember');

$this->group('/transaction', function () {
	include 'transaction.php';
});

$this->put('/profile', MemberProfileController::class . ':updateProfile');

$this->post('/{member-id:[0-9]+}/skill', SkillController::class . ':createSkill');

$this->post('/{member-id:[0-9]+}/experience', ExperienceController::class . ':createExperience');

$this->post('/chat', ChatSessionController::class . ':startSession');

$this->get('/{member-id:[0-9]+}/profile', MemberProfileController::class . ':getFullMemberProfile');

$this->get("/{member-id:[0-9]+}/industry/interest", IndustryInterestController::class. ':getAllInterest');

$this->post("/{member-id:[0-9]+}/industry/interest", IndustryInterestController::class. ':createInterest');

$this->get("/search/global/{search-type}/{search-term}", SearchController::class. ':searchUserAndIndustryGlobally');

$this->get("/search/relative/{member-id:[0-9]+}/{search-term}", SearchController::class . ':searchUserAndIndustryRelative');

$this->get('/{member-id:[0-9]+}/network/categories/{industry-id:[0-9]+}/cards', NetworkController::class . ':getNetworkCategoriesUsers');
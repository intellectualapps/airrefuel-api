<?php

use pna\controllers\AppEventController;

$this->post('', AppEventController::class . ':createEvent')->setName('api.event.create');

$this->put('/{event-id}', AppEventController::class . ':updateEvent')->setName('api.event.edit');

$this->get('', AppEventController::class . ':getEvents');

$this->get('/{event-id}', AppEventController::class . ':getEventById');

$this->post('/rsvp', AppEventController::class . ':addRSVP');
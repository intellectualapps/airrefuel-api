<?php

//
// Add this line to your crontab file:
//
// * * * * * cd /path/to/project && php jobby.php 1>> /dev/null 2>&1
//

use pna\helpers\DateTimeHelper;
use pna\models\Member;
use pna\models\PremiumMember;

require_once __DIR__ . '/../vendor/autoload.php';

$jobby = new \Jobby\Jobby();

$jobby->add('ClosureExample', array(
    'command' => function() {
        $dateTime = new DateTimeHelper();
        $today = $dateTime->format('Y-m-d H:i:s');
        $premiumMembers = PremiumMember::where('authorization_token', '!=', 'null')->get();

        if (empty($premiumMembers)) {
            $payload = [
                "message" => "No premium members."
            ];
        } else {
            $count = 0;
            foreach ($premiumMembers as $premiumMember) {
                if ($today >= $premiumMember->expiry_date) {
                    $authorizationToken = $premiumMember->authorization_token;
                    $member = Member::select('email')->where('id', $memberId)->first();
                    $email = $member->email;
                    $premiumSubscriptionAmount = $this->container->settings['premiumSubscription'];
                    $amount = $premiumSubscriptionAmount['amount'].'00';

                    $customFields = [
                        0 => [
                            "display_name" => "Payment For",
                            "variable_name" => "payment_purpose",
                            "value" => "PREMIUM MEMBERSHIP SUBSCRIPTION"
                        ]
                    ];

                    $postFields = [
                        'authorization_code' => $authorizationToken,
                        'email' => $email,
                        'amount' => $amount,
                        'metadata' => [
                            "custom_fields" => $customFields
                        ]
                    ];

                    $payload = $this->chargeAuthorization($email, $amount, $postFields);

                    if (isset($payload['code'])) {
                        // TODO: Send email for failed charge here
                    } else if ($payload['gatewayStatus'] != 'success') {
                        // TODO: Send email for failed charge here
                    } else {
                        $dateTime = new DateTimeHelper();

                        $startDate = $dateTime->format('Y-m-d');
                        $dateUpdated = $dateTime->format('Y-m-d H:i:s');
                        $reference_code = $payload['transaction']['referenceCode'];

                        $nMonths = 1;
                        $expiryDate = $this->endCycle($startDate, $nMonths);

                        $this->addPremiumMember($memberId, $reference_code, $authorizationToken, $dateUpdated, $expiryDate, $link);

                        // TODO: Send email for successful charge here
                        $count++;
                    }
                }    
            }

            $payload = [
                "message" => "All done and $count members have been charged."
            ];
        }

        return true;
    },
    'schedule' => function() {
        return date('H:i:s') === "02:00:00";
    },
    'output' => __DIR__ . '/../logs/closure.log',
    'enabled' => true,
));

$jobby->run();

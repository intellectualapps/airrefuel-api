<?php

use Slim\App;

date_default_timezone_set('Africa/Lagos');

if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

// All file paths relative to root
chdir(dirname(__DIR__));

require 'vendor/autoload.php';
session_start();

// Instantiate the app
if (file_exists('src/settings.php')) {
    $settings = require 'src/settings.php';
} else {
    $settings = require 'src/settings.php.dist';
}
$app = new App($settings);

// Set up dependencies
require 'src/pna/dependencies.php';

// Register middleware
// require 'src/pna/middleware.php';

// Register routes
require 'src/pna/routes.php';

// Register the database connection with Eloquent
$capsule = $app->getContainer()->get('capsule');
$capsule->setAsGlobal();
$capsule->bootEloquent();

$app->run();

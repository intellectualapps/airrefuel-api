# How to Contribute

## Pull Requests

1. Clone the `airrefuel-api` repository
2. Pull the latest code base in **develop** branch
2. Create a new branch for each feature or improvement. Branch names take the form of [change-type/change-description] for example feature/Authentication. Options for [change-type] are "feature", "mod", "fix", "test" etc. 
3. Send a pull request from each feature branch to the **develop** branch

It is very important to separate new features or improvements into separate feature branches, and to send a
pull request for each branch. This allows us to review and pull in new features or improvements individually.

Also make pull request as simple as possible.

## Style Guide

All pull requests must adhere to the [PSR-2 standard](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md).
